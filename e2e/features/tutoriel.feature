#language: fr

@tutoriel
Fonctionnalité: Information sur les évolutions de Deveco
	Pour pouvoir utiliser deveco
	En tant que DevEco
	Je veux avoir accès à un tutoriel

Scénario: Affichage depuis le menu principal
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je clique sur "Prise en main express"
	Alors je vois "PRISE EN MAIN EXPRESS"
	Alors je vois "Un nouveau guide est à votre disposition pour faire vos premiers pas sur Deveco."
	Quand je clique sur "Ouvrir le tutoriel"
	Alors la fenêtre qui s'ouvre contient l'url du tutoriel

Scénario: Affichage lors du premier login
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah" pour la première fois
	Quand je vais sur le lien magique pour "blah"
	Alors je vois "PRISE EN MAIN EXPRESS"
	Alors je vois "Un nouveau guide est à votre disposition pour faire vos premiers pas sur Deveco."
	Quand je clique sur "Ouvrir le tutoriel"
	Alors la fenêtre qui s'ouvre contient l'url du tutoriel
