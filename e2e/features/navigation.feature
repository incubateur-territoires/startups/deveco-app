#language: fr

@navigation
Fonctionnalité: Navigation et pages
	Pour pouvoir utiliser Déveco
	En tant que Déveco
	Je veux pouvoir consulter les pages du site

Scénario: Navigation
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Alors je vois "territoire de référence"
	Quand je clique sur l'onglet Établissements
	Alors je suis sur la page Établissements
	Alors je vois "Établissements"
	Alors je vois "Ajouter un établissement exogène au territoire"
	Quand je clique sur l'onglet Créateurs
	Alors je suis sur la page Créateurs
	Alors je vois "Créez votre première fiche"
	Quand je clique sur l'onglet Immobilier
	Alors je suis sur la page Immobilier
	Alors je vois "Créez votre première fiche"
	Quand je clique sur l'onglet Rappels
	Alors je vois "Actions Déveco à mener"
	Quand je clique sur "Accueil"
	Alors je vois "Gestion des collaborateurs"
	Quand je clique sur "Paramètres"
	Alors je vois "Adresse de courriel"
	Quand je clique sur "Me déconnecter"
	Alors je vois "Me connecter"
	Alors je vois "Bienvenue sur Deveco"

