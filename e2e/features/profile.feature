#language: fr

@profile
Fonctionnalité: Page de profil
	Pour pouvoir consulter et modifier mes informations personnelles
	En tant qu'utilisateur de la plateforme
	Je veux pouvoir consulter ma page profil et modifier mes informations

Scénario: Informations personnelles
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je clique sur "Paramètres"
	Alors je suis sur la page "/parametres/profil"
	Alors je vois "Informations personnelles"
	Alors je vois "Adresse de courriel"
	Alors je vois "machin@test.fr"
	Quand je clique sur "Modifier mes informations"
	Alors l'élément nommé "input-email" est "désactivé"
	Alors l'élément nommé "submit-profile" est "désactivé"
	Quand je renseigne "Machin" dans le champ "Prénom"
	Quand je renseigne "Truc" dans le champ "Nom"
	Quand je clique sur "Enregistrer"
	Alors je vois "Mise à jour effectuée"
	Alors je vois "Modifier mes informations"
	Alors je vois "Machin"
	Alors je vois "Truc"

Scénario: Gestion du mot de passe
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je clique sur "Paramètres"
	Alors je suis sur la page "/parametres/profil"
	Alors je vois "Gestion du mot de passe"
	Quand je clique sur "Créer mon mot de passe"
	Alors l'élément nommé "submit-password" est "désactivé"
	Quand je renseigne "test" dans le champ "Mot de passe"
	Quand je renseigne "confirmation" dans le champ "Confirmation"
	Alors l'élément nommé "submit-password" est "désactivé"
	Quand je renseigne "test" dans le champ "Confirmation"
	Alors l'élément nommé "submit-password" est "activé"
	Quand je clique sur "Enregistrer"
	Alors je vois "Mise à jour effectuée"
	Alors je vois "Modifier mon mot de passe"

Scénario: Consultation de l'accès SIG
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je clique sur "Paramètres"
	Alors je suis sur la page "/parametres/profil"
	Quand je clique sur "SIG"
	Alors je suis sur la page "/parametres/sig"
	Alors je vois "Vous n'avez pas de jeton, veuillez nous contacter."

Scénario: Gestion des étiquettes de Qualification
	Quand j'ai un compte "DevEco" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Quand je clique sur "Paramètres"
	Alors je suis sur la page "/parametres/profil"
	Quand je clique sur "Gestion des étiquettes de qualification"
	Alors je suis sur la page "/parametres/territoire"
	Alors je vois "Les modifications de qualification impactent l'ensemble des utilisateurs"
	Alors je vois "Activités réelles"
	Alors je vois "Zones géographiques"
	Alors je vois "Mots-clés"
