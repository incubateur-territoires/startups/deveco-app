#language: fr

@superadmin
Fonctionnalité: Superadmin
	Pour pouvoir gérer la plateforme
	En tant que superadmin
	Je veux pouvoir consulter les utilisateurs, en créer, et changer leur mot de passe

Scénario: Consulter les utilisateurs
	Quand j'ai un compte "superadmin" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Alors je suis sur la page "/admin/utilisateurs"
	Alors je vois "Rechercher un utilisateur"
	Alors je vois "Territoire"
	Alors je vois "Email"
	Alors je vois "Dernier login"
	Alors je vois "deveco@ccd.fr"
	Alors je vois "CC du Diois"
	Alors je vois "Communauté de communes"

Scénario: Changer le mot de passe d'une utilisatrice
	Quand j'ai un compte "superadmin" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Alors je suis sur la page "/admin/utilisateurs"
	Quand je clique sur "edit-password-6"
	Alors l'élément nommé "submit-password" est "désactivé"
	Quand je renseigne "nouveau" dans le champ "Mot de passe"
	Quand je renseigne "confirmation" dans le champ "Confirmez le mot de passe"
	Alors l'élément nommé "submit-password" est "désactivé"
	Quand je renseigne "nouveau" dans le champ "Confirmez le mot de passe"
	Alors l'élément nommé "submit-password" est "activé"
	Quand je clique sur "Enregistrer"
	Quand je clique sur "Me déconnecter"
	Quand je vais sur la page d'accueil
	Alors je vois "Deveco"
	Alors je vois "Me connecter"
	Quand je clique sur "Me connecter"
	Alors je vois "Se connecter à Dévéco"
	Quand je renseigne "deveco@ccd.fr" dans le champ "Courriel *"
	Quand je clique sur "mot de passe"
	Quand je renseigne "nouveau" dans le champ "Mot de passe *"
	Quand je clique sur "Se connecter"
	Alors je vois "territoire de référence"
	Alors je suis sur la page d'accueil

Scénario: Créer une utilisatrice
	Quand j'ai un compte "superadmin" avec l'email "machin@test.fr" et la clef "blah"
	Quand je vais sur le lien magique pour "blah"
	Alors je suis sur la page "/admin/utilisateurs"
	Quand je clique sur "Ajouter un utilisateur"
	Quand je renseigne "Sally" dans le champ "Prénom"
	Quand je renseigne "Dragon" dans le champ "Nom"
	Quand je renseigne "sally@aweso.me" dans le champ "Courriel"
	Quand je renseigne "Boucle" dans le champ "Territoire"
	Quand je clique sur "#option-0"
	Alors l'élément nommé "submit-new-user" est "activé"
	Quand je clique sur "Enregistrer"
	Alors je vois "Sally"
	Alors je vois "sally@aweso.me"
	Alors je vois "Boucle Nord de Seine"
	Alors je vois "Établissement public territorial"
