import elmPlugin from "vite-plugin-elm";

export default {
  root: "explorer",
  plugins: [elmPlugin()],
  server: {
    port: 4000,
  },
  publicDir: "./public",
};
