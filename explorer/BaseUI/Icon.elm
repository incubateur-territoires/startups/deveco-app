module BaseUI.Icon exposing (docs)

import ElmBook.Chapter as Chapter exposing (Chapter)


docs : Chapter state
docs =
    Chapter.chapter "Icônes"
        |> Chapter.renderComponentList []



-- TODO for the new icons
-- [ ( "Toutes les icônes"
--   , div [] <|
--         List.intersperse (br []) <|
--             List.map (\( icon, title ) -> span [ Attr.title title ] [ Icon.icon icon ]) <|
--                 Icon.allIcons
--   )
-- ]
