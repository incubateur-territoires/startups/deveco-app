module ReviewConfig exposing (config)

{-| Do not rename the ReviewConfig module or the config function, because
`elm-review` will look for these.

To add packages that contain rules, add them to this review project using

    `elm install author/packagename`

when inside the directory containing this file.

-}

import NoDebug.Log
import NoDebug.TodoOrToString
import NoDuplicatePorts
import NoExposingEverything
import NoImportingEverything
import NoInconsistentAliases
import NoMissingTypeAnnotation
import NoModuleOnExposedNames
import NoRecursiveUpdate
import NoRegex
import NoSinglePatternCase
import NoUnused.CustomTypeConstructorArgs
import NoUnused.CustomTypeConstructors
import NoUnused.Dependencies
import NoUnused.Exports
import NoUnused.Modules
import NoUnused.Parameters
import NoUnused.Patterns
import NoUnused.Variables
import Review.Rule exposing (Rule)


config : List Rule
config =
    [ NoDebug.Log.rule
    , NoDebug.TodoOrToString.rule
    , NoDuplicatePorts.rule
    , NoExposingEverything.rule
    , NoImportingEverything.rule
        [ "Utils.Html"
        ]
    , NoInconsistentAliases.config
        [ ( "Accessibility", "Html" )
        , ( "DSFR.Typography", "Typo" )
        ]
        |> NoInconsistentAliases.rule
    , NoMissingTypeAnnotation.rule
    , NoModuleOnExposedNames.rule
    , NoRecursiveUpdate.rule
    , NoRegex.rule
    , NoSinglePatternCase.rule NoSinglePatternCase.fixInArgument
    , NoSinglePatternCase.rule NoSinglePatternCase.fixInLet
    , NoUnused.CustomTypeConstructors.rule []
    , NoUnused.CustomTypeConstructorArgs.rule
    , NoUnused.Dependencies.rule
    , NoUnused.Exports.rule
    , NoUnused.Modules.rule
    , NoUnused.Parameters.rule
    , NoUnused.Patterns.rule
    , NoUnused.Variables.rule
    ]
        |> List.map (Review.Rule.ignoreErrorsForDirectories [ "vendor/", "src/Lib/" ])
