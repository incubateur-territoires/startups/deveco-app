const fs = require("fs");

function getJson(path) {
	return JSON.parse(fs.readFileSync(path).toString());
}

function buildJsonDb(_basePath = "public/data") {
	return JSON.stringify({
		stuff: ["This", "is", "so", "cool"],
	});
}

module.exports = {
	buildJsonDb,
	getJson,
};
