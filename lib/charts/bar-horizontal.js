import BaseChart from "./base";

export default class extends BaseChart {
	constructor() {
		super();
	}

	static get observedAttributes() {
		return ["data"];
	}

	get config() {
		return {
			chart: {
				type: "bar",
				animation: true,
			},
			credits: { enabled: false },
			title: {
				text: "",
			},
			xAxis: {
				categories: [],
				tickPosition: "outside",
				labels: {
					step: 1,
					style: { fontSize: "10px", color: "#333" },
				},
			},
			yAxis: {
				min: 0,
				title: {
					text: null,
				},
			},
			legend: {
				reversed: true,
			},
			plotOptions: {
				animation: true,
				series: {
					animation: true,
					stacking: "normal",
				},
			},
			series: [],
		};
	}

	attributeChanged(name, _oldValue, newValue) {
		if (name === "data") {
			const { values, config } = JSON.parse(newValue);

			const total = values.reduce((acc, { count }) => acc + count, 0);
			const toPercent = (count) =>
				Math.round((count / total) * 100 * 100) / 100;

			const seriesData = config?.maxItems
				? values.slice(0, config?.maxItems)
				: values;

			const title = config?.title || "";
			const series = [
				{
					showInLegend: false,
					name: title,
					tooltip: {
						pointFormatter: function () {
							const value = this.y;
							return `<strong>${value}</strong> (${toPercent(value)}%)`;
						},
					},
					data: seriesData.map(({ count }) => count),
				},
			];
			this.chart.update({
				xAxis: {
					categories: seriesData.map(({ label }) => `${label}`),
				},
			});

			// Remove all existing series...
			while (this.chart.series.length) {
				this.chart.series[0].remove();
			}

			// ... and replace them with fresh ones
			for (const serie of series) {
				this.chart.addSeries(serie);
			}
			this.chart.redraw()
		}
	}
}
