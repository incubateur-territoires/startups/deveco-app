module DSFR.Grid exposing (col, col1, col10, col11, col12, col2, col3, col4, col5, col6, col7, col8, col9, colLg, colLg1, colLg10, colLg11, colLg12, colLg2, colLg3, colLg4, colLg5, colLg6, colLg7, colLg8, colLg9, colMd, colMd1, colMd10, colMd11, colMd12, colMd2, colMd3, colMd4, colMd5, colMd6, colMd7, colMd8, colMd9, colOffset, colOffset1, colOffset10, colOffset11, colOffset12, colOffset2, colOffset3, colOffset4, colOffset5, colOffset6, colOffset7, colOffset8, colOffset9, colOffsetLg, colOffsetLg1, colOffsetLg10, colOffsetLg11, colOffsetLg12, colOffsetLg2, colOffsetLg3, colOffsetLg4, colOffsetLg5, colOffsetLg6, colOffsetLg7, colOffsetLg8, colOffsetLg9, colOffsetSm, colOffsetSm1, colOffsetSm10, colOffsetSm11, colOffsetSm12, colOffsetSm2, colOffsetSm3, colOffsetSm4, colOffsetSm5, colOffsetSm6, colOffsetSm7, colOffsetSm8, colOffsetSm9, colOffsetXl, colOffsetXl1, colOffsetXl10, colOffsetXl11, colOffsetXl12, colOffsetXl2, colOffsetXl3, colOffsetXl4, colOffsetXl5, colOffsetXl6, colOffsetXl7, colOffsetXl8, colOffsetXl9, colSm, colSm1, colSm10, colSm11, colSm12, colSm2, colSm3, colSm4, colSm5, colSm6, colSm7, colSm8, colSm9, colXl, colXl1, colXl10, colXl11, colXl12, colXl2, colXl3, colXl4, colXl5, colXl6, colXl7, colXl8, colXl9, container, containerFluid, gridRow, gridRowBottom, gridRowCenter, gridRowGutters, gridRowLeft, gridRowMiddle, gridRowRight, gridRowTop)

import Accessibility exposing (Attribute)
import Html.Attributes exposing (class)


type Layout
    = XS
    | SM
    | MD
    | LG
    | XL


container : Attribute msg
container =
    class "fr-container"


containerFluid : Attribute msg
containerFluid =
    class "fr-container--fluid"


gridRowPrefix : String
gridRowPrefix =
    "fr-grid-row"


gridRow : Attribute msg
gridRow =
    class <| gridRowPrefix


gridRowGutters : Attribute msg
gridRowGutters =
    class <| gridRowPrefix ++ "--gutters"


gridRowLeft : Attribute msg
gridRowLeft =
    class <| gridRowPrefix ++ "--left"


gridRowCenter : Attribute msg
gridRowCenter =
    class <| gridRowPrefix ++ "--center"


gridRowRight : Attribute msg
gridRowRight =
    class <| gridRowPrefix ++ "--right"


gridRowTop : Attribute msg
gridRowTop =
    class <| gridRowPrefix ++ "--top"


gridRowMiddle : Attribute msg
gridRowMiddle =
    class <| gridRowPrefix ++ "--middle"


gridRowBottom : Attribute msg
gridRowBottom =
    class <| gridRowPrefix ++ "--bottom"


genClass : String -> Layout -> Int -> Attribute msg
genClass root layout amount =
    let
        layoutClass =
            case layout of
                XS ->
                    ""

                SM ->
                    "-sm"

                MD ->
                    "-md"

                LG ->
                    "-lg"

                XL ->
                    "-xl"

        amountClass =
            if amount <= 0 then
                ""

            else if amount > 12 then
                ""

            else
                "-" ++ String.fromInt amount
    in
    class <| root ++ layoutClass ++ amountClass


genCol : Layout -> Int -> Attribute msg
genCol =
    genClass "fr-col"


genColOffset : Layout -> Int -> Attribute msg
genColOffset =
    genClass "fr-col-offset"


col : Attribute msg
col =
    genCol XS 0


col1 : Attribute msg
col1 =
    genCol XS 1


col2 : Attribute msg
col2 =
    genCol XS 2


col3 : Attribute msg
col3 =
    genCol XS 3


col4 : Attribute msg
col4 =
    genCol XS 4


col5 : Attribute msg
col5 =
    genCol XS 5


col6 : Attribute msg
col6 =
    genCol XS 6


col7 : Attribute msg
col7 =
    genCol XS 7


col8 : Attribute msg
col8 =
    genCol XS 8


col9 : Attribute msg
col9 =
    genCol XS 9


col10 : Attribute msg
col10 =
    genCol XS 10


col11 : Attribute msg
col11 =
    genCol XS 11


col12 : Attribute msg
col12 =
    genCol XS 12


colSm : Attribute msg
colSm =
    genCol SM 0


colSm1 : Attribute msg
colSm1 =
    genCol SM 1


colSm2 : Attribute msg
colSm2 =
    genCol SM 2


colSm3 : Attribute msg
colSm3 =
    genCol SM 3


colSm4 : Attribute msg
colSm4 =
    genCol SM 4


colSm5 : Attribute msg
colSm5 =
    genCol SM 5


colSm6 : Attribute msg
colSm6 =
    genCol SM 6


colSm7 : Attribute msg
colSm7 =
    genCol SM 7


colSm8 : Attribute msg
colSm8 =
    genCol SM 8


colSm9 : Attribute msg
colSm9 =
    genCol SM 9


colSm10 : Attribute msg
colSm10 =
    genCol SM 10


colSm11 : Attribute msg
colSm11 =
    genCol SM 11


colSm12 : Attribute msg
colSm12 =
    genCol SM 12


colMd : Attribute msg
colMd =
    genCol MD 0


colMd1 : Attribute msg
colMd1 =
    genCol MD 1


colMd2 : Attribute msg
colMd2 =
    genCol MD 2


colMd3 : Attribute msg
colMd3 =
    genCol MD 3


colMd4 : Attribute msg
colMd4 =
    genCol MD 4


colMd5 : Attribute msg
colMd5 =
    genCol MD 5


colMd6 : Attribute msg
colMd6 =
    genCol MD 6


colMd7 : Attribute msg
colMd7 =
    genCol MD 7


colMd8 : Attribute msg
colMd8 =
    genCol MD 8


colMd9 : Attribute msg
colMd9 =
    genCol MD 9


colMd10 : Attribute msg
colMd10 =
    genCol MD 10


colMd11 : Attribute msg
colMd11 =
    genCol MD 11


colMd12 : Attribute msg
colMd12 =
    genCol MD 12


colLg : Attribute msg
colLg =
    genCol LG 0


colLg1 : Attribute msg
colLg1 =
    genCol LG 1


colLg2 : Attribute msg
colLg2 =
    genCol LG 2


colLg3 : Attribute msg
colLg3 =
    genCol LG 3


colLg4 : Attribute msg
colLg4 =
    genCol LG 4


colLg5 : Attribute msg
colLg5 =
    genCol LG 5


colLg6 : Attribute msg
colLg6 =
    genCol LG 6


colLg7 : Attribute msg
colLg7 =
    genCol LG 7


colLg8 : Attribute msg
colLg8 =
    genCol LG 8


colLg9 : Attribute msg
colLg9 =
    genCol LG 9


colLg10 : Attribute msg
colLg10 =
    genCol LG 10


colLg11 : Attribute msg
colLg11 =
    genCol LG 11


colLg12 : Attribute msg
colLg12 =
    genCol LG 12


colXl : Attribute msg
colXl =
    genCol XL 0


colXl1 : Attribute msg
colXl1 =
    genCol XL 1


colXl2 : Attribute msg
colXl2 =
    genCol XL 2


colXl3 : Attribute msg
colXl3 =
    genCol XL 3


colXl4 : Attribute msg
colXl4 =
    genCol XL 4


colXl5 : Attribute msg
colXl5 =
    genCol XL 5


colXl6 : Attribute msg
colXl6 =
    genCol XL 6


colXl7 : Attribute msg
colXl7 =
    genCol XL 7


colXl8 : Attribute msg
colXl8 =
    genCol XL 8


colXl9 : Attribute msg
colXl9 =
    genCol XL 9


colXl10 : Attribute msg
colXl10 =
    genCol XL 10


colXl11 : Attribute msg
colXl11 =
    genCol XL 11


colXl12 : Attribute msg
colXl12 =
    genCol XL 12


colOffset : Attribute msg
colOffset =
    genColOffset MD 0


colOffset1 : Attribute msg
colOffset1 =
    genColOffset MD 1


colOffset2 : Attribute msg
colOffset2 =
    genColOffset MD 2


colOffset3 : Attribute msg
colOffset3 =
    genColOffset MD 3


colOffset4 : Attribute msg
colOffset4 =
    genColOffset MD 4


colOffset5 : Attribute msg
colOffset5 =
    genColOffset MD 5


colOffset6 : Attribute msg
colOffset6 =
    genColOffset MD 6


colOffset7 : Attribute msg
colOffset7 =
    genColOffset MD 7


colOffset8 : Attribute msg
colOffset8 =
    genColOffset MD 8


colOffset9 : Attribute msg
colOffset9 =
    genColOffset MD 9


colOffset10 : Attribute msg
colOffset10 =
    genColOffset MD 10


colOffset11 : Attribute msg
colOffset11 =
    genColOffset MD 11


colOffset12 : Attribute msg
colOffset12 =
    genColOffset MD 12


colOffsetLg : Attribute msg
colOffsetLg =
    genColOffset LG 0


colOffsetLg1 : Attribute msg
colOffsetLg1 =
    genColOffset LG 1


colOffsetLg2 : Attribute msg
colOffsetLg2 =
    genColOffset LG 2


colOffsetLg3 : Attribute msg
colOffsetLg3 =
    genColOffset LG 3


colOffsetLg4 : Attribute msg
colOffsetLg4 =
    genColOffset LG 4


colOffsetLg5 : Attribute msg
colOffsetLg5 =
    genColOffset LG 5


colOffsetLg6 : Attribute msg
colOffsetLg6 =
    genColOffset LG 6


colOffsetLg7 : Attribute msg
colOffsetLg7 =
    genColOffset LG 7


colOffsetLg8 : Attribute msg
colOffsetLg8 =
    genColOffset LG 8


colOffsetLg9 : Attribute msg
colOffsetLg9 =
    genColOffset LG 9


colOffsetLg10 : Attribute msg
colOffsetLg10 =
    genColOffset LG 10


colOffsetLg11 : Attribute msg
colOffsetLg11 =
    genColOffset LG 11


colOffsetLg12 : Attribute msg
colOffsetLg12 =
    genColOffset LG 12


colOffsetSm : Attribute msg
colOffsetSm =
    genColOffset SM 0


colOffsetSm1 : Attribute msg
colOffsetSm1 =
    genColOffset SM 1


colOffsetSm2 : Attribute msg
colOffsetSm2 =
    genColOffset SM 2


colOffsetSm3 : Attribute msg
colOffsetSm3 =
    genColOffset SM 3


colOffsetSm4 : Attribute msg
colOffsetSm4 =
    genColOffset SM 4


colOffsetSm5 : Attribute msg
colOffsetSm5 =
    genColOffset SM 5


colOffsetSm6 : Attribute msg
colOffsetSm6 =
    genColOffset SM 6


colOffsetSm7 : Attribute msg
colOffsetSm7 =
    genColOffset SM 7


colOffsetSm8 : Attribute msg
colOffsetSm8 =
    genColOffset SM 8


colOffsetSm9 : Attribute msg
colOffsetSm9 =
    genColOffset SM 9


colOffsetSm10 : Attribute msg
colOffsetSm10 =
    genColOffset SM 10


colOffsetSm11 : Attribute msg
colOffsetSm11 =
    genColOffset SM 11


colOffsetSm12 : Attribute msg
colOffsetSm12 =
    genColOffset SM 12


colOffsetXl : Attribute msg
colOffsetXl =
    genColOffset XL 0


colOffsetXl1 : Attribute msg
colOffsetXl1 =
    genColOffset XL 1


colOffsetXl2 : Attribute msg
colOffsetXl2 =
    genColOffset XL 2


colOffsetXl3 : Attribute msg
colOffsetXl3 =
    genColOffset XL 3


colOffsetXl4 : Attribute msg
colOffsetXl4 =
    genColOffset XL 4


colOffsetXl5 : Attribute msg
colOffsetXl5 =
    genColOffset XL 5


colOffsetXl6 : Attribute msg
colOffsetXl6 =
    genColOffset XL 6


colOffsetXl7 : Attribute msg
colOffsetXl7 =
    genColOffset XL 7


colOffsetXl8 : Attribute msg
colOffsetXl8 =
    genColOffset XL 8


colOffsetXl9 : Attribute msg
colOffsetXl9 =
    genColOffset XL 9


colOffsetXl10 : Attribute msg
colOffsetXl10 =
    genColOffset XL 10


colOffsetXl11 : Attribute msg
colOffsetXl11 =
    genColOffset XL 11


colOffsetXl12 : Attribute msg
colOffsetXl12 =
    genColOffset XL 12
