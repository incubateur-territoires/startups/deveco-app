module DSFR.Input exposing (InputConfig, InputType(..), MandatoryInputConfig, OptionalInputConfig, date, defaultOptions, email, input, new, number, password, select, textArea, textDisplay, view, withDisabled, withError, withExtraAttrs, withHint, withOptions, withReadonly, withType)

import Accessibility exposing (Attribute, Html, div, inputNumber, inputText, label, option, p, span, text, textarea)
import Accessibility.Aria as Aria
import DSFR.Typography
import Html
import Html.Attributes as Attr
import Html.Attributes.Extra exposing (empty)
import Html.Events as Events
import Html.Extra exposing (nothing, static)
import Json.Encode as Encode


input : MandatoryInputConfig msg -> Html msg
input config =
    view { mandatory = config, optional = defaultOptions }


type InputType data
    = TextInput
    | EmailInput
    | PasswordInput
    | TextArea (Maybe Int)
    | DateInput
    | NumberInput
    | TextDisplay
    | SelectInput (SelectInputOptions data)


type alias SelectInputOptions data =
    { options : List data
    , toId : data -> String
    , toLabel : data -> Html Never
    }


new : MandatoryInputConfig msg -> InputConfig data msg
new mandatory =
    { mandatory = mandatory, optional = defaultOptions }


withOptions : OptionalInputConfig data msg -> InputConfig data msg -> InputConfig data msg
withOptions optional config =
    { config | optional = optional }


withHint : List (Html Never) -> InputConfig data msg -> InputConfig data msg
withHint hint { mandatory, optional } =
    { mandatory = mandatory, optional = { optional | hint = hint } }


withError : Maybe (List (Html msg)) -> InputConfig data msg -> InputConfig data msg
withError errorMsg { mandatory, optional } =
    { mandatory = mandatory, optional = { optional | errorMsg = errorMsg } }


withDisabled : Bool -> InputConfig data msg -> InputConfig data msg
withDisabled disabled { mandatory, optional } =
    { mandatory = mandatory, optional = { optional | disabled = disabled } }


withReadonly : Bool -> InputConfig data msg -> InputConfig data msg
withReadonly readonly { mandatory, optional } =
    { mandatory = mandatory, optional = { optional | readonly = readonly } }


withType : InputType data -> InputConfig data msg -> InputConfig data msg
withType type_ { mandatory, optional } =
    { mandatory = mandatory, optional = { optional | type_ = type_ } }


textArea : Maybe Int -> InputConfig data msg -> InputConfig data msg
textArea rows =
    withType <| TextArea rows


email : InputConfig data msg -> InputConfig data msg
email =
    withType <| EmailInput


password : InputConfig data msg -> InputConfig data msg
password =
    withType <| PasswordInput


textDisplay : InputConfig data msg -> InputConfig data msg
textDisplay =
    withType TextDisplay


date : InputConfig data msg -> InputConfig data msg
date =
    withType DateInput


number : InputConfig data msg -> InputConfig data msg
number =
    withType NumberInput


select : { options : List data, toId : data -> String, toLabel : data -> Html Never } -> InputConfig data msg -> InputConfig data msg
select selectOptions =
    withType (SelectInput selectOptions)


withExtraAttrs : List (Attribute Never) -> InputConfig data msg -> InputConfig data msg
withExtraAttrs extraAttrs { mandatory, optional } =
    { mandatory = mandatory, optional = { optional | extraAttrs = extraAttrs } }


type alias InputConfig data msg =
    { mandatory : MandatoryInputConfig msg
    , optional : OptionalInputConfig data msg
    }


type alias MandatoryInputConfig msg =
    { value : String
    , onInput : String -> msg
    , label : Html Never
    , name : String
    }


type alias OptionalInputConfig data msg =
    { disabled : Bool
    , readonly : Bool
    , validMsg : Maybe (List (Html msg))
    , errorMsg : Maybe (List (Html msg))
    , hint : List (Html Never)
    , icon : Maybe String
    , type_ : InputType data
    , extraAttrs : List (Attribute Never)
    }


defaultOptions : OptionalInputConfig data msg
defaultOptions =
    { disabled = False
    , readonly = False
    , validMsg = Nothing
    , errorMsg = Nothing
    , hint = []
    , icon = Nothing
    , type_ = TextInput
    , extraAttrs = []
    }


view : InputConfig data msg -> Html msg
view { mandatory, optional } =
    let
        { onInput, value } =
            mandatory

        name =
            "input-" ++ mandatory.name

        { errorMsg, validMsg, disabled, readonly, hint, icon, type_, extraAttrs } =
            optional

        defaultInputAttrs =
            [ Attr.class "fr-input"
            , Attr.classList
                [ ( "fr-input--valid", validMsg /= Nothing )
                , ( "fr-input--error", errorMsg /= Nothing )
                ]
            , Html.Attributes.Extra.attributeIf (Nothing /= validMsg) <|
                Aria.describedBy [ name ++ "-desc-valid" ]
            , Html.Attributes.Extra.attributeIf (Nothing /= errorMsg) <|
                Aria.describedBy [ name ++ "-desc-error" ]
            , Attr.id name
            , Attr.name name
            , Attr.value value
            , Attr.disabled disabled
            , Attr.readonly readonly
            , Events.onInput onInput
            , Attr.property "autocomplete" <| Encode.string name
            ]

        iconWrapper =
            case icon of
                Nothing ->
                    identity

                Just iconName ->
                    List.singleton
                        >> div [ Attr.class "fr-input-wrap", Attr.class iconName ]

        ( inp, cl ) =
            case type_ of
                TextInput ->
                    ( inputText name <|
                        (defaultInputAttrs ++ [ Attr.type_ "text" ])
                    , "input"
                    )

                EmailInput ->
                    ( inputText name <|
                        (defaultInputAttrs ++ [ Attr.type_ "email" ])
                    , "input"
                    )

                PasswordInput ->
                    ( inputText name <|
                        (defaultInputAttrs ++ [ Attr.type_ "password" ])
                    , "input"
                    )

                TextArea rows ->
                    ( textarea ((rows |> Maybe.map Attr.rows |> Maybe.withDefault empty) :: defaultInputAttrs) []
                    , "input"
                    )

                DateInput ->
                    ( Html.input (defaultInputAttrs ++ [ Attr.type_ "date" ]) []
                    , "input"
                    )

                NumberInput ->
                    ( inputNumber name <|
                        (defaultInputAttrs ++ [ Attr.type_ "number", Attr.attribute "inputmode" "numeric", Attr.pattern "[0-9]*" ])
                    , "input"
                    )

                TextDisplay ->
                    ( div [ Attr.class "mt-[0.5rem] py-[0.5rem]", DSFR.Typography.textBold ]
                        [ text <|
                            if value == "" then
                                "-"

                            else
                                value
                        ]
                    , "input"
                    )

                SelectInput { options, toId, toLabel } ->
                    ( Accessibility.select
                        (defaultInputAttrs
                            ++ [ Attr.class "fr-select"
                               ]
                        )
                      <|
                        List.map
                            (\s ->
                                option
                                    [ Attr.value <| toId s
                                    , Attr.name <| toId s
                                    , Attr.selected <| (==) value <| toId <| s
                                    ]
                                    [ static <| toLabel s ]
                            )
                        <|
                            options
                    , "select"
                    )
    in
    div
        ((Attr.class <| "fr-" ++ cl ++ "-group")
            :: Attr.classList
                [ ( "fr-" ++ cl ++ "-group--valid", Nothing /= validMsg )
                , ( "fr-" ++ cl ++ "-group--error", Nothing /= errorMsg )
                , ( "fr-" ++ cl ++ "-group--disabled", disabled )
                ]
            :: extraAttrs
        )
        [ static <|
            label
                [ Attr.class "fr-label"
                , Attr.for name
                ]
                [ mandatory.label
                , case hint of
                    [] ->
                        nothing

                    hints ->
                        span [ Attr.class "fr-hint-text" ] hints
                ]
        , iconWrapper <|
            inp
        , Html.Extra.viewMaybe
            (p
                [ Attr.id <| name ++ "-desc-valid"
                , Attr.class "fr-valid-text"
                ]
            )
            validMsg
        , Html.Extra.viewMaybe
            (p
                [ Attr.id <| name ++ "-desc-error"
                , Attr.class "fr-error-text"
                ]
            )
            errorMsg
        ]
