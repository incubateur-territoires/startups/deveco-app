module DSFR.CallOut exposing (callout)

import Accessibility exposing (Html, div, h3, p, text)
import DSFR.Icons
import Html.Attributes exposing (class)
import Html.Attributes.Extra exposing (empty)
import Html.Extra exposing (nothing)


callout : Maybe DSFR.Icons.IconName -> Maybe String -> Html msg -> Html msg
callout icon title content =
    div
        [ class "fr-callout"
        , case icon of
            Just iconName ->
                DSFR.Icons.toClass iconName

            Nothing ->
                empty
        ]
        [ case title of
            Just t ->
                h3 [ class "fr-callout__title" ] [ text t ]

            Nothing ->
                nothing
        , p [ class "fr-callout__text" ]
            [ content ]
        ]
