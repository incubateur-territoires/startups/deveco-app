const gateway = require("fast-gateway");
const server = gateway({
	routes: [
		{
			prefix: "api",
			target: "http://localhost:4000",
		},
		{
			prefix: "",
			target: "http://localhost:8888",
		},
	],
});

server.start(5000);
