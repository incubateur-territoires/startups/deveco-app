module UI.Cloture exposing (ClotureInput, clotureInputToCloture, default, form, update)

import Accessibility exposing (Html, div, text)
import DSFR.Input
import DSFR.Radio
import Data.Demande as Demande exposing (Cloture, Demande)
import Dict exposing (Dict)
import Html.Attributes exposing (class)


type alias ClotureInput =
    { demande : Demande
    , motif : String
    }


default : Demande -> ClotureInput
default demande =
    { demande = demande
    , motif = ""
    }


update : String -> ClotureInput -> ClotureInput
update value cloture =
    { cloture | motif = value }


form : (Demande -> msg) -> (String -> msg) -> List Demande -> ClotureInput -> Html msg
form updateDemande updateField demandes cloture =
    div [ class "flex flex-col gap-4" ]
        [ DSFR.Radio.group
            { id = "demandes-ouvertes"
            , options = demandes
            , current = Just cloture.demande
            , toLabel = Demande.type_ >> Demande.typeDemandeToDisplay >> text
            , toId = Demande.type_ >> Demande.toString >> (++) "type-demande-option-"
            , msg = updateDemande
            , legend =
                text <|
                    "Demande"
                        ++ (if List.length demandes > 1 then
                                "s"

                            else
                                ""
                           )
                        ++ " à clôturer"
            }
            |> DSFR.Radio.view
        , div [ class "fr-form-group" ]
            [ DSFR.Input.new
                { value = cloture.motif
                , onInput = updateField
                , label = text "Motif"
                , name = "nouvelle-fiche-particulier-motif-cloture"
                }
                |> DSFR.Input.textArea (Just 4)
                |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                |> DSFR.Input.view
            ]
        ]


clotureInputToCloture : ClotureInput -> Result (Dict String (List String)) Cloture
clotureInputToCloture input =
    Ok (\demande motif -> Demande.createCloture <| { demande = demande, motif = motif })
        |> keepErrors (Ok <| .demande <| input)
        |> keepErrors (Ok <| .motif <| input)


keepErrors : Result (Dict String (List error)) value -> Result (Dict String (List error)) (value -> nextStep) -> Result (Dict String (List error)) nextStep
keepErrors value function =
    case ( value, function ) of
        ( Ok v, Ok f ) ->
            Ok (f v)

        ( Err errs, Ok _ ) ->
            Err errs

        ( Ok _, Err errs ) ->
            Err errs

        ( Err errV, Err errF ) ->
            Err <| Dict.merge (\k v d -> Dict.insert k v d) (\k v1 v2 d -> Dict.insert k (v1 ++ v2) d) (\k v d -> Dict.insert k v d) errV errF Dict.empty
