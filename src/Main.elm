module Main exposing (main)

import Browser
import Pages.Activite
import Pages.Admin.Tags
import Pages.Admin.Territoires
import Pages.Admin.Utilisateurs
import Pages.Analysis
import Pages.Auth.Check
import Pages.Auth.Jwt.Uuid_
import Pages.CGU
import Pages.Connexion
import Pages.Createur.EditId_
import Pages.Createur.Id_
import Pages.Createur.New
import Pages.Createurs
import Pages.Dashboard
import Pages.Deconnexion
import Pages.Etablissement.New
import Pages.Etablissement.Siret_
import Pages.Etablissements
import Pages.Fiches
import Pages.Fiches.Id_
import Pages.Local.EditId_
import Pages.Local.Id_
import Pages.Local.New
import Pages.Locaux
import Pages.MentionsLegales
import Pages.NotFound
import Pages.Parametres.Collaborateurs
import Pages.Parametres.Import
import Pages.Parametres.Profil
import Pages.Parametres.SIG
import Pages.Parametres.Tags
import Pages.Parametres.Zonages
import Pages.PolitiqueConfidentialite
import Pages.Rappels
import Pages.Stats
import Route
import Shared
import Spa
import View


main =
    Spa.init
        { defaultView = View.defaultView
        , extractIdentity = Shared.identity
        }
        |> addPages
        |> Spa.application View.map
            { toRoute = Route.toRoute
            , init = Shared.init
            , update = Shared.update
            , subscriptions = Shared.subscriptions
            , toDocument = View.toDocument
            , protectPage = Route.toUrl >> Just >> Route.AuthCheck >> Route.toUrl
            }
        |> Browser.application


addPages spa =
    spa
        |> Spa.addPublicPage View.mappers Route.matchNotFound Pages.NotFound.page
        |> Spa.addProtectedPage View.mappers Route.matchFicheId Pages.Fiches.Id_.page
        |> Spa.addProtectedPage View.mappers Route.matchFiches Pages.Fiches.page
        |> Spa.addProtectedPage View.mappers Route.matchEtablissementNew Pages.Etablissement.New.page
        |> Spa.addProtectedPage View.mappers Route.matchCreateurEditId Pages.Createur.EditId_.page
        |> Spa.addProtectedPage View.mappers Route.matchCreateurId Pages.Createur.Id_.page
        |> Spa.addProtectedPage View.mappers Route.matchCreateurNew Pages.Createur.New.page
        |> Spa.addProtectedPage View.mappers Route.matchCreateurs Pages.Createurs.page
        |> Spa.addProtectedPage View.mappers Route.matchEtablissements Pages.Etablissements.page
        |> Spa.addProtectedPage View.mappers Route.matchEtablissementSiret Pages.Etablissement.Siret_.page
        |> Spa.addProtectedPage View.mappers Route.matchRappels Pages.Rappels.page
        |> Spa.addProtectedPage View.mappers Route.matchZonages Pages.Parametres.Zonages.page
        |> Spa.addProtectedPage View.mappers Route.matchCollaborateurs Pages.Parametres.Collaborateurs.page
        |> Spa.addProtectedPage View.mappers Route.matchProfil Pages.Parametres.Profil.page
        |> Spa.addProtectedPage View.mappers Route.matchSIG Pages.Parametres.SIG.page
        |> Spa.addProtectedPage View.mappers Route.matchTags Pages.Parametres.Tags.page
        |> Spa.addProtectedPage View.mappers Route.matchImport Pages.Parametres.Import.page
        |> Spa.addProtectedPage View.mappers Route.matchAdminUtilisateurs Pages.Admin.Utilisateurs.page
        |> Spa.addProtectedPage View.mappers Route.matchAdminTerritoires Pages.Admin.Territoires.page
        |> Spa.addProtectedPage View.mappers Route.matchAdminTags Pages.Admin.Tags.page
        |> Spa.addProtectedPage View.mappers Route.matchLocaux Pages.Locaux.page
        |> Spa.addProtectedPage View.mappers Route.matchLocalEditId Pages.Local.EditId_.page
        |> Spa.addProtectedPage View.mappers Route.matchLocalId Pages.Local.Id_.page
        |> Spa.addProtectedPage View.mappers Route.matchLocalNew Pages.Local.New.page
        |> Spa.addProtectedPage View.mappers Route.matchAnalysis Pages.Analysis.page
        |> Spa.addProtectedPage View.mappers Route.matchActivite Pages.Activite.page
        |> Spa.addPublicPage View.mappers Route.matchDashboard Pages.Dashboard.page
        |> Spa.addPublicPage View.mappers Route.matchConnexion Pages.Connexion.page
        |> Spa.addPublicPage View.mappers Route.matchDeconnexion Pages.Deconnexion.page
        |> Spa.addPublicPage View.mappers Route.matchAuthJwtUuid Pages.Auth.Jwt.Uuid_.page
        |> Spa.addPublicPage View.mappers Route.matchAuthCheck Pages.Auth.Check.page
        |> Spa.addPublicPage View.mappers Route.matchStats Pages.Stats.page
        |> Spa.addPublicPage View.mappers Route.matchCGU Pages.CGU.page
        |> Spa.addPublicPage View.mappers Route.matchMentionsLegales Pages.MentionsLegales.page
        |> Spa.addPublicPage View.mappers Route.matchPolitiqueConfidentialite Pages.PolitiqueConfidentialite.page
