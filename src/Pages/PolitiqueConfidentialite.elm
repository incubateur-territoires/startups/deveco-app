module Pages.PolitiqueConfidentialite exposing (page)

import Accessibility exposing (Html, a, div, h1, h2, li, p, strong, table, tbody, td, text, th, thead, tr, ul)
import Api.Auth
import DSFR.Grid
import Effect
import Html.Attributes exposing (class, href)
import Lib.Variables exposing (contactEmail)
import Route
import Shared
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


page : Shared.Shared -> Page () Shared.Msg (View ()) () ()
page { identity } =
    Spa.Page.element
        { init =
            \_ ->
                ( ()
                , case identity of
                    Nothing ->
                        Effect.fromSharedCmd <| Api.Auth.checkAuth True Nothing

                    _ ->
                        Effect.none
                )
                    |> Shared.pageChangeEffects
        , update = \_ _ -> ( (), Effect.none )
        , view = \_ -> view identity
        , subscriptions = \_ -> Sub.none
        }


view : Maybe Shared.User -> View ()
view identity =
    { title =
        UI.Layout.pageTitle <|
            "Politique de confidentialité - Deveco"
    , body = UI.Layout.lazyBody body identity
    , route = Route.Stats Nothing
    }


body : Maybe Shared.User -> Html ()
body _ =
    div [ class "p-4" ]
        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col12, class "fr-card--white !p-8" ]
                [ h1 []
                    [ text "Politique de confidentialité de Deveco" ]
                , h2 []
                    [ text "Qui est responsable de Deveco\u{00A0}?" ]
                , p []
                    [ text "« Deveco » est développé au sein de l’incubateur de l’Agence Nationale de la Cohésion des Territoires." ]
                , p []
                    [ text "Le responsable de traitement des données à caractère personnel collectées par le site « Deveco » est l’Agence Nationale de la Cohésion des Territoires, représentée par Monsieur Stanislas Bourron, Directeur général." ]
                , h2 []
                    [ text "Pourquoi manipulons-nous ces données\u{00A0}?" ]
                , p []
                    [ text "Le site Deveco peut traiter des données à caractère personnelles pour mettre à disposition des collectivités un ensemble de données fiables sur l’activité économique de son territoire." ]
                , h2 []
                    [ text "Quelles sont les données que nous manipulons ?" ]
                , p []
                    [ text "Le site peut traiter les données à caractère personnel suivantes :" ]
                , ul []
                    [ li []
                        [ text "Données relatives aux comptes « équipe » ;" ]
                    , li []
                        [ text "Données relatives aux comptes « collaborateurs » ;" ]
                    , li []
                        [ text "Données relatives aux entreprises (numéro de téléphone ou données de contact) ;" ]
                    , li []
                        [ text "Données relatives aux échanges avec les contacts des entreprises (champ libre) ;" ]
                    , li []
                        [ text "Données relatives aux personnes physiques projetant de créer des entreprises (adresse e-mail de la personne physique)." ]
                    ]
                , h2 []
                    [ text "Qu’est-ce qui nous autorise à manipuler ces données ?" ]
                , p []
                    [ text "Deveco manipule des données traitées car elle réalise une mission d’intérêt public." ]
                , p []
                    [ text "Cette mission d’intérêt public est notamment prévue par l’article L.1231-2, IV du Code général des collectivités territoriales qui dispose qu’elle a pour rôle de favoriser l’aménagement et la restructuration des espaces commerciaux et artisanaux dans les territoires, notamment les territoires défavorisés économiquement." ]
                , p []
                    [ text "En outre, selon l’article L.1231-2, V du Code générale des collectivités territoriales elle a pour mission d’impulser et d’aider à concevoir les projets et initiatives portées par l’Etat et les collectivités territoriales dans le domaine du numérique." ]
                , h2 []
                    [ text "Pendant combien de temps conservons-nous ces données ?" ]
                , div
                    [ class "fr-table fr-table--bordered"
                    ]
                    [ table []
                        [ thead []
                            [ tr []
                                [ th []
                                    [ text "Types de données" ]
                                , th []
                                    [ text "Durée de conservation" ]
                                ]
                            ]
                        , tbody []
                            [ tr []
                                [ td []
                                    [ text "Données relatives aux comptes « équipe »" ]
                                , td []
                                    [ text "1 an après la désactivation du compte" ]
                                ]
                            , tr []
                                [ td []
                                    [ text "Données relatives aux comptes « collaborateurs »" ]
                                , td []
                                    [ text "1 an après la désactivation du compte" ]
                                ]
                            , tr []
                                [ td []
                                    [ text "Données relatives aux entreprises" ]
                                , td []
                                    [ text "6 ans après l’archivage de la fiche" ]
                                ]
                            , tr []
                                [ td []
                                    [ text "Données relatives aux échanges avec les contacts des entreprises" ]
                                , td []
                                    [ text "6 ans après l’archivage de la fiche" ]
                                ]
                            , tr []
                                [ td []
                                    [ text "Données relatives aux personnes physiques projetant de créer des entreprises" ]
                                , td []
                                    [ text "2 ans après l’archivage de la fiche" ]
                                ]
                            ]
                        ]
                    ]
                , h2 []
                    [ text "Quels droits avez-vous ?" ]
                , p []
                    [ text "Vous disposez des droits suivants concernant vos données à caractère personnel :" ]
                , ul []
                    [ li []
                        [ text "Droit d’information et droit d’accès aux données ;" ]
                    , li []
                        [ text "Droit d’opposition au traitement de données ;" ]
                    , li []
                        [ text "Droit de rectification des données ;" ]
                    , li []
                        [ text "Droit à la limitation des données." ]
                    ]
                , p []
                    [ text "Pour les exercer, faites-nous parvenir une demande en précisant la date et l’heure précise de la requête – ces éléments sont indispensables pour nous permettre de retrouver votre recherche – par voie électronique à l’adresse suivante :" ]
                , p []
                    [ a
                        [ href <| "mailto:" ++ contactEmail
                        ]
                        [ text contactEmail ]
                    ]
                , p []
                    [ text "En raison de l'obligation de sécurité et de confidentialité dans le traitement des données à caractère personnel qui incombe au responsable de traitement, les demandes des personnes concernées ne seront traitées que si nous sommes en mesure de vous identifier de façon certaine." ]
                , p []
                    [ text "En cas de doute sérieux sur votre identité, nous pouvons être amenés à vous demander la communication d’une preuve d’identité." ]
                , p []
                    [ text "Pour vous aider dans votre démarche, vous trouverez "
                    , a
                        [ href "https://www.cnil.fr/fr/modele/courrier/exercer-son-droit-dacces"
                        ]
                        [ text "ici un modèle de courrier élaboré par la CNIL" ]
                    , text "."
                    ]
                , p []
                    [ text "Le responsable de traitement s’engage à répondre dans un délai raisonnable qui ne saurait dépasser 1 mois à compter de la réception de votre demande." ]
                , h2 []
                    [ text "Qui va avoir accès à ces données ?" ]
                , p []
                    [ text "Les données sont accessibles aux seules personnes autorisés parmi les utilisateurs." ]
                , h2 []
                    [ text "Quelles mesures de sécurité mettons-nous en place ?" ]
                , p []
                    [ text "Les mesures techniques et organisationnelles de sécurité adoptées pour assurer la confidentialité, l’intégrité et protéger l’accès des données sont notamment :" ]
                , ul []
                    [ li []
                        [ text "Anonymisation" ]
                    , li []
                        [ text "Stockage des données en base de données" ]
                    , li []
                        [ text "Stockage des mots de passe en base sont hâchés" ]
                    , li []
                        [ text "Cloisonnement des données" ]
                    , li []
                        [ text "Mesures de traçabilité" ]
                    , li []
                        [ text "Surveillance" ]
                    , li []
                        [ text "Protection contre les virus, malwares et logiciels espions" ]
                    , li []
                        [ text "Protection des réseaux" ]
                    , li []
                        [ text "Sauvegarde" ]
                    , li []
                        [ text "Mesures restrictives limitant l’accès physiques aux données à caractère personnel" ]
                    ]
                , h2 []
                    [ text "Qui nous aide à manipuler ces données ?" ]
                , p []
                    [ text "Certaines des données sont envoyées à des sous-traitants pour réaliser certaines missions. Le responsable de traitement s'est assuré de la mise en œuvre par ses sous-traitants de garanties adéquates et du respect de conditions strictes de confidentialité, d’usage et de protection des données." ]
                , div
                    [ class "fr-table fr-table--bordered"
                    ]
                    [ table []
                        [ thead []
                            [ tr []
                                [ th []
                                    [ text "Partenaire" ]
                                , th []
                                    [ text "Pays destinataire" ]
                                , th []
                                    [ text "Traitement réalisé" ]
                                , th []
                                    [ text "Garanties" ]
                                ]
                            ]
                        , tbody []
                            [ tr []
                                [ td []
                                    [ text "Scaleway" ]
                                , td []
                                    [ text "France" ]
                                , td []
                                    [ text "Hébergement" ]
                                , td []
                                    [ a
                                        [ href "https://website-production-uploads.s3.fr-par.scw.cloud/DPA_030921_fc856ff6e8.pdf?updated_at=2022-05-24T07:21:41.437Z"
                                        ]
                                        [ text "https://website-production-uploads.s3.fr-par.scw.cloud/DPA_030921_fc856ff6e8.pdf?updated_at=2022-05-24T07:21:41.437Z" ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                , h2 []
                    [ text "Cookies" ]
                , p []
                    [ text "Un cookie est un fichier déposé sur votre terminal lors de la visite d’un site. Il a pour but de collecter des informations relatives à votre navigation et de vous adresser des services adaptés à votre terminal (ordinateur, mobile ou tablette)." ]
                , p []
                    [ text "Le site dépose des cookies de mesure d’audience (nombre de visites, pages consultées), respectant les conditions d’exemption du consentement de l’internaute définies par la recommandation « Cookies » de la Commission nationale informatique et libertés (CNIL). Cela signifie, notamment, que ces cookies ne servent qu’à la production de statistiques anonymes et ne permettent pas de suivre la navigation de l’internaute sur d’autres sites." ]
                , p []
                    [ strong []
                        [ text "Nous utilisons pour cela Matomo" ]
                    , text ", un outil de mesure d’audience web libre, paramétré pour être en conformité avec la recommandation « Cookies » de la CNIL. Cela signifie que votre adresse IP, par exemple, est anonymisée avant d’être enregistrée. Il est donc impossible d’associer vos visites sur ce site à votre personne."
                    , text " "
                    , strong []
                        [ text "Ces cookies ne peuvent être conservés plus de 13 mois." ]
                    ]
                , p []
                    [ text "Il convient d’indiquer que :" ]
                , ul []
                    [ li []
                        [ text "Les données collectées ne sont pas recoupées avec d’autres traitements" ]
                    , li []
                        [ text "Les cookies ne permettent pas de suivre la navigation de l’internaute sur d’autres sites" ]
                    ]
                , p []
                    [ text "À tout moment, vous pouvez refuser l’utilisation des cookies et désactiver le dépôt sur votre ordinateur en utilisant la fonction dédiée de votre navigateur (fonction disponible notamment sur Microsoft Internet Explorer 11, Google Chrome, Mozilla Firefox, Apple Safari et Opera)." ]
                , p []
                    [ text "Pour aller plus loin, vous pouvez consulter les fiches proposées par la Commission Nationale de l'Informatique et des Libertés (CNIL) :" ]
                , ul []
                    [ li []
                        [ a
                            [ href "https://www.cnil.fr/fr/cookies-traceurs-que-dit-la-loi"
                            ]
                            [ text "Cookies & traceurs : que dit la loi ?" ]
                        ]
                    , li []
                        [ a
                            [ href "https://www.cnil.fr/fr/cookies-les-outils-pour-les-maitriser"
                            ]
                            [ text "Cookies : les outils pour les maîtriser" ]
                        ]
                    ]
                ]
            ]
        ]
