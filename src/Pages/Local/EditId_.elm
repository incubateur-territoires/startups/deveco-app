module Pages.Local.EditId_ exposing (Model, Msg(..), page)

import Accessibility exposing (Html, div, formWithListeners, h1, span, sup, text)
import Api
import Api.Auth
import Api.EntityId exposing (EntityId)
import DSFR.Alert
import DSFR.Button
import DSFR.Checkbox
import DSFR.Icons
import DSFR.Icons.System
import DSFR.Input
import DSFR.Radio
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Adresse exposing (ApiAdresse, decodeApiFeatures)
import Data.Local as Local exposing (Local, LocalId)
import Dict exposing (Dict)
import Effect
import Html.Attributes exposing (class)
import Html.Events as Events
import Html.Extra exposing (nothing)
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import Lib.Variables exposing (hintZoneGeographique)
import MultiSelectRemote exposing (SelectConfig)
import RemoteData as RD exposing (WebData)
import Route
import Shared
import SingleSelectRemote
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page (EntityId LocalId) Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.element <|
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


type alias Model =
    { localSource : WebData Local
    , localId : EntityId LocalId
    , local : DataLocal
    , selectLocalisations : MultiSelectRemote.SmartSelect Msg String
    , selectAdresse : SingleSelectRemote.SmartSelect Msg ApiAdresse
    , selectedAdresse : Maybe ApiAdresse
    , validationLocal : RD.RemoteData FormErrors ()
    , enregistrementLocal : WebData Local
    }


init : EntityId LocalId -> ( Model, Effect.Effect Shared.Msg Msg )
init id =
    ( { localSource = RD.Loading
      , localId = id
      , local = defaultDataLocal
      , selectLocalisations =
            MultiSelectRemote.init selectLocalisationsId
                { selectionMsg = SelectedLocalisations
                , internalMsg = UpdatedSelectLocalisations
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectAdresse =
            SingleSelectRemote.init "champ-selection-adresse"
                { selectionMsg = SelectedAdresse
                , internalMsg = UpdatedSelectAdresse
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectedAdresse = Nothing
      , validationLocal = RD.NotAsked
      , enregistrementLocal = RD.NotAsked
      }
    , Effect.fromCmd <| fetchLocal id
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ model |> .selectAdresse |> SingleSelectRemote.subscriptions
        , model |> .selectLocalisations |> MultiSelectRemote.subscriptions
        ]


apiAdresseToOption : ApiAdresse -> String
apiAdresseToOption =
    .label


selectCharacterThreshold : Int
selectCharacterThreshold =
    2


selectDebounceDuration : Float
selectDebounceDuration =
    400


selectLocalisationsId : String
selectLocalisationsId =
    "champ-selection-localisations"


selectLocalisationsConfig : SelectConfig String
selectLocalisationsConfig =
    { headers = []
    , url = Api.rechercheLocalisation
    , optionDecoder = Decode.list <| Decode.field "localisation" Decode.string
    }


fetchLocal : EntityId LocalId -> Cmd Msg
fetchLocal id =
    Api.Auth.get
        { url = Api.getLocal id }
        SharedMsg
        ReceivedLocal
        Local.decodeLocal


defaultDataLocal : DataLocal
defaultDataLocal =
    { titre = ""
    , adresse = ""
    , ville = ""
    , codePostal = ""
    , geolocation = Nothing
    , statut = Local.Occupe
    , types = []
    , surface = ""
    , loyer = ""
    , localisations = []
    , commentaire = ""
    }


type alias DataLocal =
    { titre : String
    , adresse : String
    , ville : String
    , codePostal : String
    , geolocation : Maybe String
    , statut : Local.LocalStatut
    , types : List Local.LocalType
    , surface : String
    , loyer : String
    , localisations : List String
    , commentaire : String
    }


localToDataLocal : Local -> DataLocal
localToDataLocal local =
    { titre = local.titre
    , adresse = local.adresse
    , ville = local.ville
    , codePostal = local.codePostal
    , geolocation = local.geolocation
    , statut = local.statut
    , types = local.types
    , surface = local.surface
    , loyer = local.loyer
    , localisations = local.localisations
    , commentaire = local.commentaire
    }


type Msg
    = ReceivedLocal (WebData Local)
    | UpdatedLocalForm LocalField String
    | SetStatut Local.LocalStatut
    | ToggleType Bool Local.LocalType
    | SelectedLocalisations ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectLocalisations (MultiSelectRemote.Msg String)
    | UnselectLocalisation String
    | SelectedAdresse ( ApiAdresse, SingleSelectRemote.Msg ApiAdresse )
    | UpdatedSelectAdresse (SingleSelectRemote.Msg ApiAdresse)
    | RequestedSaveLocal
    | ReceivedSaveLocal (WebData Local)
    | SharedMsg Shared.Msg


type LocalField
    = LocalTitre
    | LocalSurface
    | LocalLoyer
    | LocalCommentaire


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        ReceivedLocal response ->
            let
                local =
                    response
                        |> RD.map localToDataLocal
                        |> RD.withDefault defaultDataLocal

                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.setText local.adresse selectConfig model.selectAdresse
            in
            ( { model | selectAdresse = updatedSelect, localSource = response, local = local }, Effect.fromCmd selectCmd )

        UpdatedLocalForm field value ->
            ( { model | local = updateLocalForm field value model.local }, Effect.none )

        SetStatut statut ->
            ( { model | local = model.local |> (\l -> { l | statut = statut }) }, Effect.none )

        ToggleType on type_ ->
            let
                newModel =
                    { model
                        | local =
                            model.local
                                |> (\local ->
                                        { local
                                            | types =
                                                if on then
                                                    if List.member type_ local.types then
                                                        local.types

                                                    else
                                                        local.types |> List.reverse |> (::) type_ |> List.reverse

                                                else
                                                    List.filter ((/=) type_) local.types
                                        }
                                   )
                    }
            in
            ( newModel, Effect.none )

        ReceivedSaveLocal response ->
            case response of
                RD.Success _ ->
                    ( { model | enregistrementLocal = response }
                    , Effect.fromShared <|
                        Shared.Navigate <|
                            Route.Local <|
                                model.localId
                    )

                _ ->
                    ( { model | enregistrementLocal = response }, Effect.none )

        RequestedSaveLocal ->
            let
                parsedLocal =
                    validateLocal model

                ( enregistrementLocal, effect ) =
                    case parsedLocal of
                        RD.Success () ->
                            ( RD.Loading, updateLocal model )

                        _ ->
                            ( RD.NotAsked, Effect.none )
            in
            ( { model | validationLocal = parsedLocal, enregistrementLocal = enregistrementLocal }, effect )

        SelectedAdresse ( apiAdresse, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectConfig model.selectAdresse

                local =
                    model.local
                        |> (\p ->
                                { p
                                    | ville = apiAdresse.city
                                    , codePostal = apiAdresse.postcode
                                    , adresse = apiAdresse.label
                                    , geolocation = Maybe.map2 (\x y -> "(" ++ String.fromFloat x ++ "," ++ String.fromFloat y ++ ")") apiAdresse.x apiAdresse.y
                                }
                           )
            in
            ( { model | selectedAdresse = Just apiAdresse, selectAdresse = updatedSelect, local = local }, Effect.fromCmd selectCmd )

        UpdatedSelectAdresse sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectConfig model.selectAdresse
            in
            ( { model | selectAdresse = updatedSelect }, Effect.fromCmd selectCmd )

        SelectedLocalisations ( localisations, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectLocalisationsConfig model.selectLocalisations

                localisationsUniques =
                    case localisations of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                localisations
            in
            ( { model
                | selectLocalisations = updatedSelect
                , local =
                    model.local
                        |> (\l ->
                                { l | localisations = localisationsUniques }
                           )
              }
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectLocalisations sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectLocalisationsConfig model.selectLocalisations
            in
            ( { model
                | selectLocalisations = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectLocalisation localisation ->
            ( { model
                | local =
                    model.local
                        |> (\l ->
                                { l
                                    | localisations =
                                        l.localisations
                                            |> List.filter ((/=) localisation)
                                }
                           )
              }
            , Effect.none
            )

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg


selectConfig : SelectConfig ApiAdresse
selectConfig =
    { headers = []
    , url = Api.rechercheAdresse Nothing Nothing Nothing
    , optionDecoder = decodeApiFeatures
    }


type alias FormErrors =
    { global : Maybe String, fields : Dict String (List String) }


formErrorsFor : String -> FormErrors -> Maybe (List String)
formErrorsFor key { fields } =
    fields
        |> Dict.get key


validateLocal : Model -> RD.RemoteData FormErrors ()
validateLocal { local } =
    if String.trim local.titre == "" || String.trim local.adresse == "" then
        let
            errors =
                [ ( "titre", .titre, "Ce champ est obligatoire" )
                , ( "adresse", .adresse, "Ce champ est obligatoire" )
                ]
                    |> List.foldl
                        (\( key, accessor, err ) dict ->
                            if accessor local == "" then
                                Dict.update key
                                    (\existingList ->
                                        case existingList of
                                            Just list ->
                                                Just <| list ++ [ err ]

                                            Nothing ->
                                                Just [ err ]
                                    )
                                    dict

                            else
                                dict
                        )
                        Dict.empty
        in
        RD.Failure
            { global =
                if Dict.isEmpty errors then
                    Nothing

                else
                    Just "Veuillez corriger les erreurs."
            , fields = errors
            }

    else
        RD.Success ()


updateLocalForm : LocalField -> String -> DataLocal -> DataLocal
updateLocalForm field value local =
    case field of
        LocalTitre ->
            { local | titre = value }

        LocalSurface ->
            { local | surface = value }

        LocalLoyer ->
            { local | loyer = value }

        LocalCommentaire ->
            { local | commentaire = value }


updateLocal : Model -> Effect.Effect Shared.Msg Msg
updateLocal { local, localId } =
    let
        encodedLocal =
            local
                |> (\{ titre, adresse, ville, codePostal, geolocation, statut, types, surface, loyer, localisations, commentaire } ->
                        Encode.object
                            [ ( "local"
                              , Encode.object
                                    [ ( "titre", Encode.string titre )
                                    , ( "adresse", Encode.string adresse )
                                    , ( "ville", Encode.string ville )
                                    , ( "codePostal", Encode.string codePostal )
                                    , ( "geolocation", Maybe.withDefault Encode.null <| Maybe.map Encode.string <| geolocation )
                                    , ( "localStatut", Local.encodeLocalStatut statut )
                                    , ( "localTypes", Encode.list Local.encodeLocalType types )
                                    , ( "surface", Encode.string surface )
                                    , ( "loyer", Encode.string loyer )
                                    , ( "localisations", Encode.list Encode.string localisations )
                                    , ( "commentaire", Encode.string commentaire )
                                    ]
                              )
                            ]
                   )
                |> Http.jsonBody
    in
    Api.Auth.post
        { url = Api.updateLocal localId
        , body = encodedLocal
        }
        SharedMsg
        ReceivedSaveLocal
        Local.decodeLocal
        |> Effect.fromCmd


view : Model -> View Msg
view model =
    { title =
        UI.Layout.pageTitle <|
            RD.withDefault "Modification" <|
                RD.map (\local -> "Modification de " ++ local.titre) <|
                    model.localSource
    , body = UI.Layout.lazyBody body model
    , route = Route.LocalNew
    }


localTypeForm : Model -> Html Msg
localTypeForm model =
    case model.localSource of
        RD.NotAsked ->
            text "Une erreur s'est produite"

        RD.Loading ->
            text "Chargement en cours"

        RD.Failure _ ->
            div [ class "mb-6 flex flex-col gap-4" ]
                [ DSFR.Alert.medium { title = "Une erreur s'est produite, veuillez nous contacter.", description = Nothing }
                    |> DSFR.Alert.alert Nothing DSFR.Alert.error
                ]

        RD.Success _ ->
            let
                formErrors =
                    case model.validationLocal of
                        RD.Failure errors ->
                            Just errors

                        _ ->
                            Nothing
            in
            div [ class "flex flex-col gap-8" ]
                [ div [ class "flex flex-col gap-4" ]
                    [ div [ class "flex flex-col sm:max-w-[60%]" ]
                        [ div [ class "flex flex-row sm:flex-nowrap flex-wrap gap-4 w-full" ]
                            [ DSFR.Input.new
                                { value = model.local.titre
                                , onInput = UpdatedLocalForm LocalTitre
                                , label = text "Titre *"
                                , name = "nouveau-local-titre"
                                }
                                |> DSFR.Input.withError (formErrors |> Maybe.andThen (formErrorsFor "titre") |> Maybe.map (List.map text))
                                |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                                |> DSFR.Input.view
                            ]
                        , span [ class "my-4" ]
                            [ model.selectAdresse
                                |> SingleSelectRemote.viewCustom
                                    { isDisabled = False
                                    , selected = model.selectedAdresse
                                    , optionLabelFn = apiAdresseToOption
                                    , optionDescriptionFn = \_ -> ""
                                    , optionsContainerMaxHeight = 300
                                    , selectTitle = text "Adresse *"
                                    , searchPrompt = "Rechercher une adresse"
                                    , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                    , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                    , noResultsForMsg = \searchText -> "Aucune autre adresse n'a été trouvée pour " ++ searchText
                                    , noOptionsMsg = "Aucune adresse n'a été trouvée"
                                    , error = formErrors |> Maybe.andThen (formErrorsFor "adresse") |> Maybe.map (List.map text)
                                    }
                            ]
                        , DSFR.Radio.group
                            { id = "radio-statut"
                            , options = [ Local.Occupe, Local.Vacant, Local.SansObjet ]
                            , current = Just model.local.statut
                            , toLabel =
                                (\s ->
                                    case s of
                                        Local.Occupe ->
                                            "Local occupé"

                                        Local.Vacant ->
                                            "Local vacant"

                                        Local.SansObjet ->
                                            "Sans objet"
                                )
                                    >> text
                            , toId = Local.localStatutToString
                            , msg = SetStatut
                            , legend = text "Statut du local"
                            }
                            |> DSFR.Radio.inline
                            |> DSFR.Radio.withExtraAttrs [ class "sm:max-w-[60%]" ]
                            |> DSFR.Radio.view
                        , DSFR.Checkbox.group
                            { id = "filtres-locaux-types"
                            , label = span [] [ text "Type de local" ]
                            , onChecked =
                                \filter bool ->
                                    ToggleType bool <| filter
                            , values =
                                [ Local.Commerce
                                , Local.Bureaux
                                , Local.AteliersArtisanaux
                                , Local.BatimentsIndustriels
                                , Local.Entrepot
                                , Local.Terrain
                                ]
                            , checked = model.local.types
                            , valueAsString = Local.localTypeToString
                            , toId = Local.localTypeToString
                            , toLabel = Local.localTypeToDisplay
                            }
                            |> DSFR.Checkbox.inline
                            |> DSFR.Checkbox.viewGroup
                        , div [ class "flex flex-row sm:flex-nowrap flex-wrap gap-4 w-full" ]
                            [ DSFR.Input.new
                                { value = model.local.surface
                                , onInput = UpdatedLocalForm LocalSurface
                                , label = text "Surface totale (m²)"
                                , name = "nouveau-local-surface"
                                }
                                |> DSFR.Input.withError (formErrors |> Maybe.andThen (formErrorsFor "surface") |> Maybe.map (List.map text))
                                |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                                |> DSFR.Input.view
                            , DSFR.Input.new
                                { value = model.local.loyer
                                , onInput = UpdatedLocalForm LocalLoyer
                                , label = text "Loyer annuel (€)"
                                , name = "nouveau-local-loyer"
                                }
                                |> DSFR.Input.withError (formErrors |> Maybe.andThen (formErrorsFor "loyer") |> Maybe.map (List.map text))
                                |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                                |> DSFR.Input.view
                            ]
                        ]
                    ]
                , div []
                    [ div [ class "flex flex-col gap-4 sm:max-w-[60%]" ]
                        [ model.selectLocalisations
                            |> MultiSelectRemote.viewCustom
                                { isDisabled = False
                                , selected = model.local.localisations
                                , optionLabelFn = identity
                                , optionDescriptionFn = \_ -> ""
                                , optionsContainerMaxHeight = 300
                                , selectTitle = span [] [ text "Zone géographique", sup [ Html.Attributes.title hintZoneGeographique ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                , viewSelectedOptionFn = text
                                , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                , noResultsForMsg = \searchText -> "Aucune autre zone géographique n'a été trouvée pour " ++ searchText
                                , noOptionsMsg = "Aucune zone géographique n'a été trouvée"
                                , newOption = Just identity
                                }
                        , model.local.localisations
                            |> List.map (\localisation -> DSFR.Tag.deletable UnselectLocalisation { data = localisation, toString = identity })
                            |> DSFR.Tag.medium
                        ]
                    ]
                , div [ class "flex flex-col sm:max-w-[60%]" ]
                    [ DSFR.Input.new
                        { value = model.local.commentaire
                        , onInput = UpdatedLocalForm LocalCommentaire
                        , label = text "Commentaires (commerces et transports à proximité, restriction d'activité, etc.)"
                        , name = "nouveau-local-activite"
                        }
                        |> DSFR.Input.withError (formErrors |> Maybe.andThen (formErrorsFor "activite") |> Maybe.map (List.map text))
                        |> DSFR.Input.textArea (Just 8)
                        |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                        |> DSFR.Input.view
                    ]
                ]


body : Model -> Html Msg
body model =
    div [ class "fr-card--white p-4 sm:p-8 flex flex-col gap-8" ]
        [ h1 [ class "!mb-0" ] [ text "Modifier la fiche du local" ]
        , formWithListeners [ Events.onSubmit <| RequestedSaveLocal, class "flex flex-col gap-8" ]
            [ localTypeForm model
            , footer model
            ]
        ]


footer : Model -> Html Msg
footer model =
    div [ class "flex flex-col gap-2" ]
        [ div [ class "flex flex-row justify-end" ]
            [ case model.validationLocal of
                RD.Failure _ ->
                    DSFR.Alert.small { title = Nothing, description = text "Le formulaire comporte des erreurs, veuillez les corriger." }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.info

                _ ->
                    nothing
            , case model.enregistrementLocal of
                RD.Success _ ->
                    DSFR.Alert.small { title = Nothing, description = text "Modifications enregistrées\u{00A0}!" }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.success

                RD.Failure _ ->
                    DSFR.Alert.small { title = Nothing, description = text "Une erreur s'est produite, veuillez réessayer." }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.error

                _ ->
                    nothing
            ]
        , DSFR.Button.group
            [ DSFR.Button.new { onClick = Nothing, label = "Enregistrer" }
                |> DSFR.Button.submit
                |> DSFR.Button.withDisabled (model.enregistrementLocal == RD.Loading)
            , DSFR.Button.new { onClick = Nothing, label = "Précédent" }
                |> DSFR.Button.secondary
                |> DSFR.Button.linkButton (Route.toUrl <| Route.Local model.localId)
            ]
            |> DSFR.Button.inline
            |> DSFR.Button.alignedRightInverted
            |> DSFR.Button.viewGroup
        ]
