module Pages.Stats exposing (Model, Msg, page)

import Accessibility exposing (Html, div, h1, h2, h3, span, sup, text)
import Api
import Api.Auth
import DSFR.Grid
import DSFR.Icons exposing (iconSM)
import DSFR.Icons.System exposing (informationLine)
import DSFR.Radio
import DSFR.Typography as Typo
import Effect
import Html.Attributes exposing (class, title)
import Html.Extra exposing (viewMaybe)
import Html.Lazy
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import QS
import RemoteData as RD exposing (WebData)
import Route
import Shared
import Spa.Page exposing (Page)
import UI.Layout
import User
import View exposing (View)


type alias Model =
    { stats : WebData Stats
    , typeTerritoire : TypeTerritoire
    , periode : Periode
    }


type alias Stats =
    { territoires :
        { total : Maybe Int
        , nouveaux : Maybe Int
        , actifs : Maybe Int
        }
    , devecos :
        { total : Maybe Int
        , nouveaux : Maybe Int
        , actifs : Maybe Int
        }
    , zone :
        { devecos :
            { total : Maybe Int
            , nouveaux : Maybe Int
            , actifs : Maybe Int
            }
        , fiches :
            { total : Maybe Int
            , nouvelles : Maybe Int
            , modifiees : Maybe Int
            }
        , echanges :
            { total : Maybe Int
            , nouveaux : Maybe Int
            }
        , demandes :
            { total : Maybe Int
            , nouvelles : Maybe Int
            , fermees : Maybe Int
            }
        }
    }


type Periode
    = AllTime
    | LastMonth
    | LastWeek


type TypeTerritoire
    = France
    | Commune
    | EPCI


typeTerritoireToString : TypeTerritoire -> String
typeTerritoireToString t =
    case t of
        France ->
            "france"

        Commune ->
            "commune"

        EPCI ->
            "epci"


typeTerritoireToDisplay : TypeTerritoire -> String
typeTerritoireToDisplay t =
    case t of
        France ->
            "la France entière"

        Commune ->
            "les communes"

        EPCI ->
            "les EPCIs"


type Msg
    = ReceivedStats (WebData Stats)
    | ChangedPeriode Periode
    | ChangedTerritoire TypeTerritoire
    | GetStats ( TypeTerritoire, Periode )


page : Shared.Shared -> Page (Maybe String) Shared.Msg (View Msg) Model Msg
page { identity } =
    Spa.Page.onNewFlags (parseRawQuery >> GetStats) <|
        Spa.Page.element
            { view = view
            , init = init identity
            , update = update
            , subscriptions = \_ -> Sub.none
            }


init : Maybe User.User -> Maybe String -> ( Model, Effect.Effect Shared.Msg Msg )
init identity query =
    let
        ( territoire, periode ) =
            parseRawQuery query
    in
    ( { stats = RD.Loading
      , typeTerritoire = territoire
      , periode = periode
      }
    , Effect.batch
        [ getStats territoire periode
        , case identity of
            Nothing ->
                Effect.fromSharedCmd <| Api.Auth.checkAuth True Nothing

            _ ->
                Effect.none
        ]
    )
        |> Shared.pageChangeEffects


parseRawQuery : Maybe String -> ( TypeTerritoire, Periode )
parseRawQuery rawQuery =
    case rawQuery of
        Nothing ->
            ( France, AllTime )

        Just query ->
            ( parseTerritoire query, parsePeriode query )


parseTerritoire : String -> TypeTerritoire
parseTerritoire rawQuery =
    QS.parse QS.config rawQuery
        |> (\query ->
                let
                    type_ =
                        query
                            |> QS.getAsStringList "type"
                            |> List.head
                            |> Maybe.withDefault ""
                in
                case type_ of
                    "commune" ->
                        Commune

                    "epci" ->
                        EPCI

                    _ ->
                        France
           )


parsePeriode : String -> Periode
parsePeriode rawQuery =
    QS.parse QS.config rawQuery
        |> (\query ->
                let
                    periode =
                        query
                            |> QS.getAsStringList "periode"
                            |> List.head
                            |> Maybe.withDefault ""
                in
                case periode of
                    "semaine" ->
                        LastWeek

                    "mois" ->
                        LastMonth

                    "tout" ->
                        AllTime

                    _ ->
                        AllTime
           )


getStats : TypeTerritoire -> Periode -> Effect.Effect sharedMsg Msg
getStats territoire periode =
    let
        serializedQuery =
            serializeQuery territoire periode
    in
    Effect.fromCmd <|
        Http.get
            { url = Api.getStatsData <| serializedQuery
            , expect = Http.expectJson (RD.fromResult >> ReceivedStats) <| decodeStats
            }


serializeQuery : TypeTerritoire -> Periode -> String
serializeQuery typeTerritoire periode =
    QS.serialize (QS.config |> QS.addQuestionMark False) <|
        ((typeTerritoireToString >> QS.setStr "type") <| typeTerritoire) <|
            (QS.setStr "periode" <| periodeToString <| periode) <|
                QS.empty


periodeToString : Periode -> String
periodeToString periode =
    case periode of
        AllTime ->
            "tout"

        LastMonth ->
            "mois"

        LastWeek ->
            "semaine"


periodeToDisplay : Periode -> String
periodeToDisplay periode =
    case periode of
        AllTime ->
            "Depuis le début"

        LastMonth ->
            "Les 30 derniers jours"

        LastWeek ->
            "Les 7 derniers jours"


decodeStats : Decoder Stats
decodeStats =
    Decode.succeed Stats
        |> andMap (Decode.field "territoires" decodeTerritoiresStats)
        |> andMap (Decode.field "devecos" decodeDevecoStats)
        |> andMap (Decode.field "zone" decodeZoneStats)


decodeTerritoiresStats : Decoder { total : Maybe Int, nouveaux : Maybe Int, actifs : Maybe Int }
decodeTerritoiresStats =
    Decode.succeed (\total nouveaux actifs -> { total = total, nouveaux = nouveaux, actifs = actifs })
        |> andMap (optionalNullableField "total" Decode.int)
        |> andMap (optionalNullableField "nouveaux" Decode.int)
        |> andMap (optionalNullableField "actifs" Decode.int)


decodeDevecoStats : Decoder { total : Maybe Int, nouveaux : Maybe Int, actifs : Maybe Int }
decodeDevecoStats =
    Decode.succeed (\total nouveaux actifs -> { total = total, nouveaux = nouveaux, actifs = actifs })
        |> andMap (optionalNullableField "total" Decode.int)
        |> andMap (optionalNullableField "nouveaux" Decode.int)
        |> andMap (optionalNullableField "actifs" Decode.int)


decodeZoneStats : Decoder { devecos : { total : Maybe Int, nouveaux : Maybe Int, actifs : Maybe Int }, fiches : { total : Maybe Int, nouvelles : Maybe Int, modifiees : Maybe Int }, echanges : { total : Maybe Int, nouveaux : Maybe Int }, demandes : { total : Maybe Int, nouvelles : Maybe Int, fermees : Maybe Int } }
decodeZoneStats =
    Decode.succeed
        (\devecos fiches echanges demandes ->
            { devecos = devecos
            , fiches = fiches
            , echanges = echanges
            , demandes = demandes
            }
        )
        |> andMap (Decode.field "devecos" decodeDevecos)
        |> andMap (Decode.field "fiches" decodeFiches)
        |> andMap (Decode.field "echanges" decodeEchanges)
        |> andMap (Decode.field "demandes" decodeDemandes)


decodeDevecos : Decoder { total : Maybe Int, nouveaux : Maybe Int, actifs : Maybe Int }
decodeDevecos =
    Decode.succeed (\total nouveaux actifs -> { total = total, nouveaux = nouveaux, actifs = actifs })
        |> andMap (optionalNullableField "total" Decode.int)
        |> andMap (optionalNullableField "nouveaux" Decode.int)
        |> andMap (optionalNullableField "actifs" Decode.int)


decodeFiches : Decoder { total : Maybe Int, nouvelles : Maybe Int, modifiees : Maybe Int }
decodeFiches =
    Decode.succeed (\total nouvelles modifiees -> { total = total, nouvelles = nouvelles, modifiees = modifiees })
        |> andMap (optionalNullableField "total" Decode.int)
        |> andMap (optionalNullableField "nouvelles" Decode.int)
        |> andMap (optionalNullableField "modifiees" Decode.int)


decodeEchanges : Decoder { total : Maybe Int, nouveaux : Maybe Int }
decodeEchanges =
    Decode.succeed (\total nouveaux -> { total = total, nouveaux = nouveaux })
        |> andMap (optionalNullableField "total" Decode.int)
        |> andMap (optionalNullableField "nouveaux" Decode.int)


decodeDemandes : Decoder { total : Maybe Int, nouvelles : Maybe Int, fermees : Maybe Int }
decodeDemandes =
    Decode.succeed (\total nouvelles fermees -> { total = total, nouvelles = nouvelles, fermees = fermees })
        |> andMap (optionalNullableField "total" Decode.int)
        |> andMap (optionalNullableField "nouvelles" Decode.int)
        |> andMap (optionalNullableField "fermees" Decode.int)


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        GetStats ( territoire, periode ) ->
            { model | stats = RD.Loading }
                |> Effect.with (getStats territoire periode)

        ReceivedStats response ->
            { model | stats = response }
                |> Effect.withNone

        ChangedPeriode periode ->
            let
                newModel =
                    { model | periode = periode, stats = RD.Loading }
            in
            newModel
                |> Effect.with (updateUrl newModel)

        ChangedTerritoire typeTerritoire ->
            let
                newModel =
                    { model | typeTerritoire = typeTerritoire, stats = RD.Loading }
            in
            newModel
                |> Effect.with (updateUrl newModel)


updateUrl : Model -> Effect.Effect Shared.Msg Msg
updateUrl model =
    Effect.fromShared <|
        Shared.Navigate <|
            routeWithQuery model.typeTerritoire model.periode


routeWithQuery : TypeTerritoire -> Periode -> Route.Route
routeWithQuery territoire periode =
    Route.Stats <|
        Just <|
            serializeQuery territoire periode


view : Model -> View Msg
view model =
    { title =
        UI.Layout.pageTitle <|
            "Statistiques d'utilisation - Deveco"
    , body = UI.Layout.lazyBody body model
    , route = Route.Stats Nothing
    }


body : Model -> Html Msg
body model =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12, class "flex" ]
            [ h1 [ Typo.fr_h4, class "!mb-0" ]
                [ text "Statistiques d'utilisation de Deveco" ]
            ]
        , div [ DSFR.Grid.col4 ]
            [ Html.Lazy.lazy2 viewFilters model.periode model.typeTerritoire
            ]
        , div [ DSFR.Grid.col8 ]
            [ Html.Lazy.lazy2 viewStats model.typeTerritoire model.stats
            ]
        ]


viewFilters : Periode -> TypeTerritoire -> Html Msg
viewFilters periode typeTerritoire =
    div [ class "flex flex-col gap-8 fr-card--white h-full p-2 sm:p-4" ]
        [ DSFR.Radio.group
            { id = "select-periode"
            , options = [ AllTime, LastMonth, LastWeek ]
            , current = Just periode
            , toLabel = periodeToDisplay >> text
            , toId = periodeToString
            , msg = ChangedPeriode
            , legend = text "Période"
            }
            |> DSFR.Radio.withExtraAttrs [ class "!mb-0 !grow-0" ]
            |> DSFR.Radio.withLegendAttrs [ Typo.textBold ]
            |> DSFR.Radio.view
        , DSFR.Radio.group
            { id = "select-periode"
            , options = [ France, EPCI, Commune ]
            , current = Just typeTerritoire
            , toLabel = typeTerritoireToDisplay >> text
            , toId = typeTerritoireToString
            , msg = ChangedTerritoire
            , legend = text "Type de territoires"
            }
            |> DSFR.Radio.withExtraAttrs [ class "!mb-0 !grow-0" ]
            |> DSFR.Radio.withLegendAttrs [ Typo.textBold ]
            |> DSFR.Radio.view
        ]


viewStats : TypeTerritoire -> WebData Stats -> Html msg
viewStats typeTerritoire remoteStats =
    let
        statLine =
            displayStats remoteStats
    in
    div [ class "flex flex-col gap-4" ]
        [ div [ DSFR.Grid.gridRow, class "fr-card--white p-2 sm:p-4" ]
            [ div [ DSFR.Grid.col12, class "flex flex-col gap-4" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ]
                    [ text "Stats globales" ]
                , div [ DSFR.Grid.gridRow ]
                    [ div [ DSFR.Grid.col ]
                        [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Territoires" ]
                        , statLine "Total" (Just "nombre de collectivités dont au moins un agent s’est connecté depuis la mise à disposition de Dévéco") (.territoires >> .total >> Maybe.map String.fromInt)
                        , statLine "Actifs" (Just "nombre de collectivités dont au moins un agent a été actif sur la période de référence indiquée à gauche ") (.territoires >> .actifs >> Maybe.map String.fromInt)
                        , statLine "Nouveaux" (Just "nombre de collectivités dont au moins un agent s’est créé un compte et s’est connecté à Dévéco sur la période de référence indiquée à gauche") (.territoires >> .nouveaux >> Maybe.map String.fromInt)
                        ]
                    , div [ DSFR.Grid.col ]
                        [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Devecos" ]
                        , statLine "Total" (Just "nombre de développeurs économique s’étant connecté au moins une fois depuis la mise à disposition de Dévéco") (.devecos >> .total >> Maybe.map String.fromInt)
                        , statLine "Actifs" (Just "nombre de développeurs économique ayant consulté au moins une fiche sur la période de référence indiquée à gauche ") (.devecos >> .actifs >> Maybe.map String.fromInt)
                        , statLine "Nouveaux" (Just "nombre de développeurs économique ayant bénéficié d’une création de compte et s’étant connecté à Dévéco sur la période de référence indiquée à gauche") (.devecos >> .nouveaux >> Maybe.map String.fromInt)
                        ]
                    ]
                ]
            ]
        , div [ DSFR.Grid.gridRow, class "fr-card--white p-2 sm:p-4" ]
            [ div [ DSFR.Grid.col12, class "flex flex-col gap-4" ]
                [ h2 [ Typo.fr_h5, class "!mb-0" ]
                    [ text "Stats pour "
                    , typeTerritoire
                        |> typeTerritoireToDisplay
                        |> text
                    ]
                , div [ DSFR.Grid.gridRow ]
                    [ div [ DSFR.Grid.col6 ]
                        [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Devecos" ]
                        , statLine "Total" Nothing (.zone >> .devecos >> .total >> Maybe.map String.fromInt)
                        , statLine "Actifs" Nothing (.zone >> .devecos >> .actifs >> Maybe.map String.fromInt)
                        , statLine "Nouveaux" Nothing (.zone >> .devecos >> .nouveaux >> Maybe.map String.fromInt)
                        ]
                    , div [ DSFR.Grid.col6 ]
                        [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Fiches" ]
                        , statLine "Total" Nothing (.zone >> .fiches >> .total >> Maybe.map String.fromInt)
                        , statLine "Nouvelles" (Just "nombre de fiches créées sur l'outil dévéco sur la période de référence sélectionnée à gauche (soit par import, soit manuellement)") (.zone >> .fiches >> .nouvelles >> Maybe.map String.fromInt)
                        , statLine "Modifiées" (Just "nombre de fiches ayant fait l’objet d’une modification après la date de création et sur la période de référence indiquée à gauche") (.zone >> .fiches >> .modifiees >> Maybe.map String.fromInt)
                        ]
                    ]
                , div [ DSFR.Grid.gridRow ]
                    [ div [ DSFR.Grid.col6 ]
                        [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Échanges" ]
                        , statLine "Total" Nothing (.zone >> .echanges >> .total >> Maybe.map String.fromInt)
                        , statLine "Nouveaux" Nothing (.zone >> .echanges >> .nouveaux >> Maybe.map String.fromInt)
                        ]
                    , div [ DSFR.Grid.col6 ]
                        [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Demandes" ]
                        , statLine "Total" Nothing (.zone >> .demandes >> .total >> Maybe.map String.fromInt)
                        , statLine "Nouvelles" Nothing (.zone >> .demandes >> .nouvelles >> Maybe.map String.fromInt)
                        , statLine "Fermées" Nothing (.zone >> .demandes >> .fermees >> Maybe.map String.fromInt)
                        ]
                    ]
                ]
            ]
        ]


viewMaybeFigure : Maybe String -> Html msg
viewMaybeFigure =
    Maybe.withDefault "-"
        >> text
        >> List.singleton
        >> span [ Typo.textBold ]


displayStats : WebData Stats -> String -> Maybe String -> (Stats -> Maybe String) -> Html msg
displayStats stats lab hint toMaybeString =
    div []
        [ text <| lab
        , hint |> viewMaybe (\h -> sup [ title h ] [ span [ Typo.textXs ] [ iconSM informationLine ] ])
        , text "\u{00A0}: "
        , case stats of
            RD.Loading ->
                greyPlaceholder 1

            RD.Success s ->
                s
                    |> toMaybeString
                    |> viewMaybeFigure

            _ ->
                text "-"
        ]


greyPlaceholder : Int -> Html msg
greyPlaceholder length =
    "\u{00A0}"
        |> List.repeat length
        |> List.map (text >> List.singleton >> span [ class "w-[1rem] inline-block" ])
        |> span [ class "pulse inline-block h-[22px]" ]
