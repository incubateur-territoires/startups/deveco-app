module Pages.CGU exposing (page)

import Accessibility exposing (Html, div, h1, h2, h3, h4, li, p, text, ul)
import Api.Auth
import DSFR.Grid
import Effect
import Html.Attributes exposing (class)
import Route
import Shared
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


page : Shared.Shared -> Page () Shared.Msg (View ()) () ()
page { identity } =
    Spa.Page.element
        { init =
            \_ ->
                ( ()
                , case identity of
                    Nothing ->
                        Effect.fromSharedCmd <| Api.Auth.checkAuth True Nothing

                    _ ->
                        Effect.none
                )
                    |> Shared.pageChangeEffects
        , update = \_ _ -> ( (), Effect.none )
        , view = \_ -> view identity
        , subscriptions = \_ -> Sub.none
        }


view : Maybe Shared.User -> View ()
view identity =
    { title =
        UI.Layout.pageTitle <|
            "Conditions générales d'utilisation - Deveco"
    , body = UI.Layout.lazyBody body identity
    , route = Route.Stats Nothing
    }


body : Maybe Shared.User -> Html ()
body _ =
    div [ class "p-4" ]
        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col12, class "fr-card--white !p-8" ]
                [ h1 []
                    [ text "Conditions d’utilisation de l’application Deveco" ]
                , p []
                    [ text "Les présentes conditions générales d’utilisation (dites « CGU ») fixent le cadre juridique de l’application Deveco définissent les conditions d’accès et d’utilisation des services par l’Utilisateur." ]
                , h2 []
                    [ text "Article 1 - Champ d’application" ]
                , p []
                    [ text "La plateforme Deveco est accessible en ligne, sans téléchargement, suite à la création d’un compte validé. L’utilisation de l’application vaut acceptation des présentes CGU." ]
                , h2 []
                    [ text "Article 2 - Objet" ]
                , p []
                    [ text "« Deveco » vise à assurer le suivi de la relation entre une collectivité territoriale et des établissements économiques. L’application est une solution simple qui permet également un accès à des données informatives concernant la situation économique de ces établissements." ]
                , h2 []
                    [ text "Article 3 – Définitions" ]
                , p []
                    [ text "L’Utilisateur est tout agent d’une collectivité, ayant téléchargé l’application et qui souhaite développer et améliorer l’activité économique sur son territoire." ]
                , p []
                    [ text "Les services sont l’ensemble des services proposés par Deveco." ]
                , p []
                    [ text "Les données à caractère personnel sont les informations qui permettent directement ou indirectement d’identifier une personne physique, conformément à l’article 4 du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation de ces données (RGPD)." ]
                , p []
                    [ text "Les catégories particulières de données à caractère personnel, sont des catégories de données spécifiques définies par l’article 9 du RGPD. Il s’agit données qui révèlent l'origine raciale ou ethnique, les opinions politiques, les convictions religieuses ou philosophiques ou l'appartenance syndicale, ainsi que le traitement des données génétiques, des données biométriques aux fins d'identifier une personne physique de manière unique, des données concernant la santé ou des données concernant la vie sexuelle ou l'orientation sexuelle d'une personne physique sont interdits." ]
                , h2 []
                    [ text "Article 4- Fonctionnalités" ]
                , h3 []
                    [ text "4.1 Créer son compte" ]
                , p []
                    [ text "Tout Utilisateur doit se créer un compte pour accéder au site Deveco. La création de compte est réalisée via l’envoi d’un lien de connexion par l’adresse e-mail renseignée." ]
                , h4 []
                    [ text "A – Création du compte « Équipe »" ]
                , p []
                    [ text "Les comptes « Equipe » sont créés par l’équipe Deveco, sur sollicitation mail et/ou formulaire d’inscription rempli en ligne. Les comptes « Equipe » correspondent à un territoire de référence (commune ou intercommunalité) auxquels sont rattachés des comptes « Collaborateurs »." ]
                , h4 []
                    [ text "B – Création du compte « Collaborateur »" ]
                , p []
                    [ text "Les comptes « Collaborateurs » sont créées par le compte « Équipe » de la même manière. Les comptes Collaborateurs n’ont accès qu’aux fiches du territoire du périmètre assigné au compte « Équipe »." ]
                , h3 []
                    [ text "4.2 Gérer son portefeuille d’acteurs économique" ]
                , p []
                    [ text "Les Utilisateurs peuvent créer un portefeuille contenant les acteurs économiques recensés sur leur territoire, sous forme de fiche spécifique à chaque acteur. Sur chaque fiche, il peut rentrer le nom de l’entreprise" ]
                , p []
                    [ text "Ils pourront :" ]
                , ul []
                    [ li []
                        [ text "Créer une fiche sur un acteur économique, en définissant s’il s’agit d’une personne physique ou d’une personne morale ; Ils pourront également remplir des informations sur la personne morale (SIRET, SIREN, catégorie d’activité, forme juridique, date de création, activités réelles, localisation), ainsi que les demandes en cours ;" ]
                    , li []
                        [ text "Ajouter un contact lié à la fiche d’un acteur économique ;" ]
                    , li []
                        [ text "Avoir un ensemble organisé et classé d’acteurs économiques opérant sur son territoire ;" ]
                    , li []
                        [ text "Suivre les échanges réalisés avec l’acteur économique ;" ]
                    , li []
                        [ text "Rechercher les acteurs dans le portefeuille, classés notamment par type, activités ou demandes." ]
                    ]
                , h3 []
                    [ text "4.3 Créer et organiser des contacts avec les acteurs économiques" ]
                , p []
                    [ text "Les Utilisateurs peuvent également organiser des contacts avec les acteurs économiques dont ils ont créé une fiche, notamment en :" ]
                , ul []
                    [ li []
                        [ text "Ajoutant des éléments de suivi de la relation, notamment les rappels et les échanges et leur état, c’est-à-dire s’ils sont toujours en cours ou s’ils sont clôturés. En d’autres termes, il s’agit de pouvoir échanger rapidement et directement avec les acteurs, et le cas échéant suivre les actions attendues ou souhaitées par l’acteur ; Pour ce faire l’utilisateur doit cliquer sur la page « Mes actions Deveco » qui permet d’avoir la date d’échéance, le titre de l’action ou le lien vers la fiche associée. Le rappel peut être supprimé selon qu’il arrive à échéance ou qu’il est à venir." ]
                    , li []
                        [ text "Ajoutant le contact, comprenant la fonction, le nom et un moyen de contact d’une personne travaillant dans l’acteur économique." ]
                    ]
                , h3 []
                    [ text "4.4 Analyser les statistiques d’utilisation de Deveco" ]
                , p []
                    [ text "Chaque Utilisateur peut également cliquer sur une page « Statistiques ». Cette page est publique." ]
                , h2 []
                    [ text "Article 5 - Responsabilités" ]
                , h3 []
                    [ text "5.1 L’éditeur du Site" ]
                , p []
                    [ text "Les sources des informations diffusées sur l’application sont réputées fiables mais Deveco ne garantit pas qu’il soit exempt de défauts, d’erreurs ou d’omissions." ]
                , p []
                    [ text "Tout événement dû à un cas de force majeure ayant pour conséquence un dysfonctionnement du site et sous réserve de toute interruption ou modification en cas de maintenance, n'engage pas la responsabilité de l’éditeur." ]
                , p []
                    [ text "L’éditeur s’engage à mettre en œuvre toutes mesures appropriées, afin de protéger les données traitées." ]
                , p []
                    [ text "L’éditeur s’engage à la sécurisation du site, notamment en prenant les mesures nécessaires permettant de garantir la sécurité et la confidentialité des informations fournies." ]
                , p []
                    [ text "L’éditeur fournit les moyens nécessaires et raisonnables pour assurer un accès continu, sans contrepartie financière, à Deveco. Il se réserve la liberté de faire évoluer, de modifier ou de suspendre, sans préavis, la plateforme pour des raisons de maintenance ou pour tout autre motif jugé nécessaire." ]
                , h3 []
                    [ text "5.2 L’Utilisateur" ]
                , p []
                    [ text "Toute information transmise par l'Utilisateur est de sa seule responsabilité." ]
                , p []
                    [ text "L’Utilisateur s’engage à ne pas mettre en ligne de contenus ou informations contraires aux dispositions légales et réglementaires en vigueur. En particulier, il s’engage à ne pas publier de message ou de commentaire racistes, sexistes, injurieux, insultants ou contraires à l’ordre public. Il veille également à ne publier sur ces fiches que des éléments factuels ou descriptifs, sans jamais traiter de catégories particulières de données à caractère personnel." ]
                , p []
                    [ text "Il est rappelé que toute personne procédant à une fausse déclaration pour elle-même ou pour autrui s’expose, notamment, aux sanctions prévues à l’article 441-1 du code pénal, prévoyant des peines pouvant aller jusqu’à trois ans d’emprisonnement et 45 000 euros d’amende." ]
                , h2 []
                    [ text "Article 6 - Mise à jour des conditions d’utilisation" ]
                , p []
                    [ text "Les termes des présentes conditions d’utilisation peuvent être amendés à tout moment, sans préavis, en fonction des modifications apportées à la plateforme, de l’évolution de la législation ou pour tout autre motif jugé nécessaire." ]
                ]
            ]
        ]
