module Pages.Analysis exposing (Model, Msg, page)

import Accessibility exposing (Html, div, h1, h2, h3, span, text)
import Api
import Api.Auth
import DSFR.Grid
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Commune exposing (Commune)
import Data.Effectifs exposing (tranchesEffectifs)
import Data.FormeJuridique exposing (formeJuridiqueToLabel, stringToFormeJuridique)
import Effect
import Html
import Html.Attributes exposing (class)
import Html.Lazy
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode
import MultiSelect
import QS
import RemoteData as RD exposing (WebData)
import Route
import Shared
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


type alias Model =
    { stats : WebData Stats
    , filteredStats : WebData Stats
    , selectCommunes : MultiSelect.SmartSelect Msg Commune
    , communes : WebData (List Commune)
    , filters : Filters
    }


type alias Filters =
    { communes : List Commune
    }


type alias Stats =
    { etablissements : List ( String, Int )
    , nafs : List ( String, Int )
    , effectifs : List ( String, Int )
    , categories_juridiques : List ( String, Int )
    , qpvs : List ( String, Int )
    }


type Msg
    = ReceivedStats Bool (WebData Stats)
    | ReceivedCommunes (WebData (List Commune))
    | SelectedCommunes ( List Commune, MultiSelect.Msg Commune )
    | UpdatedSelectCommunes (MultiSelect.Msg Commune)
    | UnselectCommune Commune
    | SharedMsg Shared.Msg


page : Shared.Shared -> Shared.User -> Page () Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.element
        { view = view
        , init = init
        , update = update
        , subscriptions = .selectCommunes >> MultiSelect.subscriptions
        }


init : () -> ( Model, Effect.Effect Shared.Msg Msg )
init _ =
    ( { stats = RD.Loading
      , filteredStats = RD.NotAsked
      , selectCommunes =
            MultiSelect.init "select-commune"
                { selectionMsg = SelectedCommunes
                , internalMsg = UpdatedSelectCommunes
                }
      , communes = RD.Loading
      , filters = { communes = [] }
      }
    , Effect.batch [ Effect.fromCmd getCommunes, Effect.fromCmd <| getStats [] ]
    )
        |> Shared.pageChangeEffects


getCommunes : Cmd Msg
getCommunes =
    Api.Auth.get
        { url = Api.getCommunes }
        SharedMsg
        ReceivedCommunes
        (Decode.list <| Data.Commune.decodeCommune)


getStats : List Commune -> Cmd Msg
getStats communes =
    let
        setEffectifs =
            communes
                |> List.map .id
                |> QS.setListStr "communes"

        query =
            QS.empty
                |> setEffectifs
                |> QS.serialize (QS.config |> QS.addQuestionMark False)
    in
    Http.get
        { url = Api.getAnalysisStats query
        , expect = Http.expectJson (RD.fromResult >> ReceivedStats (List.length communes /= 0)) <| decodeStats
        }


decodeStats : Decoder Stats
decodeStats =
    Decode.succeed Stats
        |> andMap (Decode.field "etablissements" <| Decode.list <| Decode.map2 Tuple.pair (Decode.field "label" Decode.string) (Decode.field "count" Decode.int))
        |> andMap (Decode.field "nafs" <| Decode.list <| Decode.map2 Tuple.pair (Decode.field "label" Decode.string) (Decode.field "count" Decode.int))
        |> andMap (Decode.field "effectifs" <| Decode.list <| Decode.map2 Tuple.pair (Decode.field "label" Decode.string) (Decode.field "count" Decode.int))
        |> andMap (Decode.field "categories_juridiques" <| Decode.list <| Decode.map2 Tuple.pair (Decode.field "label" Decode.string) (Decode.field "count" Decode.int))
        |> andMap (Decode.field "qpvs" <| Decode.list <| Decode.map2 Tuple.pair (Decode.field "label" Decode.string) (Decode.field "count" Decode.int))


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        ReceivedStats filtered response ->
            if filtered then
                { model | filteredStats = response }
                    |> Effect.withNone

            else
                { model | stats = response }
                    |> Effect.withNone

        ReceivedCommunes resp ->
            let
                communesSources =
                    resp
                        |> RD.withDefault []

                communes =
                    model.filters.communes

                newCommunes =
                    communes
                        |> List.map
                            (\comm ->
                                communesSources
                                    |> List.filter (.id >> (==) comm.id)
                                    |> List.head
                                    |> Maybe.withDefault comm
                            )
            in
            { model
                | communes = resp
                , filters =
                    model.filters |> (\f -> { f | communes = newCommunes })
            }
                |> Effect.withNone

        SelectedCommunes ( communes, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectCommunes

                communesUniques =
                    case communes of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                communes

                newModel =
                    { model
                        | selectCommunes = updatedSelect
                        , filters =
                            model.filters
                                |> (\filters -> { filters | communes = communesUniques })
                    }
            in
            ( newModel
            , Effect.batch
                [ Effect.fromCmd selectCmd
                , Effect.fromCmd <| getStats newModel.filters.communes
                ]
            )

        UpdatedSelectCommunes sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelect.update sMsg model.selectCommunes
            in
            ( { model | selectCommunes = updatedSelect }, Effect.fromCmd selectCmd )

        UnselectCommune commune ->
            let
                newModel =
                    { model
                        | filters =
                            model.filters
                                |> (\filters ->
                                        { filters
                                            | communes =
                                                filters.communes
                                                    |> List.filter (\c -> c.id /= commune.id)
                                        }
                                   )
                    }
            in
            ( newModel
            , Effect.fromCmd <| getStats newModel.filters.communes
            )


view : Model -> View Msg
view model =
    { title =
        UI.Layout.pageTitle <|
            "Statistiques d'utilisation - Deveco"
    , body = UI.Layout.lazyBody body model
    , route = Route.Analysis
    }


body : Model -> Html Msg
body { communes, selectCommunes, filters, stats, filteredStats } =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12, class "flex" ]
            [ h1 [ Typo.fr_h4, class "!mb-0" ]
                [ text "Analyse territoriale" ]
            ]
        , div [ DSFR.Grid.col12 ]
            [ viewFilters communes filters selectCommunes
            ]
        , div [ DSFR.Grid.col6 ]
            [ Html.Lazy.lazy2 viewStats "Tout le territoire" stats
            ]
        , div [ DSFR.Grid.col6 ]
            [ Html.Lazy.lazy2 viewStats "Sélection" <| filteredStats
            ]
        ]


viewFilters : WebData (List Commune) -> Filters -> MultiSelect.SmartSelect Msg Commune -> Html Msg
viewFilters communes filters selectCommunes =
    div [ class "flex flex-col gap-8 fr-card--white h-full p-2 sm:p-4" ]
        [ communeFilter (communes |> RD.withDefault []) selectCommunes filters.communes
        ]


communeFilter : List Commune -> MultiSelect.SmartSelect Msg Commune -> List Commune -> Html Msg
communeFilter optionsCommunes selectCommunes communes =
    let
        communeToLabel c =
            case ( c.label, c.departement ) of
                ( "", "" ) ->
                    c.id

                _ ->
                    c.label ++ " (" ++ c.departement ++ ")"
    in
    div [ class "flex flex-row gap-2 items-end" ]
        [ div [ class "w-[300px]" ]
            [ selectCommunes
                |> MultiSelect.viewCustom
                    { isDisabled = optionsCommunes |> List.length |> (==) 0
                    , selected = communes
                    , options = optionsCommunes
                    , optionLabelFn = communeToLabel
                    , optionDescriptionFn = \_ -> ""
                    , optionsContainerMaxHeight = 300
                    , selectTitle = text "Communes"
                    , viewSelectedOptionFn = .label >> text
                    , noResultsForMsg = \searchText -> "Aucune autre commune n'a été trouvée pour " ++ searchText
                    , noOptionsMsg = "Aucune commune n'a été trouvée"
                    , searchFn =
                        \searchText allOptions ->
                            let
                                lowerSearch =
                                    String.toLower searchText
                            in
                            List.filter (String.contains lowerSearch << String.toLower << .label) <|
                                allOptions
                    , searchPrompt = "Filtrer par commune"
                    }
            ]
        , communes
            |> List.map (\commune -> DSFR.Tag.deletable UnselectCommune { data = commune, toString = communeToLabel })
            |> DSFR.Tag.medium
            |> List.singleton
            |> div [ class "flex-shrink" ]
        ]


viewStats : String -> WebData Stats -> Html msg
viewStats header stats =
    div [ class "flex flex-col gap-4" ]
        [ div [ DSFR.Grid.gridRow, class "fr-card--white p-2 sm:p-4" ]
            [ div [ DSFR.Grid.col12 ]
                [ h2 [ Typo.fr_h5 ] [ text header ]
                , case stats of
                    RD.Success data ->
                        div [ class "flex flex-col gap-8" ]
                            [ let
                                title =
                                    "Répartition des établissements par commune"

                                subtitle =
                                    "(limité à 10 communes)"
                              in
                              div [ class "flex flex-col" ]
                                [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text title ]
                                , span [ class "!mb-0" ] [ text subtitle ]
                                , Html.node "chart-bar-horizontal"
                                    [ Encode.object
                                        [ ( "values"
                                          , data.etablissements
                                                |> Encode.list
                                                    (\( label, count ) ->
                                                        Encode.object
                                                            [ ( "label", Encode.string label )
                                                            , ( "count", Encode.float <| toFloat count )
                                                            ]
                                                    )
                                          )
                                        , ( "config"
                                          , Encode.object
                                                [ ( "title", Encode.string title )
                                                , ( "maxItems", Encode.int 10 )
                                                ]
                                          )
                                        ]
                                        |> Encode.encode 0
                                        |> Html.Attributes.attribute "data"
                                    ]
                                    []
                                ]
                            , let
                                title =
                                    "Répartition des établissements par code NAF"

                                subtitle =
                                    "(limité aux 10 premiers codes NAF)"
                              in
                              div [ class "flex flex-col" ]
                                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text title ]
                                , span [ class "!mb-0" ] [ text subtitle ]
                                , Html.node "chart-bar-horizontal"
                                    [ Encode.object
                                        [ ( "values"
                                          , data.nafs
                                                |> Encode.list
                                                    (\( label, count ) ->
                                                        Encode.object
                                                            [ ( "label", Encode.string label )
                                                            , ( "count", Encode.float <| toFloat count )
                                                            ]
                                                    )
                                          )
                                        , ( "config"
                                          , Encode.object
                                                [ ( "title", Encode.string title )
                                                , ( "maxItems", Encode.int 10 )
                                                ]
                                          )
                                        ]
                                        |> Encode.encode 0
                                        |> Html.Attributes.attribute "data"
                                    ]
                                    []
                                ]
                            , let
                                title =
                                    "Répartition des établissements par tranche d'effectifs"
                              in
                              div [ class "flex flex-col" ]
                                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text title ]
                                , Html.node "chart-bar-horizontal"
                                    [ Encode.object
                                        [ ( "values"
                                          , data.effectifs
                                                |> List.map (Tuple.mapFirst effectifsCodeToEffectifsLabel)
                                                |> Encode.list
                                                    (\( label, count ) ->
                                                        Encode.object
                                                            [ ( "label", Encode.string label )
                                                            , ( "count", Encode.float <| toFloat count )
                                                            ]
                                                    )
                                          )
                                        , ( "config"
                                          , Encode.object
                                                [ ( "title", Encode.string title )
                                                , ( "maxItems", Encode.int 10 )
                                                ]
                                          )
                                        ]
                                        |> Encode.encode 0
                                        |> Html.Attributes.attribute "data"
                                    ]
                                    []
                                ]
                            , let
                                title =
                                    "Répartition des établissements par catégorie juridique"

                                subtitle =
                                    "(limité aux 5 premières catégories)"
                              in
                              div [ class "flex flex-col" ]
                                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text title ]
                                , span [ class "!mb-0" ] [ text subtitle ]
                                , Html.node "chart-pie"
                                    [ Encode.object
                                        [ ( "values"
                                          , data.categories_juridiques
                                                |> groupCategoriesJuridiques
                                                |> Encode.list
                                                    (\( label, count ) ->
                                                        Encode.object
                                                            [ ( "label", Encode.string label )
                                                            , ( "count", Encode.float <| toFloat count )
                                                            ]
                                                    )
                                          )
                                        , ( "config"
                                          , Encode.object
                                                [ ( "title", Encode.string title )
                                                , ( "maxItems", Encode.int 5 )
                                                ]
                                          )
                                        ]
                                        |> Encode.encode 0
                                        |> Html.Attributes.attribute "data"
                                    ]
                                    []
                                ]
                            , let
                                title =
                                    "Répartition des établissements par QPV"

                                subtitle =
                                    "(uniquement les établissements en QPV)"
                              in
                              div [ class "flex flex-col" ]
                                [ h2 [ Typo.fr_h5, class "!mb-0" ] [ text title ]
                                , span [ class "!mb-0" ] [ text subtitle ]
                                , case data.qpvs of
                                    [] ->
                                        div [] [ text "Aucun établissement en QPV" ]

                                    qpvs ->
                                        Html.node "chart-bar-horizontal"
                                            [ Encode.object
                                                [ ( "values"
                                                  , qpvs
                                                        |> Encode.list
                                                            (\( label, count ) ->
                                                                Encode.object
                                                                    [ ( "label", Encode.string label )
                                                                    , ( "count", Encode.float <| toFloat count )
                                                                    ]
                                                            )
                                                  )
                                                , ( "config"
                                                  , Encode.object
                                                        [ ( "title", Encode.string title )
                                                        , ( "maxItems", Encode.int 10 )
                                                        ]
                                                  )
                                                ]
                                                |> Encode.encode 0
                                                |> Html.Attributes.attribute "data"
                                            ]
                                            []
                                ]
                            ]

                    RD.NotAsked ->
                        text "Sélectionnez une ou plusieurs communes pour comparer au territoire."

                    _ ->
                        text "Chargement"
                ]
            ]
        ]


effectifsCodeToEffectifsLabel : String -> String
effectifsCodeToEffectifsLabel code =
    tranchesEffectifs
        |> List.filter (\{ id } -> id == code)
        |> List.head
        |> Maybe.map .label
        |> Maybe.withDefault "Donnée inconnue"


groupCategoriesJuridiques : List ( String, Int ) -> List ( String, Int )
groupCategoriesJuridiques list =
    list
        |> List.filterMap
            (\( code, quantity ) ->
                ("FJ" ++ code)
                    |> stringToFormeJuridique
                    |> Maybe.andThen (\fj -> Just ( formeJuridiqueToLabel fj, quantity ))
            )
