module Pages.Rappels exposing (Model, Msg, page)

import Accessibility exposing (Html, div, h1, h2, h3, hr, span, text)
import Api
import Api.Auth
import Api.EntityId exposing (EntityId)
import DSFR.Button
import DSFR.Grid
import DSFR.Icons
import DSFR.Icons.Design
import DSFR.Icons.System
import DSFR.Modal
import DSFR.Typography as Typo
import Data.Fiche exposing (FicheId)
import Data.Rappel exposing (Rappel)
import Data.Role
import Date
import Effect
import Html.Attributes exposing (class)
import Html.Attributes.Extra exposing (empty)
import Html.Extra exposing (nothing)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode
import Lib.Date
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (Shared)
import Spa.Page exposing (Page)
import Time exposing (Posix, Zone)
import UI.Layout
import UI.Rappel exposing (RappelInput)
import User
import View exposing (View)


type alias Model =
    { rappels : WebData (List ( Rappel, Fiche ))
    , rappelAction : RappelAction
    , saveRappelRequest : WebData (List ( Rappel, Fiche ))
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | ReceivedGetRappelsResponse (WebData (List ( Rappel, Fiche )))
    | SetRappelAction RappelAction
    | UpdatedRappelForm UI.Rappel.Field String
    | CancelRappelAction
    | ConfirmRappelAction
    | ReceivedSaveRappel (WebData (List ( Rappel, Fiche )))


type Fiche
    = Etablissement String String
    | Particulier String (EntityId FicheId)


type RappelAction
    = None
    | Edit RappelInput
    | Cloture RappelInput


page : Shared -> Shared.User -> Page () Shared.Msg (View Msg) Model Msg
page shared user =
    Spa.Page.element
        { view = view shared
        , init = init user
        , update = update
        , subscriptions = \_ -> Sub.none
        }


init : Shared.User -> () -> ( Model, Effect.Effect Shared.Msg Msg )
init user () =
    Shared.pageChangeEffects <|
        if Data.Role.isDeveco <| User.role user then
            ( { rappels = RD.Loading
              , rappelAction = None
              , saveRappelRequest = RD.NotAsked
              }
            , Effect.fromCmd getRappels
            )

        else
            ( { rappels = RD.Loading
              , rappelAction = None
              , saveRappelRequest = RD.NotAsked
              }
            , Effect.fromShared <| Shared.ReplaceUrl <| Route.Dashboard
            )


getRappels : Cmd Msg
getRappels =
    Api.Auth.get
        { url = Api.getRappels }
        SharedMsg
        ReceivedGetRappelsResponse
        (Decode.list <| decodeAugmentedRappel)


decodeAugmentedRappel : Decoder ( Rappel, Fiche )
decodeAugmentedRappel =
    Decode.succeed (\rappel fiche -> ( rappel, fiche ))
        |> andMap (Decode.field "rappel" Data.Rappel.decodeRappel)
        |> andMap decodeFiche


decodeFiche : Decoder Fiche
decodeFiche =
    Decode.at [ "fiche", "entite", "entiteType" ] Decode.string
        |> Decode.andThen
            (\entiteType ->
                case entiteType of
                    "PM" ->
                        Decode.map2 Etablissement
                            (Decode.at [ "fiche", "entite", "entreprise" ] <| Decode.field "nom" Decode.string)
                            (Decode.at [ "fiche", "entite", "entreprise" ] <| Decode.field "siret" Decode.string)

                    "PP" ->
                        Decode.map2 Particulier
                            (Decode.at [ "fiche", "entite", "particulier" ] <|
                                Decode.map2 (\prenom nom -> prenom ++ " " ++ nom)
                                    (Decode.field "prenom" Decode.string)
                                    (Decode.field "nom" Decode.string)
                            )
                            (Decode.at [ "fiche", "id" ] <| Api.EntityId.decodeEntityId)

                    _ ->
                        Decode.fail "Bad entiteType"
            )


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        ReceivedGetRappelsResponse rappels ->
            { model | rappels = rappels }
                |> Effect.withNone

        SetRappelAction action ->
            { model | rappelAction = action }
                |> Effect.withNone

        UpdatedRappelForm field value ->
            case model.rappelAction of
                Edit rappel ->
                    { model | rappelAction = Edit <| UI.Rappel.update field value <| rappel }
                        |> Effect.withNone

                _ ->
                    model |> Effect.withNone

        CancelRappelAction ->
            { model | saveRappelRequest = RD.NotAsked, rappelAction = None }
                |> Effect.withNone

        ConfirmRappelAction ->
            case model.saveRappelRequest of
                RD.Loading ->
                    model |> Effect.withNone

                _ ->
                    case model.rappelAction of
                        None ->
                            model |> Effect.withNone

                        Edit rappel ->
                            { model | saveRappelRequest = RD.Loading }
                                |> Effect.withCmd (updateRappel rappel)

                        Cloture rappel ->
                            { model | saveRappelRequest = RD.Loading }
                                |> Effect.withCmd (clotureRappel rappel)

        ReceivedSaveRappel response ->
            case model.saveRappelRequest of
                RD.Loading ->
                    case response of
                        RD.Success _ ->
                            { model | rappels = response, saveRappelRequest = response, rappelAction = None } |> Effect.withNone

                        _ ->
                            { model | saveRappelRequest = response } |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone


view : Shared -> Model -> View Msg
view shared model =
    { title = UI.Layout.pageTitle "Actions Déveco à mener"
    , body = [ body shared model ]
    , route = Route.Rappels
    }


body : Shared -> Model -> Html Msg
body { timezone, now } model =
    case model.rappels of
        RD.Loading ->
            div [ class "p-4 sm:p-8 fr-card--white" ]
                [ DSFR.Icons.System.refreshFill |> DSFR.Icons.iconLG
                , text "Chargement en cours..."
                ]

        RD.Success rappels ->
            div [ class "p-4 sm:p-8 fr-card--white" ]
                [ viewModal model
                , div [ class "flex flex-col gap-8" ]
                    [ div [ class "flex flex-row justify-between" ]
                        [ h1 [ Typo.fr_h4, class "!mb-0" ] [ text "Actions Déveco à mener" ]
                        ]
                    , viewRappels timezone now "Rappels arrivés à échéance" True <|
                        List.sortWith
                            (\rap1 rap2 ->
                                Lib.Date.sortOlderDateFirst
                                    (\( rap, _ ) ->
                                        Data.Rappel.date rap
                                    )
                                    rap1
                                    rap2
                            )
                        <|
                            List.filter (\( r, _ ) -> rappelIsLate timezone now r) <|
                                List.filter (\( rap, _ ) -> Nothing == Data.Rappel.dateCloture rap) <|
                                    rappels
                    , viewRappels timezone now "Rappels à venir" False <|
                        List.sortWith
                            (\rap1 rap2 ->
                                Lib.Date.sortOlderDateFirst
                                    (\( rap, _ ) ->
                                        Data.Rappel.date rap
                                    )
                                    rap1
                                    rap2
                            )
                        <|
                            List.filter (\( r, _ ) -> rappelIsLate timezone now r |> not) <|
                                List.filter (\( rap, _ ) -> Nothing == Data.Rappel.dateCloture rap) <|
                                    rappels
                    ]
                ]

        _ ->
            div [ class "p-4 sm:p-8 fr-card--white" ]
                [ text "Une erreur s'est produite, veuillez recharger la page."
                ]


viewModal : Model -> Html Msg
viewModal model =
    case model.rappelAction of
        None ->
            text ""

        Edit rappel ->
            viewRappelForm model.saveRappelRequest rappel

        Cloture rappel ->
            viewDeleteRappelConfirmation model.saveRappelRequest rappel


viewRappelForm : WebData (List ( Rappel, Fiche )) -> RappelInput -> Html Msg
viewRappelForm saveRappelRequest rappelInput =
    DSFR.Modal.view
        { id = "suivi"
        , label = "suivi"
        , openMsg = NoOp
        , closeMsg = Just CancelRappelAction
        , title = text "Modifier un rappel"
        , opened = True
        }
        (viewRappelBody saveRappelRequest rappelInput)
        Nothing
        |> Tuple.first


viewRappelBody : WebData (List ( Rappel, Fiche )) -> RappelInput -> Html Msg
viewRappelBody saveRappelRequest rappelInput =
    div [ DSFR.Grid.gridRow ]
        [ div [ DSFR.Grid.col, class "flex flex-col gap-4" ] <|
            [ div [ class "flex font-bold" ]
                [ h2 [ Typo.fr_h6, class "w-full border-b-4 border-france-blue !mb-0" ]
                    [ text "Modification d'un rappel" ]
                ]
            , UI.Rappel.form UpdatedRappelForm rappelInput
            , viewRappelFooter saveRappelRequest rappelInput
            ]
        ]


viewRappelFooter : WebData (List ( Rappel, Fiche )) -> RappelInput -> Html Msg
viewRappelFooter saveRappelRequest rappelInput =
    let
        disabled =
            saveRappelRequest
                == RD.Loading
                || (not <| UI.Rappel.isValid rappelInput)
    in
    DSFR.Button.group
        [ DSFR.Button.new { onClick = Just <| ConfirmRappelAction, label = "Enregistrer" }
            |> DSFR.Button.submit
            |> DSFR.Button.withDisabled disabled
        , DSFR.Button.new { onClick = Just <| CancelRappelAction, label = "Annuler" }
            |> DSFR.Button.secondary
        ]
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


viewDeleteRappelConfirmation : WebData (List ( Rappel, Fiche )) -> RappelInput -> Html Msg
viewDeleteRappelConfirmation saveRappelRequest rappel =
    let
        ( title, action ) =
            if rappel.dateCloture == Nothing then
                ( "Réouvrir", "réouvrir" )

            else
                ( "Clôturer", "clôturer" )
    in
    DSFR.Modal.view
        { id = "delete"
        , label = "delete"
        , openMsg = NoOp
        , closeMsg = Just CancelRappelAction
        , title = text <| title ++ " un rappel"
        , opened = True
        }
        (div [ DSFR.Grid.gridRow ]
            [ div [ DSFR.Grid.col, class "flex flex-col gap-4" ]
                [ text <| "Êtes-vous sûr(e) de vouloir " ++ action ++ " ce rappel\u{00A0}?" ]
            ]
        )
        (Just <| viewDeleteRappelFooter saveRappelRequest)
        |> Tuple.first


viewDeleteRappelFooter : WebData (List ( Rappel, Fiche )) -> Html Msg
viewDeleteRappelFooter saveRappelRequest =
    let
        disabled =
            saveRappelRequest == RD.Loading
    in
    DSFR.Button.group
        [ DSFR.Button.new { onClick = Just <| ConfirmRappelAction, label = "Confirmer" }
            |> DSFR.Button.submit
            |> DSFR.Button.withDisabled disabled
        , DSFR.Button.new { onClick = Just <| CancelRappelAction, label = "Annuler" }
            |> DSFR.Button.secondary
        ]
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


viewRappels : Zone -> Posix -> String -> Bool -> List ( Rappel, Fiche ) -> Html Msg
viewRappels timezone now title isLate rappels =
    div [ class "flex flex-col gap-4" ]
        (div [ class "flex flex-col" ]
            [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text title ]
            , hr [ class "!pb-4" ] []
            ]
            :: div [ class "border-transparent" ]
                [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ div [ DSFR.Grid.col1 ] [ nothing ]
                    , div [ DSFR.Grid.col2, class "font-bold" ] [ text "Date d'échéance" ]
                    , div [ DSFR.Grid.col4, class "font-bold" ] [ text "Titre" ]
                    , div [ DSFR.Grid.col3, class "font-bold" ] [ text "Lien vers la fiche associée" ]
                    , div [ DSFR.Grid.col2, class "font-bold text-right !pr-[1.5rem]" ] [ text "Actions" ]
                    ]
                ]
            :: (if List.length rappels == 0 then
                    span [ class "italic text-center" ] [ text "Aucun" ]

                else
                    nothing
               )
            :: (List.map (viewRappel timezone now isLate) <|
                    rappels
               )
        )


rappelIsLate : Zone -> Posix -> Rappel -> Bool
rappelIsLate timezone now rappel =
    Data.Rappel.dateCloture rappel
        == Nothing
        && (rappel |> Data.Rappel.date |> Lib.Date.firstDateIsAfterSecondDate (Date.fromPosix timezone now))


viewRappel : Zone -> Posix -> Bool -> ( Rappel, Fiche ) -> Html Msg
viewRappel timezone now isLate ( rappel, fiche ) =
    let
        ( icon, color ) =
            if isLate && (Nothing == Data.Rappel.dateCloture rappel) then
                ( div [ class "fr-text-default--error text-center pt-[0.5em]" ]
                    [ DSFR.Icons.custom "ri-checkbox-blank-circle-fill" |> DSFR.Icons.icon ]
                , class "fr-text-default--error"
                )

            else
                ( nothing, empty )

        buttons =
            [ [ DSFR.Button.new { onClick = Just <| SetRappelAction <| Edit <| UI.Rappel.rappelToRappelInput rappel, label = "" }
                    |> DSFR.Button.onlyIcon DSFR.Icons.Design.editFill
                    |> DSFR.Button.tertiaryNoOutline
                    |> DSFR.Button.withAttrs [ class "!p-0 !m-0 !ml-1", Html.Attributes.title "Modifier le rappel" ]
              , DSFR.Button.new
                    { onClick =
                        Just <|
                            SetRappelAction <|
                                Cloture <|
                                    (\r ->
                                        { r
                                            | dateCloture =
                                                Date.fromPosix timezone now
                                                    |> Date.toIsoString
                                                    |> Just
                                        }
                                    )
                                    <|
                                        UI.Rappel.rappelToRappelInput rappel
                    , label = ""
                    }
                    |> DSFR.Button.onlyIcon DSFR.Icons.System.closeLine
                    |> DSFR.Button.tertiaryNoOutline
                    |> DSFR.Button.withAttrs [ class "!p-0 !m-0 !ml-1", Html.Attributes.title "Clôturer le rappel" ]
              ]
                |> DSFR.Button.group
                |> DSFR.Button.inline
                |> DSFR.Button.alignedRight
                |> DSFR.Button.viewGroup
            ]

        ( typeName, name, link ) =
            case fiche of
                Etablissement nom siret ->
                    ( "Établissement", nom, Route.toUrl <| Route.Etablissement siret )

                Particulier nom ficheId ->
                    ( "Créateur d'entreprise", nom, Route.toUrl <| Route.Createur ficheId )
    in
    div [ class "!py-4 border-2 dark-grey-border" ]
        [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "items-center" ]
            [ div [ DSFR.Grid.col1 ]
                [ icon ]
            , div [ color, DSFR.Grid.col2 ] [ text <| Lib.Date.formatDateShort <| Data.Rappel.date rappel ]
            , div [ DSFR.Grid.col4 ]
                [ text <| Data.Rappel.titre rappel
                ]
            , div [ DSFR.Grid.col3 ] <|
                [ text <| typeName ++ "\u{00A0}: ", Typo.link link [] [ text name ] ]
            , div [ DSFR.Grid.col2 ] buttons
            ]
        ]


updateRappel : RappelInput -> Cmd Msg
updateRappel rappelInput =
    case UI.Rappel.rappelInputToRappel rappelInput of
        Err _ ->
            Cmd.none

        Ok rappel ->
            let
                jsonBody =
                    Data.Rappel.encodeRappel "actions" rappel
                        |> Http.jsonBody
            in
            Api.Auth.post
                { url = Api.updateRappel <| Data.Rappel.id rappel
                , body = jsonBody
                }
                SharedMsg
                ReceivedSaveRappel
                (Decode.list decodeAugmentedRappel)


clotureRappel : RappelInput -> Cmd Msg
clotureRappel { id, dateCloture } =
    let
        rappel =
            [ ( "dateCloture", Maybe.withDefault Encode.null <| Maybe.map Encode.string <| dateCloture )
            ]
                |> Encode.object

        jsonBody =
            [ ( "rappel", rappel )
            , ( "from", Encode.string "actions" )
            ]
                |> Encode.object
                |> Http.jsonBody
    in
    Api.Auth.post
        { url = Api.updateRappel id
        , body = jsonBody
        }
        SharedMsg
        ReceivedSaveRappel
        (Decode.list decodeAugmentedRappel)
