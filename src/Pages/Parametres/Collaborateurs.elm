module Pages.Parametres.Collaborateurs exposing (Model, Msg, page)

import Accessibility exposing (Html, div, h3, p, span, text)
import Api
import Api.Auth
import DSFR.Card
import DSFR.Grid
import DSFR.Typography as Typo
import Data.Role
import Data.Territoire
import Effect
import Html.Attributes exposing (class)
import Json.Decode as Decode
import Lib.Variables exposing (contactEmail)
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import Spa.Page exposing (Page)
import UI.Layout
import Url.Builder as Builder
import User
import View exposing (View)


type alias Model =
    { collaborateurs : WebData (List User)
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | ReceivedCollaborateurs (WebData (List User))


page : Shared.Shared -> User -> Page () Shared.Msg (View Msg) Model Msg
page _ user =
    Spa.Page.onNewFlags (\_ -> NoOp) <|
        Spa.Page.element
            { view = view user
            , init = init
            , update = update user
            , subscriptions = \_ -> Sub.none
            }


init : () -> ( Model, Effect.Effect Shared.Msg Msg )
init () =
    ( { collaborateurs = RD.Loading
      }
    , Effect.fromCmd getCollaborators
    )
        |> Shared.pageChangeEffects


getCollaborators : Cmd Msg
getCollaborators =
    Api.Auth.get
        { url = Api.getCollaborateurs }
        SharedMsg
        ReceivedCollaborateurs
        (Decode.list <| User.decodeUser)


update : User -> Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update _ msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        ReceivedCollaborateurs response ->
            { model | collaborateurs = response }
                |> Effect.withNone


view : User.User -> Model -> View Msg
view user model =
    { title = UI.Layout.pageTitle "Gestion des collaborateurs"
    , body = [ body user model ]
    , route = Route.ParamsCollaborateurs
    }


body : User.User -> Model -> Html Msg
body user model =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col3 ] [ UI.Layout.parametresSideMenu Route.ParamsCollaborateurs ]
        , div [ DSFR.Grid.col9 ]
            [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ viewCollaborators
                    (user
                        |> User.role
                        |> Data.Role.territoire
                        |> Maybe.map Data.Territoire.nom
                        |> Maybe.withDefault "?"
                    )
                    model.collaborateurs
                ]
            ]
        ]


viewCollaborators : String -> WebData (List User) -> Html msg
viewCollaborators territoire collaborateurs =
    div [ DSFR.Grid.col12 ]
        [ DSFR.Card.card "Mes collaborateurs" DSFR.Card.vertical
            |> DSFR.Card.withDescription
                (Just <|
                    div [] <|
                        [ p []
                            [ span [ Typo.textBold ] [ text "Seuls les collaborateurs listés ci-dessous" ]
                            , text " peuvent accéder au contenu de l'outil Deveco de "
                            , text territoire
                            , text "."
                            ]
                        , case collaborateurs of
                            RD.NotAsked ->
                                text "Une erreur s'est produite, veuillez recharger la page."

                            RD.Loading ->
                                text "Chargement en cours..."

                            RD.Failure _ ->
                                text "Une erreur s'est produite, veuillez recharger la page."

                            RD.Success collabs ->
                                let
                                    ( enabledCollabs, disabledCollabs ) =
                                        List.partition User.enabled <|
                                            collabs
                                in
                                div [ class "flex flex-row gap-4" ]
                                    [ div [ class "w-full flex flex-col gap-2" ]
                                        [ h3 [ Typo.fr_h6, class "!mb-0" ]
                                            [ text "Collaborateurs actuels"
                                            ]
                                        , let
                                            emails =
                                                [ contactEmail ]

                                            subject =
                                                Builder.string "subject" <|
                                                    "Demande d'ajout d'un collaborateur"

                                            bod =
                                                Builder.string "body" <|
                                                    "Territoire\u{00A0}: "
                                                        ++ territoire
                                                        ++ "\nEmail du collaborateur\u{00A0}: \nNom du collaborateur\u{00A0}: \nPrénom du collaborateur\u{00A0}: \nFonction du collaborateur\u{00A0}: \n"

                                            mailto =
                                                "mailto:"
                                                    ++ String.join "," emails
                                                    ++ Builder.toQuery [ subject, bod ]
                                          in
                                          span []
                                            [ Typo.externalLink mailto [] [ text "Demander l'ajout d'un collaborateur" ] ]
                                        , if List.length enabledCollabs == 0 then
                                            text "Aucun collaborateur pour l'instant"

                                          else
                                            div [ class "flex flex-col gap-2" ] <|
                                                List.map viewCollaborator <|
                                                    enabledCollabs
                                        ]
                                    , div [ class "w-full flex flex-col gap-2" ]
                                        [ h3 [ Typo.fr_h6, class "!mb-0" ]
                                            [ text "Anciens collaborateurs" ]
                                        , let
                                            emails =
                                                [ contactEmail ]

                                            subject =
                                                Builder.string "subject" <|
                                                    "Demande de suppression d'un collaborateur"

                                            bod =
                                                Builder.string "body" <|
                                                    "Territoire\u{00A0}: "
                                                        ++ territoire
                                                        ++ "\nEmail du collaborateur\u{00A0}: \nNom du collaborateur\u{00A0}: \nPrénom du collaborateur\u{00A0}: \nFonction du collaborateur\u{00A0}: \n"

                                            mailto =
                                                "mailto:"
                                                    ++ String.join "," emails
                                                    ++ Builder.toQuery [ subject, bod ]
                                          in
                                          span []
                                            [ Typo.externalLink mailto [] [ text "Demander la suppression d'un collaborateur" ] ]
                                        , if List.length disabledCollabs == 0 then
                                            text "Aucun ancien collaborateur."

                                          else
                                            div [ class "flex flex-col gap-2" ] <|
                                                List.map viewCollaborator <|
                                                    disabledCollabs
                                        ]
                                    ]
                        ]
                )
            |> DSFR.Card.view
        ]


viewCollaborator : User -> Html msg
viewCollaborator collaborator =
    div []
        [ div [] <|
            [ span [ Typo.textBold ] <| List.singleton <| text <| User.email collaborator
            ]
        , div [] <|
            List.singleton <|
                case ( User.prenom collaborator, User.nom collaborator ) of
                    ( "", "" ) ->
                        span [ class "italic" ]
                            [ text <|
                                "Pas de nom"
                            ]

                    ( "", nom ) ->
                        text <|
                            nom

                    ( prenom, "" ) ->
                        text <|
                            prenom

                    ( prenom, nom ) ->
                        text <|
                            prenom
                                ++ " "
                                ++ nom
        ]
