module Pages.Parametres.SIG exposing (Model, Msg, page)

import Accessibility exposing (Html, br, div, h1, li, p, span, text, ul)
import Api
import Api.Auth
import DSFR.Grid
import DSFR.Typography as Typo
import Data.GeoToken exposing (GeoToken, decodeGeoToken)
import Effect
import Html.Attributes exposing (class)
import Json.Decode as Decode
import Lib.Variables exposing (contactEmail)
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


type alias Model =
    { geoTokens : WebData (List GeoToken)
    , appUrl : String
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | ReceivedGeoTokens (WebData (List GeoToken))


page : Shared.Shared -> User -> Page () Shared.Msg (View Msg) Model Msg
page { appUrl } user =
    Spa.Page.onNewFlags (\_ -> NoOp) <|
        Spa.Page.element
            { view = view
            , init = init appUrl
            , update = update user
            , subscriptions = \_ -> Sub.none
            }


init : String -> () -> ( Model, Effect.Effect Shared.Msg Msg )
init appUrl () =
    ( { geoTokens = RD.Loading
      , appUrl = appUrl
      }
    , getGeoTokens
    )
        |> Shared.pageChangeEffects


getGeoTokens : Effect.Effect Shared.Msg Msg
getGeoTokens =
    Effect.fromCmd <|
        Api.Auth.get
            { url = Api.geoTokens
            }
            SharedMsg
            ReceivedGeoTokens
            (Decode.list decodeGeoToken)


update : User -> Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update _ msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        ReceivedGeoTokens geoTokens ->
            { model | geoTokens = geoTokens }
                |> Effect.withNone


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle "SIG"
    , body = [ body model ]
    , route = Route.ParamsSIG
    }


body : Model -> Html Msg
body model =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col3 ] [ UI.Layout.parametresSideMenu Route.ParamsSIG ]
        , div [ DSFR.Grid.col9 ]
            [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ viewGeoTokens model.appUrl model.geoTokens
                ]
            ]
        ]


viewGeoToken : String -> GeoToken -> List (Html msg)
viewGeoToken appUrl geoToken =
    [ text "Jeton\u{00A0}: "
    , span [ Typo.textBold ] [ text geoToken.token ]
    , br []
    , text "Actif\u{00A0}: "
    , span [ Typo.textBold ]
        [ text <|
            if geoToken.active then
                "Oui"

            else
                "Non"
        ]
    , br []
    , span [ Html.Attributes.style "user-select" "none" ] [ text "Lien\u{00A0}: " ]
    , span [ Typo.textBold ] [ text <| appUrl ++ "/api/geojson/" ++ geoToken.token ]
    ]


viewGeoTokens : String -> WebData (List GeoToken) -> Html msg
viewGeoTokens appUrl geoTokens =
    div [ DSFR.Grid.col12, DSFR.Grid.colLg6, class "p-4 sm:p-8" ]
        [ div [ class "fr-card--white p-4" ]
            [ h1 [ Typo.fr_h2 ] [ text "Accès SIG" ]
            , div [ class "flex flex-col" ] <|
                case geoTokens of
                    RD.Success [] ->
                        [ p []
                            [ text "Vous n'avez pas de jeton, veuillez "
                            , Typo.link ("mailto:" ++ contactEmail) [] [ text "nous contacter" ]
                            , text "."
                            ]
                        ]

                    RD.Success tokens ->
                        [ ul [] <|
                            List.map (li []) <|
                                List.map (viewGeoToken appUrl) <|
                                    tokens
                        ]

                    RD.Failure _ ->
                        [ p []
                            [ text "Vous n'avez pas de jeton, veuillez "
                            , Typo.link ("mailto:" ++ contactEmail) [] [ text "nous contacter" ]
                            , text "."
                            ]
                        ]

                    RD.NotAsked ->
                        []

                    RD.Loading ->
                        [ text "Chargement en cours" ]
            ]
        ]
