module Pages.Fiches.Id_ exposing (Model, Msg, page)

import Accessibility exposing (div)
import Api
import Api.Auth
import Api.EntityId exposing (EntityId)
import Data.Fiche exposing (FicheId)
import Effect
import Html exposing (Html)
import Json.Decode as Decode exposing (Decoder)
import RemoteData as RD exposing (WebData)
import Route
import Shared
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page (EntityId FicheId) Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.element <|
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


type Msg
    = ReceivedTypeFiche (WebData TypeFiche)
    | SharedMsg Shared.Msg


type alias Model =
    WebData TypeFiche


type TypeFiche
    = Etablissement String
    | Createur (EntityId FicheId)


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


init : EntityId FicheId -> ( Model, Effect.Effect Shared.Msg Msg )
init id =
    ( RD.Loading
    , Effect.fromCmd <| getFicheType id
    )


getFicheType : EntityId FicheId -> Cmd Msg
getFicheType id =
    Api.Auth.get
        { url = Api.getFicheType id }
        SharedMsg
        ReceivedTypeFiche
        decodeTypeFiche


decodeTypeFiche : Decoder TypeFiche
decodeTypeFiche =
    Decode.field "type" Decode.string
        |> Decode.andThen
            (\typeFiche ->
                case typeFiche of
                    "etablissement" ->
                        Decode.map Etablissement <|
                            Decode.field "siret" <|
                                Decode.string

                    "createur" ->
                        Decode.map Createur <|
                            Decode.field "ficheId" <|
                                Api.EntityId.decodeEntityId

                    _ ->
                        Decode.fail <|
                            "Type inconnu\u{00A0}: "
                                ++ typeFiche
            )


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        ReceivedTypeFiche response ->
            let
                effect =
                    case response of
                        RD.Success (Etablissement siret) ->
                            Effect.fromShared <| Shared.Navigate <| Route.Etablissement siret

                        RD.Success (Createur id) ->
                            Effect.fromShared <| Shared.Navigate <| Route.Createur id

                        _ ->
                            Effect.fromCmd Cmd.none
            in
            response
                |> Effect.with effect


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle <| "Redirection"
    , body = UI.Layout.lazyBody body model
    , route = Route.Fiches
    }


body : Model -> Html Msg
body _ =
    div [] []
