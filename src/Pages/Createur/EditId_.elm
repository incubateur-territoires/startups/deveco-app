module Pages.Createur.EditId_ exposing (Model, Msg(..), page)

import Accessibility exposing (Html, div, formWithListeners, h1, h2, span, sup, text)
import Api
import Api.Auth
import Api.EntityId exposing (EntityId)
import DSFR.Alert
import DSFR.Button
import DSFR.Icons
import DSFR.Icons.System
import DSFR.Input
import DSFR.Tag
import DSFR.Typography as Typo
import Data.Adresse exposing (ApiAdresse, decodeApiFeatures)
import Data.Fiche as Fiche exposing (Fiche, FicheId)
import Effect
import Html.Attributes exposing (class)
import Html.Events as Events
import Html.Extra exposing (nothing)
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import Lib.Variables exposing (hintActivites, hintMotsCles, hintZoneGeographique)
import MultiSelectRemote
import RemoteData as RD exposing (WebData)
import Route
import Shared
import SingleSelectRemote exposing (SelectConfig)
import Spa.Page exposing (Page)
import UI.Layout
import View exposing (View)


page : Shared.Shared -> Shared.User -> Page (EntityId FicheId) Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.element <|
        { init = init
        , update = update
        , view = view
        , subscriptions =
            \model ->
                [ model.selectAdresse |> SingleSelectRemote.subscriptions
                , model.selectActivites |> MultiSelectRemote.subscriptions
                , model.selectLocalisations |> MultiSelectRemote.subscriptions
                , model.selectMots |> MultiSelectRemote.subscriptions
                ]
                    |> Sub.batch
        }


type alias Model =
    { fiche : WebData Fiche
    , ficheId : EntityId FicheId
    , particulier : DataParticulier
    , validationFiche : RD.RemoteData String ()
    , enregistrementFiche : WebData Fiche
    , selectAdresse : SingleSelectRemote.SmartSelect Msg ApiAdresse
    , selectedAdresse : Maybe ApiAdresse
    , selectActivites : MultiSelectRemote.SmartSelect Msg String
    , activites : List String
    , selectLocalisations : MultiSelectRemote.SmartSelect Msg String
    , localisations : List String
    , selectMots : MultiSelectRemote.SmartSelect Msg String
    , mots : List String
    , autre : String
    }


init : EntityId FicheId -> ( Model, Effect.Effect Shared.Msg Msg )
init id =
    ( { fiche = RD.Loading
      , ficheId = id
      , particulier = defaultDataParticulier
      , validationFiche = RD.NotAsked
      , enregistrementFiche = RD.NotAsked
      , selectAdresse =
            SingleSelectRemote.init "champ-selection-adresse"
                { selectionMsg = SelectedAdresse
                , internalMsg = UpdatedSelectAdresse
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , selectedAdresse = Nothing
      , selectActivites =
            MultiSelectRemote.init selectActivitesId
                { selectionMsg = SelectedActivites
                , internalMsg = UpdatedSelectActivites
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , activites = []
      , selectLocalisations =
            MultiSelectRemote.init selectLocalisationsId
                { selectionMsg = SelectedLocalisations
                , internalMsg = UpdatedSelectLocalisations
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , localisations = []
      , selectMots =
            MultiSelectRemote.init selectMotsId
                { selectionMsg = SelectedMots
                , internalMsg = UpdatedSelectMots
                , characterSearchThreshold = selectCharacterThreshold
                , debounceDuration = selectDebounceDuration
                }
      , mots = []
      , autre = ""
      }
    , Effect.fromCmd <| fetchFiche id
    )


selectActivitesId : String
selectActivitesId =
    "champ-selection-activites"


selectLocalisationsId : String
selectLocalisationsId =
    "champ-selection-zones"


selectMotsId : String
selectMotsId =
    "champ-selection-mots"


selectCharacterThreshold : Int
selectCharacterThreshold =
    2


selectDebounceDuration : Float
selectDebounceDuration =
    400


selectActivitesConfig : SelectConfig String
selectActivitesConfig =
    { headers = []
    , url = Api.rechercheActiviteParticulier
    , optionDecoder = Decode.list <| Decode.field "activite" Decode.string
    }


selectLocalisationsConfig : SelectConfig String
selectLocalisationsConfig =
    { headers = []
    , url = Api.rechercheLocalisation
    , optionDecoder = Decode.list <| Decode.field "localisation" Decode.string
    }


selectMotsConfig : SelectConfig String
selectMotsConfig =
    { headers = []
    , url = Api.rechercheMot
    , optionDecoder = Decode.list <| Decode.field "motCle" Decode.string
    }


fetchFiche : EntityId FicheId -> Cmd Msg
fetchFiche id =
    Api.Auth.get
        { url = Api.getFiche id
        }
        SharedMsg
        ReceivedFiche
        Fiche.decodeFiche


apiAdresseToOption : ApiAdresse -> String
apiAdresseToOption =
    .label


defaultDataParticulier : DataParticulier
defaultDataParticulier =
    { nom = ""
    , prenom = ""
    , email = ""
    , telephone = ""
    , adresse = ""
    , ville = ""
    , codePostal = ""
    , geolocation = Nothing
    , description = ""
    , futureEnseigne = ""
    }


type alias DataParticulier =
    { nom : String
    , prenom : String
    , email : String
    , telephone : String
    , adresse : String
    , ville : String
    , codePostal : String
    , geolocation : Maybe String
    , description : String
    , futureEnseigne : String
    }


type Msg
    = ReceivedFiche (WebData Fiche)
    | UpdatedParticulierForm ParticulierField String
    | UpdatedAutre String
    | SelectedActivites ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectActivites (MultiSelectRemote.Msg String)
    | UnselectActivite String
    | SelectedLocalisations ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectLocalisations (MultiSelectRemote.Msg String)
    | UnselectLocalisation String
    | SelectedMots ( List String, MultiSelectRemote.Msg String )
    | UpdatedSelectMots (MultiSelectRemote.Msg String)
    | UnselectMot String
    | RequestedSaveFiche
    | ReceivedSaveFiche (WebData Fiche)
    | SelectedAdresse ( ApiAdresse, SingleSelectRemote.Msg ApiAdresse )
    | UpdatedSelectAdresse (SingleSelectRemote.Msg ApiAdresse)
    | SharedMsg Shared.Msg


type ParticulierField
    = ParticulierNom
    | ParticulierPrenom
    | ParticulierEmail
    | ParticulierTelephone
    | ParticulierDescription
    | ParticulierFutureEnseigne


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        UpdatedParticulierForm field value ->
            ( { model | particulier = updateParticulierForm field value model.particulier }, Effect.none )

        ReceivedSaveFiche response ->
            case response of
                RD.Success _ ->
                    ( { model | enregistrementFiche = response }
                    , Effect.fromShared <|
                        Shared.Navigate <|
                            Route.Createur <|
                                model.ficheId
                    )

                _ ->
                    ( { model | enregistrementFiche = response }, Effect.none )

        RequestedSaveFiche ->
            let
                parsedFiche =
                    validateFiche model

                ( enregistrementFiche, effect ) =
                    case parsedFiche of
                        RD.Success () ->
                            ( RD.Loading, updateFiche model )

                        _ ->
                            ( RD.NotAsked, Effect.none )
            in
            ( { model | validationFiche = parsedFiche, enregistrementFiche = enregistrementFiche }, effect )

        SelectedAdresse ( apiAdresse, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectConfig model.selectAdresse

                particulier =
                    model.particulier
                        |> (\p ->
                                { p
                                    | ville = apiAdresse.city
                                    , codePostal = apiAdresse.postcode
                                    , adresse = apiAdresse.label
                                    , geolocation = Maybe.map2 (\x y -> "(" ++ String.fromFloat x ++ "," ++ String.fromFloat y ++ ")") apiAdresse.x apiAdresse.y
                                }
                           )
            in
            ( { model | selectedAdresse = Just apiAdresse, selectAdresse = updatedSelect, particulier = particulier }, Effect.fromCmd selectCmd )

        UpdatedSelectAdresse sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelectRemote.update sMsg selectConfig model.selectAdresse
            in
            ( { model | selectAdresse = updatedSelect }, Effect.fromCmd selectCmd )

        ReceivedFiche response ->
            case response of
                RD.Success fiche ->
                    case ( fiche.entite.etablissement, fiche.entite.createur ) of
                        ( Just entreprise, _ ) ->
                            ( model
                            , Effect.fromShared <|
                                Shared.Navigate <|
                                    Route.Etablissement <|
                                        entreprise.siret
                            )

                        ( _, Just p ) ->
                            let
                                activites =
                                    Fiche.activitesReelles fiche

                                localisations =
                                    Fiche.localisations fiche

                                mots =
                                    Fiche.motsCles fiche

                                particulier =
                                    DataParticulier p.nom p.prenom p.email p.telephone p.adresse p.ville p.codePostal p.geolocation p.description <|
                                        Maybe.withDefault "" <|
                                            fiche.entite.futureEnseigne

                                ( updatedSelectAdresse, selectCmd ) =
                                    SingleSelectRemote.setText particulier.adresse selectConfig model.selectAdresse
                            in
                            ( { model
                                | selectAdresse = updatedSelectAdresse
                                , fiche = response
                                , particulier = particulier
                                , activites = activites
                                , localisations = localisations
                                , mots = mots
                              }
                            , Effect.fromCmd selectCmd
                            )

                        _ ->
                            ( model, Effect.none )

                _ ->
                    ( model, Effect.none )

        UpdatedAutre autre ->
            ( { model | autre = autre }
            , Effect.none
            )

        SelectedActivites ( activites, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectActivitesConfig model.selectActivites

                activitesUniques =
                    case activites of
                        [] ->
                            []

                        a :: rest ->
                            if List.member a rest then
                                rest

                            else
                                activites
            in
            ( { model
                | selectActivites = updatedSelect
                , activites = activitesUniques
              }
            , Effect.fromCmd selectCmd
            )

        UpdatedSelectActivites sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectActivitesConfig model.selectActivites
            in
            ( { model
                | selectActivites = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectActivite activite ->
            if model.enregistrementFiche == RD.Loading then
                ( model, Effect.none )

            else
                ( { model
                    | activites =
                        model.activites
                            |> List.filter ((/=) activite)
                  }
                , Effect.none
                )

        SelectedLocalisations ( localisations, sMsg ) ->
            if model.enregistrementFiche == RD.Loading then
                ( model, Effect.none )

            else
                let
                    ( updatedSelect, selectCmd ) =
                        MultiSelectRemote.update sMsg selectLocalisationsConfig model.selectLocalisations

                    localisationsUniques =
                        case localisations of
                            [] ->
                                []

                            a :: rest ->
                                if List.member a rest then
                                    rest

                                else
                                    localisations
                in
                ( { model
                    | selectLocalisations = updatedSelect
                    , localisations = localisationsUniques
                  }
                , Effect.fromCmd selectCmd
                )

        UpdatedSelectLocalisations sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectLocalisationsConfig model.selectLocalisations
            in
            ( { model
                | selectLocalisations = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectLocalisation localisation ->
            ( { model
                | localisations =
                    model.localisations
                        |> List.filter ((/=) localisation)
              }
            , Effect.none
            )

        SelectedMots ( mots, sMsg ) ->
            if model.enregistrementFiche == RD.Loading then
                ( model, Effect.none )

            else
                let
                    ( updatedSelect, selectCmd ) =
                        MultiSelectRemote.update sMsg selectMotsConfig model.selectMots

                    motsUniques =
                        case mots of
                            [] ->
                                []

                            a :: rest ->
                                if List.member a rest then
                                    rest

                                else
                                    mots
                in
                ( { model
                    | selectMots = updatedSelect
                    , mots = motsUniques
                  }
                , Effect.fromCmd selectCmd
                )

        UpdatedSelectMots sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    MultiSelectRemote.update sMsg selectMotsConfig model.selectMots
            in
            ( { model
                | selectMots = updatedSelect
              }
            , Effect.fromCmd selectCmd
            )

        UnselectMot mot ->
            ( { model | mots = model.mots |> List.filter ((/=) mot) }
            , Effect.none
            )


selectConfig : SelectConfig ApiAdresse
selectConfig =
    { headers = []
    , url = Api.rechercheAdresse Nothing Nothing Nothing
    , optionDecoder = decodeApiFeatures
    }


validateFiche : Model -> RD.RemoteData String ()
validateFiche { particulier } =
    RD.fromResult <|
        if String.trim particulier.nom == "" || String.trim particulier.prenom == "" then
            Err "Veuillez remplir les champs marqués d'un astérisque."

        else
            Ok ()


updateParticulierForm : ParticulierField -> String -> DataParticulier -> DataParticulier
updateParticulierForm field value particulier =
    case field of
        ParticulierNom ->
            { particulier | nom = value }

        ParticulierPrenom ->
            { particulier | prenom = value }

        ParticulierEmail ->
            { particulier | email = value }

        ParticulierTelephone ->
            { particulier | telephone = value }

        ParticulierDescription ->
            { particulier | description = value }

        ParticulierFutureEnseigne ->
            { particulier | futureEnseigne = value }


updateFiche : Model -> Effect.Effect Shared.Msg Msg
updateFiche { activites, localisations, mots, autre, particulier, ficheId } =
    let
        jsonBody =
            particulier
                |> (\{ nom, prenom, email, telephone, adresse, ville, codePostal, geolocation, description, futureEnseigne } ->
                        [ ( "activites", Encode.list Encode.string activites )
                        , ( "localisations", Encode.list Encode.string localisations )
                        , ( "mots", Encode.list Encode.string mots )
                        , ( "autre", Encode.string autre )
                        , ( "particulier"
                          , [ ( "nom", Encode.string nom )
                            , ( "prenom", Encode.string prenom )
                            , ( "email", Encode.string email )
                            , ( "telephone", Encode.string telephone )
                            , ( "adresse", Encode.string adresse )
                            , ( "ville", Encode.string ville )
                            , ( "codePostal", Encode.string codePostal )
                            , ( "geolocation", Maybe.withDefault Encode.null <| Maybe.map Encode.string <| geolocation )
                            , ( "description", Encode.string description )
                            , ( "futureEnseigne", Encode.string futureEnseigne )
                            ]
                                |> Encode.object
                          )
                        ]
                   )
                |> (Encode.object >> Http.jsonBody)
    in
    Api.Auth.post
        { url = Api.updateFiche ficheId
        , body = jsonBody
        }
        SharedMsg
        ReceivedSaveFiche
        Fiche.decodeFiche
        |> Effect.fromCmd


view : Model -> View Msg
view model =
    { title =
        UI.Layout.pageTitle <|
            RD.withDefault "Modification" <|
                RD.map (\fiche -> "Modification de " ++ Fiche.nom fiche) <|
                    model.fiche
    , body = UI.Layout.lazyBody body model
    , route = Route.CreateurNew
    }


ficheTypeForm : Model -> Html Msg
ficheTypeForm model =
    case model.fiche of
        RD.NotAsked ->
            text "Une erreur s'est produite"

        RD.Loading ->
            text "Chargement en cours"

        RD.Failure _ ->
            text "Une erreur s'est produite"

        RD.Success _ ->
            div [ class "flex flex-col gap-8" ]
                [ div [ class "flex flex-col gap-4" ]
                    [ div [ class "flex font-bold" ] [ h2 [ Typo.fr_h6, class "w-full border-b-4 border-france-blue !mb-0" ] [ text "Identité et contact du Créateur d'entreprise" ] ]
                    , div [ class "flex flex-col sm:max-w-[60%]" ]
                        [ div [ class "flex flex-row sm:flex-nowrap flex-wrap gap-4 w-full" ]
                            [ DSFR.Input.new
                                { value = model.particulier.nom
                                , onInput = UpdatedParticulierForm ParticulierNom
                                , label = text "Nom *"
                                , name = "fiche-existante-particulier-nom"
                                }
                                |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                                |> DSFR.Input.view
                            , DSFR.Input.new
                                { value = model.particulier.prenom
                                , onInput = UpdatedParticulierForm ParticulierPrenom
                                , label = text "Prénom *"
                                , name = "fiche-existante-particulier-prenom"
                                }
                                |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                                |> DSFR.Input.view
                            ]
                        ]
                    , div [ class "flex flex-col sm:max-w-[60%]" ]
                        [ div [ class "flex flex-row sm:flex-nowrap flex-wrap gap-4 w-full" ]
                            [ DSFR.Input.new
                                { value = model.particulier.futureEnseigne
                                , onInput = UpdatedParticulierForm ParticulierFutureEnseigne
                                , label = text "Nom d'enseigne pressenti"
                                , name = "fiche-existante-particulier-futureEnseigne"
                                }
                                |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                                |> DSFR.Input.view
                            ]
                        ]
                    , span [ class "sm:max-w-[60%]" ]
                        [ model.selectAdresse
                            |> SingleSelectRemote.viewCustom
                                { isDisabled = False
                                , selected = model.selectedAdresse
                                , optionLabelFn = apiAdresseToOption
                                , optionDescriptionFn = \_ -> ""
                                , optionsContainerMaxHeight = 300
                                , selectTitle = text "Adresse"
                                , searchPrompt = "Rechercher une adresse"
                                , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                , noResultsForMsg = \searchText -> "Aucune autre adresse n'a été trouvée pour " ++ searchText
                                , noOptionsMsg = "Aucune adresse n'a été trouvée"
                                , error = Nothing
                                }
                        ]
                    , div [ class "flex flex-row sm:flex-nowrap flex-wrap gap-4 w-full sm:max-w-[60%]" ]
                        [ DSFR.Input.new
                            { value = model.particulier.email
                            , onInput = UpdatedParticulierForm ParticulierEmail
                            , label = text "Email"
                            , name = "fiche-existante-particulier-email"
                            }
                            |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                            |> DSFR.Input.view
                        , DSFR.Input.new
                            { value = model.particulier.telephone
                            , onInput = UpdatedParticulierForm ParticulierTelephone
                            , label = text "Téléphone"
                            , name = "fiche-existante-particulier-telephone"
                            }
                            |> DSFR.Input.withExtraAttrs [ class "w-full !mb-0" ]
                            |> DSFR.Input.view
                        ]
                    ]
                , div [ class "flex flex-col gap-4" ]
                    [ div [ class "flex font-bold" ] [ h2 [ Typo.fr_h6, class "w-full border-b-4 border-france-blue !mb-0" ] [ text "Activité du Créateur d'entreprise" ] ]
                    , div [ class "flex flex-col gap-4 sm:max-w-[60%]" ]
                        [ model.selectActivites
                            |> MultiSelectRemote.viewCustom
                                { isDisabled = False
                                , selected = model.activites
                                , optionLabelFn = identity
                                , optionDescriptionFn = \_ -> ""
                                , optionsContainerMaxHeight = 300
                                , selectTitle = span [] [ text "Activités réelles et filières", sup [ Html.Attributes.title hintActivites ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                , viewSelectedOptionFn = text
                                , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                , noResultsForMsg = \searchText -> "Aucune autre activité n'a été trouvée pour " ++ searchText
                                , noOptionsMsg = "Aucune activité n'a été trouvée"
                                , newOption = Just identity
                                }
                        , model.activites
                            |> List.sort
                            |> List.map (\activite -> DSFR.Tag.deletable UnselectActivite { data = activite, toString = identity })
                            |> DSFR.Tag.medium
                        ]
                    , div [ class "flex flex-col gap-4 sm:max-w-[60%]" ]
                        [ model.selectLocalisations
                            |> MultiSelectRemote.viewCustom
                                { isDisabled = False
                                , selected = model.localisations
                                , optionLabelFn = identity
                                , optionDescriptionFn = \_ -> ""
                                , optionsContainerMaxHeight = 300
                                , selectTitle = span [] [ text "Zone géographique", sup [ Html.Attributes.title hintZoneGeographique ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                , viewSelectedOptionFn = text
                                , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                , noResultsForMsg = \searchText -> "Aucune autre zone géographique n'a été trouvée pour " ++ searchText
                                , noOptionsMsg = "Aucune zone géographique n'a été trouvée"
                                , newOption = Just identity
                                }
                        , model.localisations
                            |> List.sort
                            |> List.map (\localisation -> DSFR.Tag.deletable UnselectLocalisation { data = localisation, toString = identity })
                            |> DSFR.Tag.medium
                        ]
                    , div [ class "flex flex-col gap-4 sm:max-w-[60%]" ]
                        [ model.selectMots
                            |> MultiSelectRemote.viewCustom
                                { isDisabled = False
                                , selected = model.mots
                                , optionLabelFn = identity
                                , optionDescriptionFn = \_ -> ""
                                , optionsContainerMaxHeight = 300
                                , selectTitle = span [] [ text "Mots-clés", sup [ Html.Attributes.title hintMotsCles ] [ span [ Typo.textXs ] [ DSFR.Icons.iconSM DSFR.Icons.System.informationLine ] ] ]
                                , viewSelectedOptionFn = text
                                , characterThresholdPrompt = \_ -> "Veuillez taper au moins " ++ String.fromInt selectCharacterThreshold ++ " caractères pour lancer la recherche"
                                , queryErrorMsg = "Une erreur s'est produite, veuillez réessayer."
                                , noResultsForMsg = \searchText -> "Aucun autre mot-clé n'a été trouvé pour " ++ searchText
                                , noOptionsMsg = "Aucun mot-clé n'a été trouvé"
                                , newOption = Just identity
                                }
                        , model.mots
                            |> List.sort
                            |> List.map (\mot -> DSFR.Tag.deletable UnselectMot { data = mot, toString = identity })
                            |> DSFR.Tag.medium
                        ]
                    , DSFR.Input.new { value = model.autre, label = span [] [ text "Commentaires" ], onInput = UpdatedAutre, name = "autre" }
                        |> DSFR.Input.textArea (Just 4)
                        |> DSFR.Input.view
                        |> List.singleton
                        |> div [ class "sm:max-w-[60%]" ]
                    , div [ class "flex flex-col sm:max-w-[60%]" ]
                        [ DSFR.Input.new
                            { value = model.particulier.description
                            , onInput = UpdatedParticulierForm ParticulierDescription
                            , label = text "Description du projet (activité, produit)"
                            , name = "fiche-existante-particulier-activite"
                            }
                            |> DSFR.Input.textArea (Just 8)
                            |> DSFR.Input.withExtraAttrs [ class "w-full" ]
                            |> DSFR.Input.view
                        ]
                    ]
                ]


body : Model -> Html Msg
body model =
    div [ class "fr-card--white p-4 sm:p-8 flex flex-col gap-8" ]
        [ h1 [ class "!mb-0" ] [ text "Modifier une fiche Créateur d'entreprise" ]
        , formWithListeners [ Events.onSubmit <| RequestedSaveFiche, class "flex flex-col gap-8" ]
            [ ficheTypeForm model
            , footer model
            ]
        ]


footer : Model -> Html Msg
footer model =
    div [ class "flex flex-col gap-2" ]
        [ div [ class "flex flex-row justify-end" ]
            [ case model.validationFiche of
                RD.Failure err ->
                    DSFR.Alert.small { title = Nothing, description = text err }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.info

                _ ->
                    nothing
            , case model.enregistrementFiche of
                RD.Success _ ->
                    DSFR.Alert.small { title = Nothing, description = text "Modifications enregistrées\u{00A0}!" }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.success

                RD.Failure _ ->
                    DSFR.Alert.small { title = Nothing, description = text "Une erreur s'est produite, veuillez réessayer." }
                        |> DSFR.Alert.alert Nothing DSFR.Alert.error

                _ ->
                    nothing
            ]
        , DSFR.Button.group
            [ DSFR.Button.new { onClick = Nothing, label = "Enregistrer" }
                |> DSFR.Button.submit
                |> DSFR.Button.withDisabled (model.enregistrementFiche == RD.Loading)
            , DSFR.Button.new { onClick = Nothing, label = "Précédent" }
                |> DSFR.Button.secondary
                |> DSFR.Button.linkButton (Route.toUrl <| Route.Createur model.ficheId)
            ]
            |> DSFR.Button.inline
            |> DSFR.Button.alignedRightInverted
            |> DSFR.Button.viewGroup
        ]
