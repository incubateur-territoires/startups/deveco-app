module Pages.Activite exposing (Model, Msg, page)

import Accessibility exposing (Html, div, h1, h2, h3, span, text)
import Api
import Api.Auth
import DSFR.Button
import DSFR.Grid
import DSFR.Tabs
import DSFR.Typography as Typo
import Data.Demande
import Data.Echange
import Data.Role
import Effect
import Html
import Html.Attributes exposing (class)
import Html.Extra exposing (viewIf)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import Json.Encode as Encode
import Lib.UI exposing (numberCard, plural)
import List.Extra
import Pages.Etablissements.Filters
import QS
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import Spa.Page exposing (Page)
import Time
import UI.Echange
import UI.Entite
import UI.Layout
import User
import View exposing (View)


type alias Model =
    { etablissementsStats : WebData EtablissementsStatsData
    , createursStats : WebData CreateursStatsData
    , currentDate : StatsDate
    , currentTab : StatsTab
    }


type StatsTab
    = StatsEtablissements
    | StatsCreateurs


type StatsDate
    = Semaine


statsTabToString : StatsTab -> String
statsTabToString statsTab =
    case statsTab of
        StatsEtablissements ->
            "etablissements"

        StatsCreateurs ->
            "createurs"


statsTabToDisplay : StatsTab -> String
statsTabToDisplay statsTab =
    case statsTab of
        StatsEtablissements ->
            "Établissements"

        StatsCreateurs ->
            "Créateurs d'entreprise"


stringToStatsTab : String -> Maybe StatsTab
stringToStatsTab s =
    case s of
        "etablissements" ->
            Just StatsEtablissements

        "createurs" ->
            Just StatsCreateurs

        _ ->
            Nothing


type alias EtablissementsStatsData =
    { etablissements : EtablissementsStats
    , rappels : RappelsStats
    , echanges : List EchangesStats
    , demandes : DemandesStats
    , actions : ActionsStats
    }


type alias CreateursStatsData =
    { createurs : CreateursStats
    , rappels : RappelsStats
    , echanges : List EchangesStats
    , demandes : DemandesStats
    , actions : ActionsStats
    }


type alias EtablissementsStats =
    { nouveaux : Int
    , qpvs : Int
    , exogenes : Int
    }


type alias CreateursStats =
    { nouveaux : Int
    , qpvs : Int
    , transformes : Int
    }


type alias RappelsStats =
    { nouveaux : Int
    , expires : Int
    , clotures : Int
    }


type alias EchangesStats =
    { auteur : String
    , type_ : String
    }


type alias DemandesStats =
    { nouvelles : Int
    , ouvertes : List String
    , cloturees : Int
    }


type alias ActionsStats =
    { vues : Int
    , qualifications : Int
    , etiquetages : Int
    }


type Msg
    = ReceivedEtablissementsStats (WebData EtablissementsStatsData)
    | ReceivedCreateursStats (WebData CreateursStatsData)
    | ClickedChangeTab StatsTab
    | ChangeTab StatsTab
      -- | ChangeStatsDate StatsDate
      -- | NoOp
    | SharedMsg Shared.Msg


page : Shared.Shared -> User -> Page (Maybe String) Shared.Msg (View Msg) Model Msg
page { now, timezone } identity =
    Spa.Page.onNewFlags (parseStatsTab >> ChangeTab) <|
        Spa.Page.element
            { view = view now timezone identity
            , init = init identity
            , update = update
            , subscriptions = \_ -> Sub.none
            }


parseStatsTab : Maybe String -> StatsTab
parseStatsTab =
    Maybe.withDefault ""
        >> QS.parse QS.config
        >> (\q ->
                case QS.getAsStringList "onglet" q of
                    "createurs" :: _ ->
                        StatsCreateurs

                    _ ->
                        StatsEtablissements
           )


serializeStatsTab : StatsTab -> Maybe String
serializeStatsTab statsTab =
    case statsTab of
        StatsCreateurs ->
            QS.empty
                |> QS.setStr "onglet" "createurs"
                |> QS.serialize (QS.config |> QS.addQuestionMark False)
                |> Just

        StatsEtablissements ->
            Nothing


init : User -> Maybe String -> ( Model, Effect.Effect Shared.Msg Msg )
init user query =
    let
        currentTab =
            parseStatsTab query

        ( cmd, etablissementsStats, createursStats ) =
            if currentTab == StatsEtablissements then
                ( getEtablissementsStats, RD.Loading, RD.NotAsked )

            else
                ( getCreateursStats, RD.NotAsked, RD.Loading )
    in
    ( { etablissementsStats = etablissementsStats
      , createursStats = createursStats
      , currentDate = Semaine
      , currentTab = currentTab
      }
    , if Data.Role.isDeveco <| User.role user then
        Effect.fromCmd cmd

      else
        Effect.fromShared <|
            Shared.ReplaceUrl <|
                Route.defaultPageForRole <|
                    User.role user
    )
        |> Shared.pageChangeEffects


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        -- NoOp ->
        --     model
        --         |> Effect.withNone
        --
        ReceivedEtablissementsStats response ->
            { model | etablissementsStats = response }
                |> Effect.withNone

        ReceivedCreateursStats response ->
            { model | createursStats = response }
                |> Effect.withNone

        ClickedChangeTab statsTab ->
            model
                |> Effect.with
                    (Effect.fromShared <|
                        Shared.Navigate <|
                            Route.Activite <|
                                serializeStatsTab statsTab
                    )

        ChangeTab statsTab ->
            if statsTab == StatsEtablissements then
                let
                    ( state, effect ) =
                        case model.etablissementsStats of
                            RD.Failure _ ->
                                ( RD.Loading, Effect.fromCmd getEtablissementsStats )

                            RD.NotAsked ->
                                ( RD.Loading, Effect.fromCmd getEtablissementsStats )

                            _ ->
                                ( model.etablissementsStats, Effect.none )
                in
                { model | currentTab = statsTab, etablissementsStats = state }
                    |> Effect.with effect

            else
                let
                    ( state, effect ) =
                        case model.createursStats of
                            RD.Failure _ ->
                                ( RD.Loading, Effect.fromCmd getCreateursStats )

                            RD.NotAsked ->
                                ( RD.Loading, Effect.fromCmd getCreateursStats )

                            _ ->
                                ( model.createursStats, Effect.none )
                in
                { model | currentTab = statsTab, createursStats = state }
                    |> Effect.with effect

        -- ChangeStatsDate statsDate ->
        --     { model | currentDate = statsDate }
        --         |> Effect.withNone
        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg


view : Time.Posix -> Time.Zone -> User -> Model -> View Msg
view now timezone identity model =
    { title =
        UI.Layout.pageTitle <| "Activité du service"
    , body = [ body now timezone identity model ]
    , route = Route.Activite Nothing
    }


body : Time.Posix -> Time.Zone -> User -> Model -> Html Msg
body _ _ _ model =
    div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ div [ DSFR.Grid.col12, class "flex" ]
            [ h1 [ Typo.fr_h4, class "!mb-0" ]
                [ text "Activité du service - Tableau de bord" ]
            ]
        , div [ DSFR.Grid.col12 ]
            [ DSFR.Tabs.new
                { changeTabMsg = stringToStatsTab >> Maybe.withDefault StatsEtablissements >> ClickedChangeTab
                , tabs =
                    [ { id = StatsEtablissements |> statsTabToString
                      , title = StatsEtablissements |> statsTabToDisplay
                      , icon = UI.Entite.iconeEtablissement
                      , content = etablissementsTab model.etablissementsStats
                      }
                    , { id = StatsCreateurs |> statsTabToString
                      , title = StatsCreateurs |> statsTabToDisplay
                      , icon = UI.Entite.iconeCreateur
                      , content = createursTab model.createursStats
                      }
                    ]
                }
                |> DSFR.Tabs.view (statsTabToString model.currentTab)
            ]
        ]


etablissementsTab : WebData EtablissementsStatsData -> Html Msg
etablissementsTab wdStats =
    div [ class "flex flex-col fr-card--white gap-8" ] <|
        case wdStats of
            RD.Success { etablissements, echanges, rappels, demandes, actions } ->
                [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ div [ DSFR.Grid.col12, class "flex flex-col gap-4" ]
                        [ div [ class "" ]
                            [ h2 [ Typo.fr_h4, class "!mb-0" ] [ text "Établissements" ]
                            ]
                        , div [ class "flex flex-row justify-between items-center" ]
                            [ text "Données des 7 derniers jours"
                            , DSFR.Button.new { label = "Exporter", onClick = Nothing }
                                |> DSFR.Button.withAttrs [ Html.Attributes.title "Indisponible pour le moment" ]
                                |> DSFR.Button.withDisabled True
                                -- |> DSFR.Button.linkButton (Route.toUrl <| Route.Connexion Nothing)
                                |> DSFR.Button.primary
                                |> DSFR.Button.view
                            ]
                        ]
                    ]
                , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ div [ DSFR.Grid.col6 ]
                        [ etablissementsBlocs etablissements
                        ]
                    , div [ DSFR.Grid.col6 ]
                        [ rappelsBlocs rappels
                        ]
                    ]
                , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ echangesSection echanges
                    ]
                , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ demandesBlocs demandes
                    , demandesGraphiques demandes
                    , actionsBlocs True actions
                    ]
                ]

            RD.Loading ->
                [ text "Chargement des statistiques en cours..." ]

            _ ->
                [ text "Une erreur s'est produite, veuillez recharger la page et nous contacter si le problème persiste." ]


createursTab : WebData CreateursStatsData -> Html Msg
createursTab wdStats =
    div [ class "flex flex-col fr-card--white gap-8" ] <|
        case wdStats of
            RD.Success { createurs, echanges, rappels, demandes, actions } ->
                [ div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ div [ DSFR.Grid.col12, class "flex flex-col gap-4" ]
                        [ div []
                            [ h2 [ Typo.fr_h4, class "!mb-0" ] [ text "Créateurs d'entreprise" ]
                            ]
                        , div [ class "flex flex-row justify-between items-center" ]
                            [ text "Données des 7 derniers jours"
                            , DSFR.Button.new { label = "Exporter", onClick = Nothing }
                                |> DSFR.Button.withAttrs [ Html.Attributes.title "Indisponible pour le moment" ]
                                |> DSFR.Button.withDisabled True
                                -- |> DSFR.Button.linkButton (Route.toUrl <| Route.Connexion Nothing)
                                |> DSFR.Button.primary
                                |> DSFR.Button.view
                            ]
                        ]
                    ]
                , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ div [ DSFR.Grid.col6 ]
                        [ createursBlocs createurs
                        ]
                    , div [ DSFR.Grid.col6 ]
                        [ rappelsBlocs rappels
                        ]
                    ]
                , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ echangesSection echanges
                    ]
                , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                    [ demandesBlocs demandes
                    , demandesGraphiques demandes
                    , actionsBlocs False actions
                    ]
                ]

            RD.Loading ->
                [ text "Chargement des statistiques en cours..." ]

            _ ->
                [ text "Une erreur s'est produite, veuillez recharger la page et nous contacter si le problème persiste." ]


etablissementsBlocs : { nouveaux : Int, qpvs : Int, exogenes : Int } -> Html msg
etablissementsBlocs etablissements =
    div [ class "flex flex-col gap-4 fr-card--grey p-4" ]
        [ div [ class "flex flex-row justify-between items-center" ]
            [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Portefeuille du service" ]
            , DSFR.Button.new { label = "Voir le Portefeuille", onClick = Nothing }
                |> DSFR.Button.linkButton (Route.toUrl <| Route.Etablissements <| Just <| Pages.Etablissements.Filters.makeQueryForPortefeuilleMode ())
                |> DSFR.Button.secondary
                |> DSFR.Button.view
            ]
        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col4 ]
                [ numberCard []
                    "+"
                    [ "établiss."
                    ]
                    etablissements.nouveaux
                ]
            , div [ DSFR.Grid.col4 ]
                [ numberCard []
                    ""
                    [ "dont "
                    , String.fromInt etablissements.qpvs
                    , " issu"
                    , plural etablissements.qpvs
                    , " de QPV"
                    ]
                    etablissements.qpvs
                ]
            , div [ DSFR.Grid.col4 ]
                [ numberCard []
                    ""
                    [ "dont "
                    , String.fromInt etablissements.exogenes
                    , " établiss. exogène"
                    ]
                    etablissements.exogenes
                ]
            ]
        ]


createursBlocs : { nouveaux : Int, qpvs : Int, transformes : Int } -> Html msg
createursBlocs createurs =
    div [ class "flex flex-col gap-4 fr-card--grey p-4" ]
        [ div [ class "flex flex-row justify-between items-center" ]
            [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Créateurs d'entreprise" ]
            , DSFR.Button.new { label = "Voir tous les créateurs", onClick = Nothing }
                |> DSFR.Button.linkButton (Route.toUrl <| Route.Createurs Nothing)
                |> DSFR.Button.secondary
                |> DSFR.Button.view
            ]
        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col4 ]
                [ numberCard []
                    "+"
                    [ "créateur"
                    , plural createurs.nouveaux
                    , " créé"
                    , plural createurs.nouveaux
                    ]
                    createurs.nouveaux
                ]
            , div [ DSFR.Grid.col4 ]
                [ numberCard []
                    ""
                    [ "dont "
                    , String.fromInt createurs.qpvs
                    , " issu"
                    , plural createurs.qpvs
                    , " de QPV"
                    ]
                    createurs.qpvs
                ]
            , div [ DSFR.Grid.col4 ]
                [ numberCard []
                    ""
                    [ "transformé"
                    , plural createurs.transformes
                    , " en établiss."
                    ]
                    createurs.transformes
                ]
            ]
        ]


rappelsBlocs : RappelsStats -> Html msg
rappelsBlocs rappels =
    div [ class "flex flex-col gap-4 fr-card--grey p-4" ]
        [ div [ class "flex flex-row justify-between items-center" ]
            [ h3 [ Typo.fr_h6, class "!mb-0" ] [ text "Rappels" ]
            , DSFR.Button.new { label = "Voir tous les rappels", onClick = Nothing }
                |> DSFR.Button.linkButton (Route.toUrl <| Route.Rappels)
                |> DSFR.Button.secondary
                |> DSFR.Button.view
            ]
        , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
            [ div [ DSFR.Grid.col4 ]
                [ numberCard []
                    ""
                    [ "rappel"
                    , plural rappels.nouveaux
                    , " créé"
                    , plural rappels.nouveaux
                    ]
                    rappels.nouveaux
                ]
            , div [ DSFR.Grid.col4 ]
                [ numberCard []
                    ""
                    [ "arrivant à échéance"
                    ]
                    rappels.expires
                ]
            , div [ DSFR.Grid.col4 ]
                [ numberCard []
                    ""
                    [ "rappel"
                    , plural rappels.clotures
                    , " clôturé"
                    , plural rappels.clotures
                    ]
                    rappels.clotures
                ]
            ]
        ]


echangesSection : List EchangesStats -> Html msg
echangesSection echanges =
    div [ DSFR.Grid.col12 ]
        [ h3 [ Typo.fr_h6 ] [ text "Échanges" ]
        , div [ DSFR.Grid.gridRow ]
            [ div [ DSFR.Grid.col2, class "" ]
                [ numberCard []
                    "+"
                    [ "échange"
                    , plural <|
                        List.length echanges
                    ]
                    (List.length echanges)
                ]
            , div [ DSFR.Grid.col5, class "p-4 box-shadow" ]
                [ div [ DSFR.Grid.gridRow ]
                    [ div [ DSFR.Grid.col4 ]
                        [ span [ Typo.textBold ] [ text "Répartition des échanges par type de contact" ] ]
                    , div [ DSFR.Grid.col8 ]
                        [ Html.node "chart-pie"
                            [ Encode.object
                                [ ( "values"
                                  , echanges
                                        |> List.Extra.gatherEqualsBy .type_
                                        |> List.map (\( e, rest ) -> ( e.type_, 1 + List.length rest ))
                                        |> List.sortBy (\( _, length ) -> length)
                                        |> List.reverse
                                        |> Encode.list
                                            (\( type_, count ) ->
                                                Encode.object
                                                    [ ( "label", Encode.string <| type_ )
                                                    , ( "count", Encode.float <| toFloat count )
                                                    ]
                                            )
                                  )
                                , ( "config"
                                  , Encode.object
                                        [ ( "title", Encode.string "répartition" )
                                        , ( "height", Encode.int 200 )
                                        ]
                                  )
                                ]
                                |> Encode.encode 0
                                |> Html.Attributes.attribute "data"
                            ]
                            []
                        ]
                    ]
                ]
            , div [ DSFR.Grid.col5, class "p-4 box-shadow" ]
                [ div [ DSFR.Grid.gridRow ]
                    [ div [ DSFR.Grid.col4 ]
                        [ span [ Typo.textBold ] [ text "Alimentation de l'outil par des échanges" ] ]
                    , div [ DSFR.Grid.col8 ]
                        [ Html.node "chart-pie"
                            [ Encode.object
                                [ ( "values"
                                  , echanges
                                        |> List.Extra.gatherEqualsBy .auteur
                                        |> List.map (\( e, rest ) -> ( e.auteur, 1 + List.length rest ))
                                        |> List.sortBy (\( _, length ) -> length)
                                        |> List.reverse
                                        |> Encode.list
                                            (\( type_, count ) ->
                                                Encode.object
                                                    [ ( "label", Encode.string <| type_ )
                                                    , ( "count", Encode.float <| toFloat count )
                                                    ]
                                            )
                                  )
                                , ( "config"
                                  , Encode.object
                                        [ ( "title", Encode.string "répartition" )
                                        , ( "height", Encode.int 200 )
                                        ]
                                  )
                                ]
                                |> Encode.encode 0
                                |> Html.Attributes.attribute "data"
                            ]
                            []
                        ]
                    ]
                ]
            ]
        ]


demandesBlocs : DemandesStats -> Html msg
demandesBlocs demandes =
    div [ DSFR.Grid.col2, class "!pt-4 !pr-4 !pb-0" ]
        [ h3 [ Typo.fr_h6 ] [ text "Demandes" ]
        , div [ class "flex flex-col" ]
            [ numberCard []
                "+"
                [ "nouvelle"
                , plural demandes.nouvelles
                , " demande"
                , plural demandes.nouvelles
                ]
                demandes.nouvelles
            , numberCard []
                ""
                [ "demande"
                , plural <| List.length demandes.ouvertes
                , " en cours (au total)"
                ]
                (List.length demandes.ouvertes)
            , numberCard []
                ""
                [ "demande"
                , plural demandes.cloturees
                , " clôturée"
                , plural demandes.cloturees
                ]
                demandes.cloturees
            ]
        ]


demandesGraphiques : DemandesStats -> Html msg
demandesGraphiques demandes =
    div [ DSFR.Grid.col8, class "fr-card--grey !p-4 !pb-0" ]
        [ h3 [ Typo.fr_h6 ] [ text "Types de demandes en cours" ]
        , Html.node "chart-bar-horizontal"
            [ Encode.object
                [ ( "values"
                  , demandes.ouvertes
                        |> List.Extra.gatherEquals
                        |> List.map (\( type_, rest ) -> ( type_, 1 + List.length rest ))
                        |> List.sortBy (\( _, length ) -> length)
                        |> List.reverse
                        |> Encode.list
                            (\( type_, count ) ->
                                Encode.object
                                    [ ( "label", Encode.string <| type_ )
                                    , ( "count", Encode.float <| toFloat count )
                                    ]
                            )
                  )
                ]
                |> Encode.encode 0
                |> Html.Attributes.attribute "data"
            ]
            []
        ]


actionsBlocs : Bool -> ActionsStats -> Html msg
actionsBlocs withQualifications actions =
    div [ DSFR.Grid.col2, class "!pt-4 !pl-4 !pb-0" ]
        [ h3 [ Typo.fr_h6 ] [ text "Actions" ]
        , div [ class "flex flex-col" ]
            [ numberCard []
                ""
                [ "consultation"
                , plural actions.vues
                , " de fiches"
                ]
                actions.vues
            , viewIf withQualifications <|
                numberCard []
                    ""
                    [ "qualification"
                    , plural actions.qualifications
                    , " de la base de données"
                    ]
                    actions.qualifications
            , numberCard []
                ""
                [ "action"
                , plural actions.etiquetages
                , " sur étiquettes"
                ]
                actions.etiquetages
            ]
        ]


getEtablissementsStats : Cmd Msg
getEtablissementsStats =
    Api.Auth.get
        { url = Api.getActiviteStats "etablissements" }
        SharedMsg
        ReceivedEtablissementsStats
        decodeEtablissementsStats


getCreateursStats : Cmd Msg
getCreateursStats =
    Api.Auth.get
        { url = Api.getActiviteStats "createurs" }
        SharedMsg
        ReceivedCreateursStats
        decodeCreateursStats


decodeEtablissementsStats : Decoder EtablissementsStatsData
decodeEtablissementsStats =
    Decode.succeed EtablissementsStatsData
        |> andMap decodeEtablissements
        |> andMap decodeRappels
        |> andMap decodeEchanges
        |> andMap decodeDemandes
        |> andMap decodeActions


decodeCreateursStats : Decoder CreateursStatsData
decodeCreateursStats =
    Decode.succeed CreateursStatsData
        |> andMap decodeCreateurs
        |> andMap decodeRappels
        |> andMap decodeEchanges
        |> andMap decodeDemandes
        |> andMap decodeActions


decodeEtablissements : Decoder EtablissementsStats
decodeEtablissements =
    Decode.field "etablissements" <|
        (Decode.succeed EtablissementsStats
            |> andMap (Decode.field "nouveaux" <| Decode.int)
            |> andMap (Decode.field "qpvs" <| Decode.int)
            |> andMap (Decode.field "exogenes" <| Decode.int)
        )


decodeCreateurs : Decoder CreateursStats
decodeCreateurs =
    Decode.field "createurs" <|
        (Decode.succeed CreateursStats
            |> andMap (Decode.field "nouveaux" <| Decode.int)
            |> andMap (Decode.field "qpvs" <| Decode.int)
            |> andMap (Decode.field "transformes" <| Decode.int)
        )


decodeRappels : Decoder RappelsStats
decodeRappels =
    Decode.field "rappels" <|
        (Decode.succeed RappelsStats
            |> andMap (Decode.field "nouveaux" <| Decode.int)
            |> andMap (Decode.field "expires" <| Decode.int)
            |> andMap (Decode.field "clotures" <| Decode.int)
        )


decodeEchanges : Decoder (List EchangesStats)
decodeEchanges =
    Decode.field "echanges" <|
        Decode.list <|
            (Decode.succeed EchangesStats
                |> andMap (Decode.field "auteur" <| Decode.string)
                |> andMap (Decode.field "type" <| Decode.map UI.Echange.echangeToDisplay <| Data.Echange.decodeTypeEchange)
            )


decodeDemandes : Decoder DemandesStats
decodeDemandes =
    Decode.field "demandes" <|
        (Decode.succeed DemandesStats
            |> andMap (Decode.field "nouvelles" <| Decode.int)
            |> andMap (Decode.field "ouvertes" <| Decode.list <| Decode.field "type" <| Decode.map Data.Demande.typeDemandeToDisplay <| Data.Demande.decodeTypeDemande)
            |> andMap (Decode.field "cloturees" <| Decode.int)
        )


decodeActions : Decoder ActionsStats
decodeActions =
    Decode.field "actions" <|
        (Decode.succeed ActionsStats
            |> andMap (Decode.field "vues" <| Decode.int)
            |> andMap (Decode.field "qualifications" <| Decode.int)
            |> andMap (Decode.field "etiquetages" <| Decode.int)
        )
