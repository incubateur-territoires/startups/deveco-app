module Pages.Admin.Utilisateurs exposing (Model, Msg, page)

import Accessibility exposing (Html, br, div, formWithListeners, label, span, text)
import Api
import Api.Auth
import Api.EntityId
import DSFR.Alert
import DSFR.Button
import DSFR.CallOut
import DSFR.Grid
import DSFR.Icons exposing (iconMD)
import DSFR.Icons.Business exposing (lineChartFill)
import DSFR.Icons.Design exposing (editFill)
import DSFR.Icons.System exposing (addLine, arrowDownLine, arrowUpLine, downloadLine, lockLine, lockUnlockLine)
import DSFR.Input
import DSFR.Modal
import DSFR.SearchBar
import DSFR.Table
import Data.Role
import Data.Territoire exposing (Territoire)
import Effect
import Html
import Html.Attributes exposing (class)
import Html.Attributes.Extra exposing (empty)
import Html.Events as Events exposing (onClick)
import Html.Extra exposing (nothing)
import Http
import Json.Decode as Decode
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode
import Lib.Date
import Lib.UI exposing (withEmptyAs)
import List.Extra
import RemoteData as RD exposing (WebData)
import Route
import Shared exposing (User)
import SingleSelect
import Spa.Page exposing (Page)
import Time exposing (Posix)
import TimeZone
import UI.Layout
import User
import View exposing (View)


type alias Model =
    { recherche : String
    , territoires : WebData (List Territoire)
    , users : WebData (List UserWithAdminInfo)
    , userInput : Maybe UserInput
    , saveUserRequest : WebData (List UserWithAdminInfo)
    , passwordInput : Maybe PasswordInput
    , savePasswordRequest : WebData ()
    , selectTerritoire : SingleSelect.SmartSelect Msg (Maybe Territoire)
    , selectedTerritoire : Maybe Territoire
    , sorting : Sorting
    , actifRequest : WebData ()
    , statsGeneration : WebData ()
    }


type Msg
    = NoOp
    | SharedMsg Shared.Msg
    | ReceivedTerritoires (WebData (List Territoire))
    | ReceivedUsers (WebData (List UserWithAdminInfo))
    | UpdatedRecherche String
    | SelectedTerritoire ( Maybe Territoire, SingleSelect.Msg (Maybe Territoire) )
    | UpdatedSelectTerritoire (SingleSelect.Msg (Maybe Territoire))
    | ClickedAddUser
    | CanceledAddUser
    | UpdatedUserInput UserField String
    | ClickedEditPassword (Api.EntityId.EntityId User.UserId)
    | CanceledEditPassword
    | UpdatedPasswordInput PasswordField String
    | SelectedNewTerritoire ( Territoire, SingleSelect.Msg Territoire )
    | UpdatedSelectNewTerritoire (SingleSelect.Msg Territoire)
    | RequestedSaveUser
    | ReceivedSaveUser (WebData (List UserWithAdminInfo))
    | RequestedSavePassword
    | ReceivedSavePassword (WebData ())
    | SetSorting Header
    | ClickedToggleActif (Api.EntityId.EntityId User.UserId) Bool
    | ReceivedToggleActif (WebData (List UserWithAdminInfo))
    | ReceivedStatsGeneration (WebData ())
    | ClickedGenerateStats


type UserField
    = UserEmail
    | UserNom
    | UserPrenom


type PasswordField
    = Password
    | Repeat


type alias UserWithAdminInfo =
    { user : User
    , lastLogin : Maybe Posix
    , lastAuth : Maybe Posix
    , lastRequest : Maybe Posix
    , accessUrl : Maybe String
    , enabled : Bool
    }


type alias Sorting =
    ( Header, Direction )


type Direction
    = Ascending
    | Descending


type alias UserInput =
    { email : String
    , nom : String
    , prenom : String
    , nomTerritoire : String
    , selectNewTerritoire : SingleSelect.SmartSelect Msg Territoire
    , selectedNewTerritoire : Maybe Territoire
    }


defaultUserInput : UserInput
defaultUserInput =
    { email = ""
    , nom = ""
    , prenom = ""
    , nomTerritoire = ""
    , selectNewTerritoire =
        SingleSelect.init "champ-selection-territoire"
            { selectionMsg = SelectedNewTerritoire
            , internalMsg = UpdatedSelectNewTerritoire
            }
    , selectedNewTerritoire = Nothing
    }


type alias PasswordInput =
    { password : String
    , repeat : String
    , id : Api.EntityId.EntityId User.UserId
    }


defaultPasswordInput : Api.EntityId.EntityId User.UserId -> PasswordInput
defaultPasswordInput id =
    { password = ""
    , repeat = ""
    , id = id
    }


page : Shared.Shared -> User -> Page () Shared.Msg (View Msg) Model Msg
page _ _ =
    Spa.Page.element
        { view = view
        , init = init
        , update = update
        , subscriptions = .selectTerritoire >> SingleSelect.subscriptions
        }


init : () -> ( Model, Effect.Effect Shared.Msg Msg )
init () =
    ( { recherche = ""
      , territoires = RD.Loading
      , users = RD.Loading
      , userInput = Nothing
      , saveUserRequest = RD.NotAsked
      , passwordInput = Nothing
      , savePasswordRequest = RD.NotAsked
      , selectTerritoire =
            SingleSelect.init "territoire-select"
                { selectionMsg = SelectedTerritoire
                , internalMsg = UpdatedSelectTerritoire
                }
      , selectedTerritoire = Nothing
      , sorting = ( HEmail, Ascending )
      , actifRequest = RD.NotAsked
      , statsGeneration = RD.NotAsked
      }
    , Effect.batch [ getUsers, getTerritoires ]
    )
        |> Shared.pageChangeEffects


getUsers : Effect.Effect Shared.Msg Msg
getUsers =
    Api.Auth.get
        { url = Api.getUtilisateurs
        }
        SharedMsg
        ReceivedUsers
        decodeAdminUtilisateurs
        |> Effect.fromCmd


getTerritoires : Effect.Effect Shared.Msg Msg
getTerritoires =
    Api.Auth.get
        { url = Api.adminTerritoires
        }
        SharedMsg
        ReceivedTerritoires
        (Decode.list Data.Territoire.decodeTerritoire)
        |> Effect.fromCmd


decodeAdminUtilisateurs : Decode.Decoder (List UserWithAdminInfo)
decodeAdminUtilisateurs =
    Decode.list
        (Decode.succeed UserWithAdminInfo
            |> andMap User.decodeUser
            |> andMap (optionalNullableField "lastLogin" <| Lib.Date.decodeDateWithTimeFromISOString)
            |> andMap (optionalNullableField "lastAuth" <| Lib.Date.decodeDateWithTimeFromISOString)
            |> andMap (optionalNullableField "accessKeyDate" <| Lib.Date.decodeDateWithTimeFromISOString)
            |> andMap (optionalNullableField "accessUrl" Decode.string)
            |> andMap (Decode.map (Maybe.withDefault True) (optionalNullableField "enabled" Decode.bool))
        )


update : Msg -> Model -> ( Model, Effect.Effect Shared.Msg Msg )
update msg model =
    case msg of
        NoOp ->
            model
                |> Effect.withNone

        SharedMsg sharedMsg ->
            model
                |> Effect.withShared sharedMsg

        ReceivedTerritoires response ->
            { model | territoires = response }
                |> Effect.withNone

        ReceivedUsers response ->
            { model | users = response }
                |> Effect.withNone

        UpdatedRecherche recherche ->
            { model | recherche = recherche }
                |> Effect.withNone

        SelectedTerritoire ( territoire, sMsg ) ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg model.selectTerritoire
            in
            ( { model | selectedTerritoire = territoire, selectTerritoire = updatedSelect }, Effect.fromCmd selectCmd )

        UpdatedSelectTerritoire sMsg ->
            let
                ( updatedSelect, selectCmd ) =
                    SingleSelect.update sMsg model.selectTerritoire
            in
            ( { model | selectTerritoire = updatedSelect }, Effect.fromCmd selectCmd )

        UpdatedUserInput field value ->
            { model | userInput = model.userInput |> Maybe.map (updateUserInput field value) }
                |> Effect.withNone

        ClickedAddUser ->
            { model | userInput = Just defaultUserInput, saveUserRequest = RD.NotAsked }
                |> Effect.withNone

        CanceledAddUser ->
            { model | userInput = Nothing }
                |> Effect.withNone

        UpdatedPasswordInput field value ->
            { model | passwordInput = model.passwordInput |> Maybe.map (updatePasswordInput field value) }
                |> Effect.withNone

        ClickedEditPassword id ->
            { model | passwordInput = Just <| defaultPasswordInput id }
                |> Effect.withNone

        CanceledEditPassword ->
            { model | passwordInput = Nothing }
                |> Effect.withNone

        RequestedSaveUser ->
            case ( model.saveUserRequest, model.userInput ) of
                ( RD.Loading, _ ) ->
                    model
                        |> Effect.withNone

                ( _, Just userInput ) ->
                    { model | saveUserRequest = RD.Loading }
                        |> Effect.withCmd (saveUser userInput)

                _ ->
                    model
                        |> Effect.withNone

        ReceivedSaveUser response ->
            case model.saveUserRequest of
                RD.Loading ->
                    case response of
                        RD.Success _ ->
                            { model
                                | saveUserRequest = response
                                , userInput = Nothing
                                , users = response
                            }
                                |> Effect.withNone

                        _ ->
                            { model | saveUserRequest = response }
                                |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        RequestedSavePassword ->
            case ( model.savePasswordRequest, model.passwordInput ) of
                ( RD.Loading, _ ) ->
                    model
                        |> Effect.withNone

                ( _, Just passwordInput ) ->
                    { model | savePasswordRequest = RD.Loading }
                        |> Effect.withCmd (savePassword passwordInput)

                _ ->
                    model
                        |> Effect.withNone

        ReceivedSavePassword response ->
            case model.savePasswordRequest of
                RD.Loading ->
                    case response of
                        RD.Success _ ->
                            { model
                                | savePasswordRequest = response
                                , passwordInput = Nothing
                            }
                                |> Effect.withNone

                        _ ->
                            { model | savePasswordRequest = response }
                                |> Effect.withNone

                _ ->
                    model
                        |> Effect.withNone

        SelectedNewTerritoire ( newTerritoire, sMsg ) ->
            case model.userInput of
                Nothing ->
                    model
                        |> Effect.withNone

                Just input ->
                    let
                        ( updatedSelect, selectCmd ) =
                            SingleSelect.update sMsg input.selectNewTerritoire
                    in
                    ( { model | userInput = Just { input | selectNewTerritoire = updatedSelect, selectedNewTerritoire = Just newTerritoire } }, Effect.fromCmd selectCmd )

        UpdatedSelectNewTerritoire sMsg ->
            case model.userInput of
                Nothing ->
                    model
                        |> Effect.withNone

                Just input ->
                    let
                        ( updatedSelect, selectCmd ) =
                            SingleSelect.update sMsg input.selectNewTerritoire
                    in
                    ( { model | userInput = Just { input | selectNewTerritoire = updatedSelect } }, Effect.fromCmd selectCmd )

        SetSorting header ->
            let
                sorting =
                    case model.sorting of
                        ( h, Ascending ) ->
                            if h == header then
                                ( h, Descending )

                            else
                                ( header, Ascending )

                        ( h, Descending ) ->
                            if h == header then
                                ( h, Ascending )

                            else
                                ( header, Ascending )
            in
            ( { model | sorting = sorting }, Effect.none )

        ClickedToggleActif userId on ->
            ( { model
                | actifRequest = RD.Loading
              }
            , Effect.fromCmd <| requestToggleActif userId on
            )

        ReceivedToggleActif (RD.Success r) ->
            ( { model | users = RD.Success r, actifRequest = RD.NotAsked }, Effect.none )

        ReceivedToggleActif response ->
            ( { model | actifRequest = response |> RD.map (\_ -> ()) }, Effect.none )

        ClickedGenerateStats ->
            ( model
            , Effect.fromCmd <| requestStatsGeneration
            )

        ReceivedStatsGeneration response ->
            ( { model | statsGeneration = response }, Effect.none )


view : Model -> View Msg
view model =
    { title = UI.Layout.pageTitle "Utilisateurs - Superadmin"
    , body = [ body model ]
    , route = Route.AdminUtilisateurs
    }


rechercherUtilisateursInputName : String
rechercherUtilisateursInputName =
    "recherches-utilisateurs"


body : Model -> Html Msg
body model =
    let
        territoires =
            model.territoires
                |> RD.withDefault []

        territoriesWithImportInProgress =
            territoires
                |> List.filter Data.Territoire.importInProgress
    in
    div [ DSFR.Grid.gridRow ]
        [ case model.userInput of
            Just input ->
                addUserModal territoires model.saveUserRequest input

            Nothing ->
                nothing
        , case model.passwordInput of
            Just input ->
                addPasswordModal model.savePasswordRequest input

            Nothing ->
                nothing
        , div [ DSFR.Grid.col12, class "p-4 fr-card--white" ]
            [ if List.length territoriesWithImportInProgress > 0 then
                div [ DSFR.Grid.gridRow ]
                    [ div [ DSFR.Grid.col ]
                        [ DSFR.CallOut.callout Nothing (Just "Import de territoire en cours") <|
                            div []
                                [ span []
                                    [ text "Le"
                                    , text <| Lib.UI.plural <| List.length territoriesWithImportInProgress
                                    , text " territoire"
                                    , text <| Lib.UI.plural <| List.length territoriesWithImportInProgress
                                    , text " suivant"
                                    , text <| Lib.UI.plural <| List.length territoriesWithImportInProgress
                                    , text " "
                                    , text <|
                                        if List.length territoriesWithImportInProgress > 1 then
                                            "sont"

                                        else
                                            "est"
                                    , text " actuellement en cours d'import\u{00A0}:"
                                    ]
                                , br []
                                , text <| String.join ", " <| List.map Data.Territoire.nom <| territoriesWithImportInProgress
                                ]
                        ]
                    ]

              else
                nothing
            , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
                [ div [ DSFR.Grid.col, class "flex flex-row gap-4 justify-end" ]
                    [ DSFR.Button.new { label = "Exporter les utilisateurs", onClick = Nothing }
                        |> DSFR.Button.linkButton Api.getUtilisateursCsv
                        |> DSFR.Button.leftIcon downloadLine
                        |> DSFR.Button.withAttrs [ Html.Attributes.target "_self" ]
                        |> DSFR.Button.secondary
                        |> DSFR.Button.view
                    , DSFR.Button.new { label = "Générer les stats", onClick = Just <| ClickedGenerateStats }
                        |> DSFR.Button.withAttrs
                            [ Html.Attributes.name <| "generate-stats"
                            ]
                        |> DSFR.Button.tertiary
                        |> DSFR.Button.leftIcon lineChartFill
                        |> DSFR.Button.withDisabled (model.statsGeneration == RD.Loading || RD.isSuccess model.statsGeneration)
                        |> DSFR.Button.view
                    , DSFR.Button.new { label = "Ajouter un utilisateur", onClick = Just ClickedAddUser }
                        |> DSFR.Button.leftIcon addLine
                        |> DSFR.Button.view
                    ]
                ]
            , div [ DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters, class "!mb-4" ]
                [ div [ DSFR.Grid.col6, class "flex flex-col gap-2" ] <|
                    [ label
                        [ class "fr-label"
                        , Html.Attributes.for rechercherUtilisateursInputName
                        ]
                        [ text "Rechercher un utilisateur" ]
                    , DSFR.SearchBar.searchBar
                        { submitMsg = NoOp
                        , buttonLabel = "Rechercher"
                        , inputMsg = UpdatedRecherche
                        , inputLabel = "Rechercher un utilisateur"
                        , inputPlaceholder = Just "Recherche par email, nom, prénom"
                        , inputId = rechercherUtilisateursInputName
                        , inputValue = model.recherche
                        , hints = []
                        , fullLabel = Nothing
                        }
                    ]
                , div [ DSFR.Grid.col6 ] <|
                    List.singleton <|
                        SingleSelect.viewCustom
                            { selected = Just model.selectedTerritoire
                            , options =
                                territoires
                                    |> List.map Just
                                    |> (::) Nothing
                            , optionLabelFn = Maybe.map Data.Territoire.nom >> Maybe.withDefault "Tous"
                            , isDisabled = False
                            , optionDescriptionFn = \_ -> ""
                            , optionsContainerMaxHeight = 300
                            , selectTitle = text "Territoire"
                            , searchPrompt = "Rechercher un territoire"
                            , noResultsForMsg = \searchText -> "Aucun autre territoire n'a été trouvé pour " ++ searchText
                            , noOptionsMsg = "Aucune territoire n'a été trouvé"
                            , searchFn =
                                \searchText allOptions ->
                                    List.filter
                                        (Maybe.map
                                            (String.contains (String.toLower searchText) << String.toLower << Data.Territoire.nom)
                                            >> Maybe.withDefault False
                                        )
                                        allOptions
                            }
                            model.selectTerritoire
                ]
            , div [ DSFR.Grid.gridRow ]
                [ div [ DSFR.Grid.col ] <|
                    List.singleton <|
                        viewUsersTable model.actifRequest model.sorting model.recherche model.selectedTerritoire model.users
                ]
            ]
        ]


viewUsersTable : WebData () -> Sorting -> String -> Maybe Territoire -> WebData (List UserWithAdminInfo) -> Html Msg
viewUsersTable actifRequest sorting recherche territoire data =
    case data of
        RD.NotAsked ->
            nothing

        RD.Loading ->
            div [ class "text-center", DSFR.Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Chargement en cours"
                    ]
                ]

        RD.Failure _ ->
            div [ class "text-center", DSFR.Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Une erreur s'est produite, veuillez recharger la page."
                    ]
                ]

        RD.Success [] ->
            div [ class "text-center", DSFR.Grid.col ]
                [ div [ class "fr-card--white h-[350px] p-20" ]
                    [ text "Aucun utilisateur pour l'instant."
                    ]
                ]

        RD.Success users ->
            case users |> List.filter (.user >> filterUsers recherche territoire) of
                [] ->
                    div [ class "text-center", DSFR.Grid.col ]
                        [ div [ class "fr-card--white p-20" ]
                            [ text "Aucune utilisateur correspondant."
                            ]
                        ]

                us ->
                    div [ DSFR.Grid.col12 ]
                        [ DSFR.Table.table
                            { id = "tableau-utilisateurs"
                            , caption = text "Utilisateurs"
                            , headers = headers
                            , rows = us |> sortWithSorting sorting
                            , toHeader =
                                \header ->
                                    let
                                        sortOnClick =
                                            if isSortable header then
                                                [ onClick <| SetSorting <| header, class "cursor-pointer" ]

                                            else
                                                []

                                        sortIcon =
                                            if isSortable header then
                                                sortingIcon sorting header

                                            else
                                                nothing
                                    in
                                    Html.span sortOnClick
                                        [ headerToString header |> text
                                        , sortIcon
                                        ]
                            , toRowId = .user >> User.email
                            , toCell = toCell actifRequest
                            }
                            |> DSFR.Table.withContainerAttrs [ class "!mb-0" ]
                            |> DSFR.Table.withToRowAttrs
                                (\{ enabled } ->
                                    if enabled then
                                        []

                                    else
                                        [ class "opacity-[0.5]"
                                        , Html.Attributes.title "Compte désactivé"
                                        ]
                                )
                            |> DSFR.Table.noBorders
                            |> DSFR.Table.captionHidden
                            |> DSFR.Table.fixed
                            |> DSFR.Table.view
                        ]


sortingIcon : ( Header, Direction ) -> Header -> Html msg
sortingIcon ( sortingHeader, direction ) header =
    if header == sortingHeader then
        case direction of
            Ascending ->
                iconMD arrowDownLine

            Descending ->
                iconMD arrowUpLine

    else
        nothing


sortWithSorting : ( Header, Direction ) -> List UserWithAdminInfo -> List UserWithAdminInfo
sortWithSorting ( header, direction ) =
    let
        sortWith =
            case header of
                HEmail ->
                    .user
                        >> User.email
                        >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"

                HPrenom ->
                    .user
                        >> User.prenom
                        >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"

                HNom ->
                    .user
                        >> User.nom
                        >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"

                HTerritoireNom ->
                    .user
                        >> User.role
                        >> Data.Role.territoire
                        >> Maybe.map (Data.Territoire.nom >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ")
                        >> Maybe.withDefault "ZZZZZZZZZZZZZZZZZZZZZ"

                HTerritoireType ->
                    .user
                        >> User.role
                        >> Data.Role.territoire
                        >> Maybe.map (Data.Territoire.type_ >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ")
                        >> Maybe.withDefault "ZZZZZZZZZZZZZZZZZZZZZ"

                HLogin ->
                    .lastLogin
                        >> Maybe.map Time.posixToMillis
                        >> Maybe.withDefault 0
                        >> String.fromInt

                HAuth ->
                    .lastAuth
                        >> Maybe.map Time.posixToMillis
                        >> Maybe.withDefault 0
                        >> String.fromInt

                HToken ->
                    .user
                        >> User.email
                        >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"

                HPassword ->
                    .user
                        >> User.email
                        >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"

                HActif ->
                    .user
                        >> User.email
                        >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"

                HWelcome ->
                    .user
                        >> User.email
                        >> withEmptyAs "ZZZZZZZZZZZZZZZZZZZZZ"
    in
    List.Extra.stableSortWith
        (\a b ->
            case direction of
                Ascending ->
                    compare (sortWith a) (sortWith b)

                Descending ->
                    compare (sortWith b) (sortWith a)
        )


filterUsers : String -> Maybe Territoire -> User -> Bool
filterUsers recherche territoire user =
    let
        lowerRecherche =
            String.toLower recherche

        matches =
            String.toLower
                >> String.contains lowerRecherche

        matchRecherche =
            (lowerRecherche == "")
                || (matches <| User.email user)
                || (matches <| User.nom user)
                || (matches <| User.prenom user)

        userTerritoire =
            user
                |> User.role
                |> Data.Role.territoire

        matchTerritoire =
            case ( territoire, userTerritoire ) of
                ( Nothing, _ ) ->
                    True

                ( Just _, Nothing ) ->
                    False

                ( Just t, Just ut ) ->
                    Data.Territoire.nom t == Data.Territoire.nom ut
    in
    matchTerritoire && matchRecherche


type Header
    = HEmail
    | HPrenom
    | HNom
    | HTerritoireNom
    | HTerritoireType
    | HLogin
    | HAuth
    | HToken
    | HPassword
    | HActif
    | HWelcome


headers : List Header
headers =
    [ HEmail
    , HPrenom
    , HNom
    , HTerritoireNom
    , HTerritoireType
    , HLogin
    , HAuth
    , HToken
    , HPassword
    , HActif
    , HWelcome
    ]


isSortable : Header -> Bool
isSortable header =
    not <|
        List.member header [ HToken, HPassword ]


headerToString : Header -> String
headerToString header =
    case header of
        HEmail ->
            "Email"

        HNom ->
            "Nom"

        HPrenom ->
            "Prénom"

        HTerritoireNom ->
            "Territoire"

        HTerritoireType ->
            "Type"

        HLogin ->
            "Dernier login"

        HAuth ->
            "Dernière action"

        HToken ->
            "Lien"

        HPassword ->
            ""

        HActif ->
            ""

        HWelcome ->
            ""


toCell : WebData () -> Header -> UserWithAdminInfo -> Html Msg
toCell actifRequest header { user, accessUrl, lastRequest, lastLogin, lastAuth, enabled } =
    let
        pendingActif =
            actifRequest == RD.Loading
    in
    case header of
        HEmail ->
            span [ Html.Attributes.style "overflow-wrap" "break-word" ] <|
                List.singleton <|
                    text <|
                        withEmptyAs "-" <|
                            User.email user

        HPrenom ->
            text <|
                withEmptyAs "-" <|
                    User.prenom user

        HNom ->
            text <|
                withEmptyAs "-" <|
                    User.nom user

        HTerritoireNom ->
            user
                |> User.role
                |> Data.Role.territoire
                |> Maybe.map (Data.Territoire.nom >> withEmptyAs "-")
                |> Maybe.withDefault "-"
                |> text

        HTerritoireType ->
            user
                |> User.role
                |> Data.Role.territoire
                |> Maybe.map (Data.Territoire.categorie >> Data.Territoire.typeToDisplay)
                |> Maybe.withDefault "-"
                |> text

        HLogin ->
            lastLogin
                |> Maybe.map (Lib.Date.dateTimeToShortFrenchString (TimeZone.europe__paris ()))
                |> Maybe.withDefault "-"
                |> text

        HAuth ->
            lastAuth
                |> Maybe.map (Lib.Date.dateTimeToShortFrenchString (TimeZone.europe__paris ()))
                |> Maybe.withDefault "-"
                |> text

        HToken ->
            accessUrl
                |> Maybe.withDefault "-"
                |> withEmptyAs "-"
                |> text
                |> List.singleton
                |> span [ class "break-all", lastRequest |> Maybe.map (Lib.Date.dateTimeToShortFrenchString (TimeZone.europe__paris ())) |> Maybe.map Html.Attributes.title |> Maybe.withDefault empty ]

        HPassword ->
            DSFR.Button.new { label = "", onClick = Just <| ClickedEditPassword <| User.id <| user }
                |> DSFR.Button.withAttrs [ Html.Attributes.name <| "edit-password-" ++ (Api.EntityId.entityIdToString <| User.id user) ]
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.onlyIcon editFill
                |> DSFR.Button.view

        HActif ->
            DSFR.Button.new { label = "", onClick = Just <| ClickedToggleActif (User.id <| user) <| not enabled }
                |> DSFR.Button.withAttrs
                    [ Html.Attributes.name <| "edit-actif-" ++ (Api.EntityId.entityIdToString <| User.id user)
                    , Html.Attributes.title <|
                        if enabled then
                            "Désactiver"

                        else
                            "Activer"
                    ]
                |> DSFR.Button.tertiaryNoOutline
                |> DSFR.Button.withDisabled pendingActif
                |> DSFR.Button.onlyIcon
                    (if enabled then
                        lockLine

                     else
                        lockUnlockLine
                    )
                |> DSFR.Button.view

        HWelcome ->
            let
                ( title, colorClass ) =
                    if user |> User.role |> Data.Role.welcomeEmailSent |> Maybe.withDefault False then
                        ( "Email de bienvenue envoyé", "blue-text" )

                    else
                        ( "Email de bienvenue pas encore envoyé", "red-text" )
            in
            span [ class colorClass, Html.Attributes.title title ] [ DSFR.Icons.icon <| DSFR.Icons.Business.sendPlaneFill ]


addUserModal : List Territoire -> WebData (List UserWithAdminInfo) -> UserInput -> Html Msg
addUserModal territoires request input =
    DSFR.Modal.view
        { id = "utilisateur"
        , label = "utilisateur"
        , openMsg = NoOp
        , closeMsg = Just CanceledAddUser
        , title = text "Ajouter un utilisateur"
        , opened = True
        }
        (viewUserForm territoires request input)
        Nothing
        |> Tuple.first


addPasswordModal : WebData () -> PasswordInput -> Html Msg
addPasswordModal request input =
    DSFR.Modal.view
        { id = "password"
        , label = "password"
        , openMsg = NoOp
        , closeMsg = Just CanceledEditPassword
        , title = text "Remplacer le mot de passe de l'utilisateur"
        , opened = True
        }
        (viewPasswordForm request input)
        Nothing
        |> Tuple.first


updateUserInput : UserField -> String -> UserInput -> UserInput
updateUserInput field value userInput =
    case field of
        UserEmail ->
            { userInput | email = value }

        UserNom ->
            { userInput | nom = value }

        UserPrenom ->
            { userInput | prenom = value }


updatePasswordInput : PasswordField -> String -> PasswordInput -> PasswordInput
updatePasswordInput field value passwordInput =
    case field of
        Password ->
            { passwordInput | password = value }

        Repeat ->
            { passwordInput | repeat = value }


viewUserForm : List Territoire -> WebData (List UserWithAdminInfo) -> UserInput -> Html Msg
viewUserForm territoires request input =
    formWithListeners [ Events.onSubmit <| RequestedSaveUser, DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ DSFR.Input.new { value = input.prenom, label = text "Prénom *", onInput = UpdatedUserInput UserPrenom, name = "prenom" }
            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
            |> DSFR.Input.view
        , DSFR.Input.new { value = input.nom, label = text "Nom *", onInput = UpdatedUserInput UserNom, name = "nom" }
            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
            |> DSFR.Input.view
        , DSFR.Input.new { value = input.email, label = text "Courriel *", onInput = UpdatedUserInput UserEmail, name = "email" }
            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
            |> DSFR.Input.view
        , div [ DSFR.Grid.col6 ]
            [ input.selectNewTerritoire
                |> SingleSelect.viewCustom
                    { isDisabled = False
                    , selected = input.selectedNewTerritoire
                    , options =
                        territoires
                    , optionLabelFn = Data.Territoire.nom
                    , optionDescriptionFn = \_ -> ""
                    , optionsContainerMaxHeight = 300
                    , selectTitle = text "Territoire"
                    , searchPrompt = "Rechercher un territoire"
                    , noResultsForMsg = \searchText -> "Aucun autre territoire n'a été trouvé pour " ++ searchText
                    , noOptionsMsg = "Aucun territoire n'a été trouvé"
                    , searchFn =
                        \searchText allOptions ->
                            List.filter
                                (String.contains (String.toLower searchText) << String.toLower << Data.Territoire.nom)
                                allOptions
                    }
            ]
        , case request of
            RD.Failure _ ->
                DSFR.Alert.small { title = Just "Création échouée", description = text "Assurez-vous qu'un utilisateur avec ce courriel n'existe pas déjà." }
                    |> DSFR.Alert.alert Nothing DSFR.Alert.error
                    |> List.singleton
                    |> div [ class "flex justify-center items-center p-4 w-full" ]

            _ ->
                nothing
        , div [ DSFR.Grid.col12 ]
            [ footerUtilisateur request input
            ]
        ]


viewPasswordForm : WebData () -> PasswordInput -> Html Msg
viewPasswordForm request input =
    formWithListeners [ Events.onSubmit <| RequestedSavePassword, DSFR.Grid.gridRow, DSFR.Grid.gridRowGutters ]
        [ DSFR.Input.new
            { value = input.password
            , label = text "Mot de passe *"
            , onInput = UpdatedPasswordInput Password
            , name = "password"
            }
            |> DSFR.Input.password
            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
            |> DSFR.Input.view
        , DSFR.Input.new
            { value = input.repeat
            , label = text "Confirmez le mot de passe *"
            , onInput = UpdatedPasswordInput Repeat
            , name = "repeat"
            }
            |> DSFR.Input.password
            |> DSFR.Input.withExtraAttrs [ DSFR.Grid.col6 ]
            |> DSFR.Input.view
        , case request of
            RD.Failure _ ->
                DSFR.Alert.small { title = Just "Modification du mot de passe échouée", description = text "Assurez-vous que les deux mots de passe sont identiques." }
                    |> DSFR.Alert.alert Nothing DSFR.Alert.error
                    |> List.singleton
                    |> div [ class "flex justify-center items-center p-4 w-full" ]

            _ ->
                nothing
        , div [ DSFR.Grid.col12 ]
            [ footerPassword request input
            ]
        ]


footerUtilisateur : WebData (List UserWithAdminInfo) -> UserInput -> Html Msg
footerUtilisateur request userInput =
    let
        valid =
            (userInput.prenom /= "")
                && (userInput.nom /= "")
                && (userInput.email /= "")
                && (userInput.selectedNewTerritoire /= Nothing)

        ( isButtonDisabled, title ) =
            case ( request, valid ) of
                ( RD.Loading, _ ) ->
                    ( True, "Enregistrer" )

                ( _, False ) ->
                    ( True, "Enregistrer" )

                _ ->
                    ( False, "Enregistrer" )
    in
    DSFR.Button.group
        [ DSFR.Button.new { onClick = Nothing, label = "Enregistrer" }
            |> DSFR.Button.submit
            |> DSFR.Button.withAttrs [ Html.Attributes.title title, Html.Attributes.name "submit-new-user" ]
            |> DSFR.Button.withDisabled isButtonDisabled
        , DSFR.Button.new { onClick = Just CanceledAddUser, label = "Annuler" }
            |> DSFR.Button.secondary
        ]
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


footerPassword : WebData () -> PasswordInput -> Html Msg
footerPassword request passwordInput =
    let
        valid =
            passwordInput.password /= "" && passwordInput.repeat /= "" && passwordInput.password == passwordInput.repeat

        ( isButtonDisabled, title ) =
            case ( request, valid ) of
                ( RD.Loading, _ ) ->
                    ( True, "Enregistrer" )

                ( _, False ) ->
                    ( True, "Enregistrer" )

                _ ->
                    ( False, "Enregistrer" )
    in
    DSFR.Button.group
        [ DSFR.Button.new { onClick = Nothing, label = "Enregistrer" }
            |> DSFR.Button.submit
            |> DSFR.Button.withAttrs [ Html.Attributes.title title, Html.Attributes.name "submit-password" ]
            |> DSFR.Button.withDisabled isButtonDisabled
        , DSFR.Button.new { onClick = Just CanceledEditPassword, label = "Annuler" }
            |> DSFR.Button.secondary
        ]
        |> DSFR.Button.inline
        |> DSFR.Button.alignedRightInverted
        |> DSFR.Button.viewGroup


saveUser : UserInput -> Cmd Msg
saveUser { nom, prenom, email, selectedNewTerritoire } =
    case selectedNewTerritoire of
        Nothing ->
            Cmd.none

        Just territoire ->
            let
                jsonBody =
                    [ ( "nom", Encode.string nom )
                    , ( "prenom", Encode.string prenom )
                    , ( "email", Encode.string email )
                    , ( "idTerritoire", Api.EntityId.encodeEntityId <| Data.Territoire.id <| territoire )
                    ]
                        |> Encode.object
                        |> Http.jsonBody
            in
            Api.Auth.post
                { url = Api.createUtilisateur
                , body = jsonBody
                }
                SharedMsg
                ReceivedSaveUser
                decodeAdminUtilisateurs


savePassword : PasswordInput -> Cmd Msg
savePassword { password, repeat, id } =
    let
        jsonBody =
            [ ( "password", Encode.string password )
            , ( "repeat", Encode.string repeat )
            , ( "id", Api.EntityId.encodeEntityId id )
            ]
                |> Encode.object
                |> Http.jsonBody
    in
    Api.Auth.post
        { url = Api.updatePasswordAdmin
        , body = jsonBody
        }
        SharedMsg
        ReceivedSavePassword
        (Decode.succeed ())


requestToggleActif : Api.EntityId.EntityId User.UserId -> Bool -> Cmd Msg
requestToggleActif id on =
    let
        jsonBody =
            [ ( "enabled", Encode.bool on )
            , ( "id", Api.EntityId.encodeEntityId id )
            ]
                |> Encode.object
                |> Http.jsonBody
    in
    Api.Auth.post
        { url = Api.toggleActifAdmin
        , body = jsonBody
        }
        SharedMsg
        ReceivedToggleActif
        decodeAdminUtilisateurs


requestStatsGeneration : Cmd Msg
requestStatsGeneration =
    Api.Auth.post
        { url = Api.refreshStats
        , body = Http.emptyBody
        }
        SharedMsg
        ReceivedStatsGeneration
        (Decode.succeed ())
