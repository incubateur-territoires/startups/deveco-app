module Data.Entite exposing (Contact, ContactId, Entite, EntiteId, activiteAutre, activitesReelles, decodeEntite, encodeContact, getFutureEnseigne, getParticulier, id, localisations, motsCles)

import Api.EntityId exposing (EntityId)
import Data.Etablissement exposing (Etablissement)
import Data.Particulier exposing (Particulier)
import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode
import Lib.Date


type EntiteId
    = EntiteId


type ContactId
    = ContactId


type alias Entite =
    { id : EntityId EntiteId
    , etablissement : Maybe Etablissement
    , createur : Maybe Particulier
    , contacts : List Contact
    , activitesReelles : List String
    , localisations : List String
    , activiteAutre : String
    , motsCles : List String
    , ficheId : Maybe (EntityId ())
    , proprietes : List (EntityId ())
    , futureEnseigne : Maybe String
    , etablissementCree : Maybe Etablissement
    , createurLie : Maybe ( Particulier, EntityId () )
    }


type alias Contact =
    { id : EntityId ContactId
    , fonction : String
    , prenom : String
    , nom : String
    , email : String
    , telephone : String
    , dateDeNaissance : Maybe Date
    }


decodeEntite : Decoder Entite
decodeEntite =
    Decode.succeed Entite
        |> andMap (Decode.field "entiteId" Api.EntityId.decodeEntityId)
        |> andMap (optionalNullableField "entreprise" Data.Etablissement.decodeEtablissement)
        |> andMap (optionalNullableField "particulier" Data.Particulier.decodeParticulier)
        |> andMap (Decode.map (Maybe.withDefault []) <| optionalNullableField "contacts" <| Decode.list decodeContact)
        |> andMap (Decode.map (Maybe.withDefault []) <| optionalNullableField "activitesReelles" <| Decode.list Decode.string)
        |> andMap (Decode.map (Maybe.withDefault []) <| Decode.maybe <| Decode.field "localisations" <| Decode.list Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "activiteAutre" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault []) <| Decode.maybe <| Decode.field "motsCles" <| Decode.list Decode.string)
        |> andMap (optionalNullableField "fiche" <| Decode.field "id" <| Api.EntityId.decodeEntityId)
        |> andMap (Decode.map (Maybe.withDefault []) <| optionalNullableField "nbProprietes" <| Decode.list <| Api.EntityId.decodeEntityId)
        |> andMap (optionalNullableField "futureEnseigne" Decode.string)
        |> andMap (optionalNullableField "etablissementCree" Data.Etablissement.decodeEtablissement)
        |> andMap
            (Decode.succeed Tuple.pair
                |> andMap (Decode.field "particulier" Data.Particulier.decodeParticulier)
                |> andMap (Decode.field "ficheId" Api.EntityId.decodeEntityId)
                |> optionalNullableField "createurLie"
            )


decodeContact : Decoder Contact
decodeContact =
    Decode.succeed Contact
        |> andMap (Decode.field "id" Api.EntityId.decodeEntityId)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "fonction" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "prenom" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "nom" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "email" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "telephone" Decode.string)
        |> andMap (optionalNullableField "dateDeNaissance" Lib.Date.decodeCalendarDate)


encodeContact : Contact -> Encode.Value
encodeContact contact =
    [ ( "fonction", Encode.string contact.fonction )
    , ( "prenom", Encode.string contact.prenom )
    , ( "nom", Encode.string contact.nom )
    , ( "email", Encode.string contact.email )
    , ( "telephone", Encode.string contact.telephone )
    , ( "dateDeNaissance", Maybe.withDefault Encode.null <| Maybe.map Encode.string <| Maybe.map Date.toIsoString <| contact.dateDeNaissance )
    ]
        |> Encode.object


id : Entite -> EntityId EntiteId
id entite =
    entite.id


getParticulier : Entite -> Maybe Particulier
getParticulier { createur } =
    createur


getFutureEnseigne : Entite -> Maybe String
getFutureEnseigne { futureEnseigne } =
    futureEnseigne


activitesReelles : Entite -> List String
activitesReelles =
    .activitesReelles


localisations : Entite -> List String
localisations =
    .localisations


motsCles : Entite -> List String
motsCles =
    .motsCles


activiteAutre : Entite -> String
activiteAutre =
    .activiteAutre
