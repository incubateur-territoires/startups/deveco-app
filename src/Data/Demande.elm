module Data.Demande exposing (Cloture, Demande, DemandeId, TypeDemande(..), cloture, createCloture, decodeDemande, decodeTypeDemande, demandeTypeToString, encodeCloture, encodeTypeDemande, label, liste, stringToTypeDemande, toString, typeDemandeToDisplay, type_)

import Api.EntityId exposing (EntityId)
import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Json.Encode as Encode exposing (Value)
import Lib.Date


type Demande
    = Demande DemandeData


type DemandeId
    = DemandeId


type alias DemandeData =
    { id : EntityId DemandeId
    , type_ : TypeDemande
    , cloture : Bool
    , motif : String
    , dateCloture : Date
    }


type Cloture
    = Cloture ClotureData


type alias ClotureData =
    { demande : Demande
    , motif : String
    }


type TypeDemande
    = Foncier
    | Immobilier
    | Economique
    | Fiscalite
    | Accompagnement
    | RH
    | Autre


id : Demande -> EntityId DemandeId
id (Demande data) =
    data.id


type_ : Demande -> TypeDemande
type_ (Demande data) =
    data.type_


cloture : Demande -> Bool
cloture (Demande data) =
    data.cloture


label : Demande -> String
label =
    type_ >> typeDemandeToDisplay


encodeTypeDemande : TypeDemande -> Value
encodeTypeDemande =
    demandeTypeToString >> Encode.string


demandeTypeToString : TypeDemande -> String
demandeTypeToString demande =
    case demande of
        Foncier ->
            "foncier"

        Immobilier ->
            "immobilier"

        Economique ->
            "economique"

        Fiscalite ->
            "fiscalite"

        Accompagnement ->
            "accompagnement"

        RH ->
            "rh"

        Autre ->
            "autre"


decodeTypeDemande : Decoder TypeDemande
decodeTypeDemande =
    Decode.string
        |> Decode.andThen
            (\s ->
                s
                    |> stringToTypeDemande
                    |> Maybe.map Decode.succeed
                    |> Maybe.withDefault (Decode.fail <| "Unknown typeDemande " ++ s)
            )


stringToTypeDemande : String -> Maybe TypeDemande
stringToTypeDemande s =
    case s of
        "foncier" ->
            Just Foncier

        "immobilier" ->
            Just Immobilier

        "economique" ->
            Just Economique

        "fiscalite" ->
            Just Fiscalite

        "accompagnement" ->
            Just Accompagnement

        "rh" ->
            Just RH

        "autre" ->
            Just Autre

        _ ->
            Nothing


typeDemandeToDisplay : TypeDemande -> String
typeDemandeToDisplay demande =
    case demande of
        Foncier ->
            "Foncier"

        Immobilier ->
            "Immobilier et locaux d'activité"

        Economique ->
            "Aide économique"

        Fiscalite ->
            "Fiscalité"

        Accompagnement ->
            "Accompagnement"

        RH ->
            "RH"

        Autre ->
            "Autre"


liste : List TypeDemande
liste =
    [ Foncier
    , Immobilier
    , Economique
    , Fiscalite
    , Accompagnement
    , RH
    , Autre
    ]


toString : TypeDemande -> String
toString demande =
    case demande of
        Foncier ->
            "foncier"

        Immobilier ->
            "immobilier"

        Economique ->
            "economique"

        Fiscalite ->
            "fiscalite"

        Accompagnement ->
            "accompagnement"

        RH ->
            "rh"

        Autre ->
            "autre"


decodeDemande : Decoder Demande
decodeDemande =
    Decode.succeed DemandeData
        |> andMap (Decode.field "id" Api.EntityId.decodeEntityId)
        |> andMap (Decode.field "typeDemande" <| decodeTypeDemande)
        |> andMap (Decode.map (Maybe.withDefault False) <| optionalNullableField "cloture" <| Decode.bool)
        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "motif" <| Decode.string)
        |> andMap (Decode.map (Maybe.withDefault <| Date.fromRataDie 0) <| optionalNullableField "dateCloture" <| Lib.Date.decodeCalendarDate)
        |> Decode.map Demande


createCloture : { demande : Demande, motif : String } -> Cloture
createCloture =
    Cloture


encodeCloture : String -> Cloture -> Value
encodeCloture from (Cloture clot) =
    [ ( "demandeId", Api.EntityId.encodeEntityId <| id clot.demande )
    , ( "motif", Encode.string clot.motif )
    , ( "from", Encode.string from )
    ]
        |> Encode.object
