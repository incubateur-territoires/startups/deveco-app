module Data.Cloture exposing (Cloture, date, decodeCloture, demande, id, motif)

import Api.EntityId exposing (EntityId)
import Data.Demande
import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Lib.Date


type Cloture
    = Cloture ClotureData


type ClotureId
    = ClotureId


type alias ClotureData =
    { id : EntityId ClotureId
    , demande : Data.Demande.TypeDemande
    , date : Date
    , motif : String
    }


id : Cloture -> EntityId ClotureId
id (Cloture data) =
    data.id


demande : Cloture -> Data.Demande.TypeDemande
demande (Cloture data) =
    data.demande


date : Cloture -> Date
date (Cloture data) =
    data.date


motif : Cloture -> String
motif (Cloture data) =
    data.motif


decodeCloture : Decoder (Maybe Cloture)
decodeCloture =
    optionalNullableField "cloture" Decode.bool
        |> Decode.map (Maybe.withDefault False)
        |> Decode.andThen
            (\cloture ->
                if cloture then
                    Decode.succeed ClotureData
                        |> andMap (Decode.field "id" Api.EntityId.decodeEntityId)
                        |> andMap (Decode.field "typeDemande" <| Data.Demande.decodeTypeDemande)
                        |> andMap (Decode.map (Maybe.withDefault <| Date.fromRataDie 0) <| optionalNullableField "dateCloture" Lib.Date.decodeCalendarDate)
                        |> andMap (Decode.map (Maybe.withDefault "") <| optionalNullableField "motif" Decode.string)
                        |> Decode.map Just

                else
                    Decode.succeed <| Nothing
            )
        |> Decode.map (Maybe.map Cloture)
