module Data.Effectifs exposing (Effectifs, trancheToLabel, tranchesEffectifs)


type alias Effectifs =
    { id : String
    , label : String
    }


tranchesEffectifs : List Effectifs
tranchesEffectifs =
    [ { id = "NN", label = "Unité non employeuse" }
    , { id = "00", label = "Pas de salarié(e)" }
    , { id = "01", label = "1 ou 2 salarié(e)s" }
    , { id = "02", label = "3 à 5 salarié(e)s" }
    , { id = "03", label = "6 à 9 salarié(e)s" }
    , { id = "11", label = "10 à 19 salarié(e)s" }
    , { id = "12", label = "20 à 49 salarié(e)s" }
    , { id = "21", label = "50 à 99 salarié(e)s" }
    , { id = "22", label = "100 à 199 salarié(e)s" }
    , { id = "31", label = "200 à 249 salarié(e)s" }
    , { id = "32", label = "250 à 499 salarié(e)s" }
    , { id = "41", label = "500 à 999 salarié(e)s" }
    , { id = "42", label = "1 000 à 1 999 salarié(e)s" }
    , { id = "51", label = "2 000 à 4 999 salarié(e)s" }
    , { id = "52", label = "5 000 à 9 999 salarié(e)s" }
    , { id = "53", label = "10 000 salarié(e)s et plus" }
    ]


trancheToLabel : String -> Maybe String
trancheToLabel tranche =
    tranchesEffectifs
        |> List.filter (\{ id } -> id == tranche)
        |> List.map .label
        |> List.head
