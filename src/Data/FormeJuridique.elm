module Data.FormeJuridique exposing (Code(..), FormeJuridique(..), decodeFormeJuridique, formeJuridiqueToLabel, formeJuridiqueToString, formesJuridiquesExclues, stringToFormeJuridique)

import Json.Decode as Decode exposing (Decoder)


type FormeJuridique
    = FormeJuridique Code


type Code
    = FJ00
    | FJ10
    | FJ21
    | FJ22
    | FJ23
    | FJ24
    | FJ27
    | FJ28
    | FJ29
    | FJ31
    | FJ32
    | FJ41
    | FJ51
    | FJ52
    | FJ53
    | FJ54
    | FJ55
    | FJ56
    | FJ57
    | FJ58
    | FJ61
    | FJ62
    | FJ63
    | FJ64
    | FJ65
    | FJ69
    | FJ71
    | FJ72
    | FJ73
    | FJ74
    | FJ81
    | FJ82
    | FJ83
    | FJ84
    | FJ85
    | FJ91
    | FJ92
    | FJ93
    | FJ99


formeJuridiqueToLabel : FormeJuridique -> String
formeJuridiqueToLabel (FormeJuridique code) =
    case code of
        FJ00 ->
            "Organisme de placement collectif en valeurs mobilières sans personnalité morale"

        FJ10 ->
            "Entrepreneur individuel"

        FJ21 ->
            "Indivision"

        FJ22 ->
            "Société créée de fait"

        FJ23 ->
            "Société en participation"

        FJ24 ->
            "Fiducie"

        FJ27 ->
            "Paroisse hors zone concordataire"

        FJ28 ->
            "Assujetti unique à la TVA"

        FJ29 ->
            "Autre groupement de droit privé non doté de la personnalité morale"

        FJ31 ->
            "Personne morale de droit étranger, immatriculée au RCS (registre du commerce et des sociétés)"

        FJ32 ->
            "Personne morale de droit étranger, non immatriculée au RCS"

        FJ41 ->
            "Etablissement public ou régie à caractère industriel ou commercial"

        FJ51 ->
            "Société coopérative commerciale particulière"

        FJ52 ->
            "Société en nom collectif"

        FJ53 ->
            "Société en commandite"

        FJ54 ->
            "Société à responsabilité limitée (SARL)"

        FJ55 ->
            "Société anonyme à conseil d'administration"

        FJ56 ->
            "Société anonyme à directoire"

        FJ57 ->
            "Société par actions simplifiée"

        FJ58 ->
            "Société européenne "

        FJ61 ->
            "Caisse d'épargne et de prévoyance"

        FJ62 ->
            "Groupement d'intérêt économique"

        FJ63 ->
            "Société coopérative agricole"

        FJ64 ->
            "Société d'assurance mutuelle"

        FJ65 ->
            "Société civile"

        FJ69 ->
            "Autre personne morale de droit privé inscrite au registre du commerce et des sociétés"

        FJ71 ->
            "Administration de l'état"

        FJ72 ->
            "Collectivité territoriale"

        FJ73 ->
            "Etablissement public administratif"

        FJ74 ->
            "Autre personne morale de droit public administratif"

        FJ81 ->
            "Organisme gérant un régime de protection sociale à adhésion obligatoire"

        FJ82 ->
            "Organisme mutualiste"

        FJ83 ->
            "Comité d'entreprise"

        FJ84 ->
            "Organisme professionnel"

        FJ85 ->
            "Organisme de retraite à adhésion non obligatoire"

        FJ91 ->
            "Syndicat de propriétaires"

        FJ92 ->
            "Association loi 1901 ou assimilé"

        FJ93 ->
            "Fondation"

        FJ99 ->
            "Autre personne morale de droit privé"


formeJuridiqueToString : FormeJuridique -> String
formeJuridiqueToString (FormeJuridique code) =
    case code of
        FJ00 ->
            "FJ00"

        FJ10 ->
            "FJ10"

        FJ21 ->
            "FJ21"

        FJ22 ->
            "FJ22"

        FJ23 ->
            "FJ23"

        FJ24 ->
            "FJ24"

        FJ27 ->
            "FJ27"

        FJ28 ->
            "FJ28"

        FJ29 ->
            "FJ29"

        FJ31 ->
            "FJ31"

        FJ32 ->
            "FJ32"

        FJ41 ->
            "FJ41"

        FJ51 ->
            "FJ51"

        FJ52 ->
            "FJ52"

        FJ53 ->
            "FJ53"

        FJ54 ->
            "FJ54"

        FJ55 ->
            "FJ55"

        FJ56 ->
            "FJ56"

        FJ57 ->
            "FJ57"

        FJ58 ->
            "FJ58"

        FJ61 ->
            "FJ61"

        FJ62 ->
            "FJ62"

        FJ63 ->
            "FJ63"

        FJ64 ->
            "FJ64"

        FJ65 ->
            "FJ65"

        FJ69 ->
            "FJ69"

        FJ71 ->
            "FJ71"

        FJ72 ->
            "FJ72"

        FJ73 ->
            "FJ73"

        FJ74 ->
            "FJ74"

        FJ81 ->
            "FJ81"

        FJ82 ->
            "FJ82"

        FJ83 ->
            "FJ83"

        FJ84 ->
            "FJ84"

        FJ85 ->
            "FJ85"

        FJ91 ->
            "FJ91"

        FJ92 ->
            "FJ92"

        FJ93 ->
            "FJ93"

        FJ99 ->
            "FJ99"


stringToFormeJuridique : String -> Maybe FormeJuridique
stringToFormeJuridique string =
    case string of
        "FJ00" ->
            Just <| FormeJuridique FJ00

        "FJ10" ->
            Just <| FormeJuridique FJ10

        "FJ21" ->
            Just <| FormeJuridique FJ21

        "FJ22" ->
            Just <| FormeJuridique FJ22

        "FJ23" ->
            Just <| FormeJuridique FJ23

        "FJ24" ->
            Just <| FormeJuridique FJ24

        "FJ27" ->
            Just <| FormeJuridique FJ27

        "FJ28" ->
            Just <| FormeJuridique FJ28

        "FJ29" ->
            Just <| FormeJuridique FJ29

        "FJ31" ->
            Just <| FormeJuridique FJ31

        "FJ32" ->
            Just <| FormeJuridique FJ32

        "FJ41" ->
            Just <| FormeJuridique FJ41

        "FJ51" ->
            Just <| FormeJuridique FJ51

        "FJ52" ->
            Just <| FormeJuridique FJ52

        "FJ53" ->
            Just <| FormeJuridique FJ53

        "FJ54" ->
            Just <| FormeJuridique FJ54

        "FJ55" ->
            Just <| FormeJuridique FJ55

        "FJ56" ->
            Just <| FormeJuridique FJ56

        "FJ57" ->
            Just <| FormeJuridique FJ57

        "FJ58" ->
            Just <| FormeJuridique FJ58

        "FJ61" ->
            Just <| FormeJuridique FJ61

        "FJ62" ->
            Just <| FormeJuridique FJ62

        "FJ63" ->
            Just <| FormeJuridique FJ63

        "FJ64" ->
            Just <| FormeJuridique FJ64

        "FJ65" ->
            Just <| FormeJuridique FJ65

        "FJ69" ->
            Just <| FormeJuridique FJ69

        "FJ71" ->
            Just <| FormeJuridique FJ71

        "FJ72" ->
            Just <| FormeJuridique FJ72

        "FJ73" ->
            Just <| FormeJuridique FJ73

        "FJ74" ->
            Just <| FormeJuridique FJ74

        "FJ81" ->
            Just <| FormeJuridique FJ81

        "FJ82" ->
            Just <| FormeJuridique FJ82

        "FJ83" ->
            Just <| FormeJuridique FJ83

        "FJ84" ->
            Just <| FormeJuridique FJ84

        "FJ85" ->
            Just <| FormeJuridique FJ85

        "FJ91" ->
            Just <| FormeJuridique FJ91

        "FJ92" ->
            Just <| FormeJuridique FJ92

        "FJ93" ->
            Just <| FormeJuridique FJ93

        "FJ99" ->
            Just <| FormeJuridique FJ99

        _ ->
            Nothing


formesJuridiquesExclues : List FormeJuridique
formesJuridiquesExclues =
    [ FJ00
    , FJ21
    , FJ22
    , FJ27
    , FJ65
    , FJ71
    , FJ72
    , FJ73
    , FJ74
    , FJ81
    , FJ83
    , FJ84
    , FJ85
    , FJ91
    ]
        |> List.map FormeJuridique


decodeFormeJuridique : Decoder FormeJuridique
decodeFormeJuridique =
    Decode.string
        |> Decode.andThen
            (\s ->
                case stringToFormeJuridique s of
                    Just fj ->
                        Decode.succeed fj

                    Nothing ->
                        Decode.fail <| "Not a valid forme juridique: " ++ s
            )
