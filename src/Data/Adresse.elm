module Data.Adresse exposing (ApiAdresse, ApiStreet, decodeApiFeatures, decodeApiStreet)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap)
import List.Extra


type alias ApiAdresse =
    { label : String
    , housenumber : String
    , street : String
    , city : String
    , postcode : String
    , x : Maybe Float
    , y : Maybe Float
    }


type alias ApiStreet =
    { label : String
    , name : String
    , postcode : String
    , city : String
    }


decodeApiAdresse : Decoder ApiAdresse
decodeApiAdresse =
    Decode.succeed ApiAdresse
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.at [ "properties", "label" ] Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.at [ "properties", "housenumber" ] Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.at [ "properties", "street" ] Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.at [ "properties", "city" ] Decode.string)
        |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.at [ "properties", "postcode" ] Decode.string)
        |> andMap (Decode.map List.head <| Decode.at [ "geometry", "coordinates" ] <| Decode.list <| Decode.float)
        |> andMap (Decode.map (List.Extra.getAt 1) <| Decode.at [ "geometry", "coordinates" ] <| Decode.list <| Decode.float)


decodeApiFeatures : Decoder (List ApiAdresse)
decodeApiFeatures =
    Decode.field "features" <|
        Decode.list <|
            decodeApiAdresse


decodeApiStreet : Decoder (List ApiStreet)
decodeApiStreet =
    Decode.field "features" <|
        Decode.list <|
            Decode.field "properties" <|
                (Decode.succeed ApiStreet
                    |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "label" Decode.string)
                    |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "name" Decode.string)
                    |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "postcode" Decode.string)
                    |> andMap (Decode.map (Maybe.withDefault "") <| Decode.maybe <| Decode.field "city" Decode.string)
                )
