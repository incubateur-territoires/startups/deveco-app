module Data.Etablissement exposing (Etablissement, EtatAdministratif(..), decodeEtablissement, displayNomEnseigne, etatAdministratifToDisplay, etatAdministratifToString)

import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (andMap, optionalNullableField)
import Lib.Date
import Lib.UI exposing (withEmptyAs)


type alias Etablissement =
    { siret : String
    , siren : String
    , nom : String
    , adresse : String
    , dateCreationEtablissement : Maybe Date
    , dateFermetureEtablissement : Maybe Date
    , activitePrincipaleUniteLegale : Maybe String
    , categorieActivitePrincipaleUniteLegale : Maybe String
    , etatAdministratif : EtatAdministratif
    , formeJuridique : Maybe String
    , exercices : List ( String, Date )
    , effectifs : Maybe ( String, Date )
    , effectifsGroupe : Maybe ( String, Date )
    , enseigne : Maybe String
    , exogene : Bool
    , zonagesPrioritaires : List String
    , siege : Bool
    , ess : Bool
    }


type EtatAdministratif
    = Inconnu
    | Actif
    | Inactif


decodeEtablissement : Decoder Etablissement
decodeEtablissement =
    Decode.succeed Etablissement
        |> andMap (Decode.field "siret" Decode.string)
        |> andMap (Decode.map (String.left 9) <| Decode.field "siret" Decode.string)
        |> andMap
            (Decode.map (Maybe.withDefault "") <|
                optionalNullableField "nom" Decode.string
            )
        |> andMap
            (Decode.map (Maybe.withDefault "") <|
                optionalNullableField "adresse" Decode.string
            )
        |> andMap (optionalNullableField "dateCreation" <| Lib.Date.decodeCalendarDate)
        |> andMap (optionalNullableField "dateFermeture" <| Lib.Date.decodeCalendarDate)
        |> andMap decodeNaf
        |> andMap (optionalNullableField "libelleCategorieNaf" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault Inconnu) <| optionalNullableField "etatAdministratif" decodeEtatAdministratif)
        |> andMap (optionalNullableField "formeJuridique" <| Decode.string)
        |> andMap
            (Decode.map (List.filterMap identity) <|
                Decode.map (Maybe.withDefault []) <|
                    optionalNullableField "exercices" <|
                        Decode.list decodeCa
            )
        |> andMap decodeEffectifs
        |> andMap decodeEffectifsGroupe
        |> andMap (optionalNullableField "enseigne" Decode.string)
        |> andMap (Decode.map (Maybe.withDefault False) <| optionalNullableField "exogene" Decode.bool)
        |> andMap (Decode.map (Maybe.withDefault []) <| Decode.maybe <| Decode.field "zonages" <| Decode.list <| Decode.string)
        |> andMap (Decode.map (Maybe.withDefault False) <| Decode.maybe <| Decode.field "siegeSocial" <| Decode.bool)
        |> andMap (Decode.map (Maybe.withDefault False) <| optionalNullableField "ess" Decode.bool)


decodeCa : Decoder (Maybe ( String, Date ))
decodeCa =
    Decode.map2 (Maybe.map2 Tuple.pair)
        (optionalNullableField "ca" <| Decode.string)
        (optionalNullableField "dateCloture" <| Lib.Date.decodeCalendarDate)


decodeEffectifs : Decoder (Maybe ( String, Date ))
decodeEffectifs =
    Decode.map2 (Maybe.map2 Tuple.pair)
        (optionalNullableField "effectifs" <| Decode.string)
        (optionalNullableField "dateEffectifs" <| Lib.Date.decodeCalendarDate)


decodeEffectifsGroupe : Decoder (Maybe ( String, Date ))
decodeEffectifsGroupe =
    Decode.map2 (Maybe.map2 Tuple.pair)
        (optionalNullableField "effectifsGroupe" <| Decode.string)
        (optionalNullableField "dateEffectifsGroupe" <| Lib.Date.decodeCalendarDate)


decodeNaf : Decoder (Maybe String)
decodeNaf =
    Decode.succeed
        (\libelle code ->
            case ( libelle, code ) of
                ( Just l, Just c ) ->
                    l
                        ++ " ("
                        ++ c
                        ++ ")"
                        |> Just

                ( Just l, Nothing ) ->
                    l
                        |> Just

                ( Nothing, Just c ) ->
                    c
                        |> Just

                ( Nothing, Nothing ) ->
                    Nothing
        )
        |> andMap
            (optionalNullableField
                "libelleNaf"
                Decode.string
            )
        |> andMap
            (optionalNullableField
                "codeNaf"
                Decode.string
            )


decodeEtatAdministratif : Decoder EtatAdministratif
decodeEtatAdministratif =
    Decode.string
        |> Decode.andThen
            (\s ->
                s
                    |> stringToEtatAdministratif
                    |> Maybe.map Decode.succeed
                    |> Maybe.withDefault (Decode.fail <| "État administratif inconnu :" ++ s)
            )


{-| nom d'enseigne (nom maison-mère) OU nom maison-mère OU nom d'enseigne si identique à la maison-mère
-}
displayNomEnseigne : { etablissement | siret : String, nom : String, enseigne : Maybe String } -> String
displayNomEnseigne etablissement =
    case etablissement.enseigne of
        Just "" ->
            etablissement.nom
                |> withEmptyAs (etablissement.siret ++ " (non-diffusible)")

        Just ens ->
            if ens == etablissement.nom then
                ens

            else
                ens ++ " (" ++ etablissement.nom ++ ")"

        Nothing ->
            etablissement.nom
                |> withEmptyAs (etablissement.siret ++ " (non-diffusible)")


etatAdministratifToDisplay : EtatAdministratif -> String
etatAdministratifToDisplay etat =
    case etat of
        Actif ->
            "Actif"

        Inactif ->
            "Inactif"

        Inconnu ->
            "Inconnu"


etatAdministratifToString : EtatAdministratif -> String
etatAdministratifToString s =
    case s of
        Actif ->
            "A"

        Inactif ->
            "F"

        Inconnu ->
            "I"


stringToEtatAdministratif : String -> Maybe EtatAdministratif
stringToEtatAdministratif s =
    case s of
        "A" ->
            Just Actif

        "F" ->
            Just Inactif

        "I" ->
            Just Inconnu

        _ ->
            Nothing
