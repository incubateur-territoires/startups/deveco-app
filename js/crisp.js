const crispId = import.meta.env.VITE_CRISP_ID;

export function toggleCrisp(on) {
	if (!window.$crisp) {
		window.$crisp = [];
		window.CRISP_WEBSITE_ID = crispId;
		(function () {
			const d = document;
			const s = d.createElement("script");
			s.src = "https://client.crisp.chat/l.js";
			s.async = 1;
			d.getElementsByTagName("head")[0].appendChild(s);
		})();
	}

	if (on) {
		window.$crisp.push(["do", "chat:show"]);
	} else {
		window.$crisp.push(["do", "chat:hide"]);
	}
}
